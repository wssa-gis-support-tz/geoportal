// This function sets or replaces a html parameter with the given key/value tuple
function setGetParameter(paramName, paramValue, reload_page = true)
{
	var url = window.location.href;
	var hash = location.hash;
	url = url.replace(hash, '');

	euri_param = encodeURI(paramName);
	euri_val = encodeURI(paramValue);

	if (url.indexOf(euri_param + "=") >= 0)
	{
		var prefix = url.substring(0, url.indexOf(euri_param + "="));
		var suffix = url.substring(url.indexOf(euri_param + "="));
		suffix = suffix.substring(suffix.indexOf("=") + 1);
		suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
		url = prefix + euri_param + "=" + euri_val + suffix;
	}
	else {
		if (url.indexOf("?") < 0)
			url += "?" + euri_param + "=" + euri_val;
		else
			url += "&" + euri_param + "=" + euri_val;
	}


	if (reload_page) {
		//alert('Reload with ' + url + hash);
		window.location.href = url + hash;
	}
	else {
		//alert('Set ' + url + hash);
		window.history.pushState({"html":'',"pageTitle":''},"", url + hash);
	}
}



