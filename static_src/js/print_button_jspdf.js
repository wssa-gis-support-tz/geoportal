// https://openlayers.org/en/latest/examples/export-pdf.html

//
// inspired by https://stackoverflow.com/a/838755/521197
// https://gist.github.com/bsorrentino/cf3f8a439ef688d2f869e1c00aaeecf9
//
function getDPI() {
	var div = document.createElement( "div");
	div.style.height = "1in";
	div.style.width = "1in";
	div.style.top = "-100%";
	div.style.left = "-100%";
	div.style.position = "absolute";

	document.body.appendChild(div);

	var result =  div.offsetHeight;

	document.body.removeChild( div );

	return result;
}



var dims = {
	a0: [1189, 841],
	a1: [841, 594],
	a2: [594, 420],
	a3: [420, 297],
	a4: [297, 210],
	a5: [210, 148]
};


function px_to_mm(px, dpi) {
	return 24.5 / dpi * px
};

function mm_to_px(mm, dpi) {
	return dpi / 24.5 * mm
};



/*
if (exportButton) {
	exportButton.addEventListener('click', export_pdf(), false );
}
*/


function export_pdf() {

	// var exportButton = document.getElementById('export-to-pdf');

	// exportButton.disabled = true;
	document.body.style.cursor = 'progress';

	// var format = document.getElementById('format').value;
	// var resolution = document.getElementById('resolution').value;
	// var dim = dims[format];

	// capture map size and resolution
	var map_size = map.getSize();
	var map_px_resolution = map.getView().getResolution();

	var screen_dpi = getDPI(); // dpi
	var map_px = map_size;
	var map_aspect_ratio = map_px[0]/map_px[1];

	var img_dpi = 225; // dpi
	var dpi_scale_factor = img_dpi/screen_dpi;

	print_paper_border_mm = 6 // min 6mm border
	// print_paper_border_x_px = Math.round(print_paper_border_mm * img_dpi / 25.4)

	//var paper_mm = dims['a4']; // mm

	var landscape = map_aspect_ratio > 1;

	if(landscape) {
		var paper_mm = [ Math.max.apply(null, dims['a4']), Math.min.apply(null, dims['a4']) ]; // mm
		var orientation = 'landscape';
		//var img_mm_max = [ paper_mm[0] - 2 * print_paper_border_mm, paper_mm[1] - 2 * print_paper_border_mm ];
	} else {
		var paper_mm = [ Math.min.apply(null, dims['a4']), Math.max.apply(null, dims['a4']) ]; // mm
		var orientation = 'portrait';
		//var img_mm_max = [ paper_mm[0] - 2 * print_paper_border_mm, paper_mm[1] - 2 * print_paper_border_mm ];
	}
	// or if fails:
	// var paper_mm = [ Math.max(...dims['a4']), Math.min(...dims['a4']) ];

	//img_x_px = Math.round(paper_x_px - 2 * print_paper_border_x_px);
	//img_y_px = Math.round(img_x_px * dpi_scale_factor);



	// max image size is paper size excluding print frame
	var img_mm_max = [ paper_mm[0] - 2 * print_paper_border_mm, paper_mm[1] - 2 * print_paper_border_mm ];

	// determine real image size; TODO: depending on image orientation

	var img_px = [ (map_px[0] * dpi_scale_factor), (map_px[1] * dpi_scale_factor) ]

	// img_mm = [ Math.min(px_to_mm(img_px[0], img_dpi), img_mm_max[0]), Math.min(px_to_mm(img_px[1], img_dpi), img_mm_max[1]) ];
	if(landscape) {
		var img_x_mm = Math.min(px_to_mm(img_px[0], img_dpi), img_mm_max[0]);
		var img_mm = [ img_x_mm, img_x_mm/map_aspect_ratio ];
	} else {
		var img_y_mm = Math.min(px_to_mm(img_px[1], img_dpi), img_mm_max[1]);
		var img_mm = [ img_y_mm * map_aspect_ratio, img_y_mm ];
	}

	// image placement on paper
	img_d_mm = [ Math.max(((( paper_mm[0] - 2 * print_paper_border_mm) - img_mm[0]) / 2), 0),  Math.max(((( paper_mm[1] - 2 * print_paper_border_mm) - img_mm[1]) / 2), 0) ];

	//alert('img_mm ratio: ' + img_mm[0]/img_mm[1] );

	alert('paper_mm: ' + paper_mm + '\n img_mm_max: ' + img_mm_max + '\n img_mm: ' + img_mm + '\n' + '@' + img_dpi + '\n' + 'map_px: ' + map_px + '@scale = ' + screen_dpi + 'dpi\n' + 'img_mm_max: ' + img_mm_max + '\n' + 'img_d_mm: ' + img_d_mm  + '\n' + 'Aspect ratio: ' + map_aspect_ratio + '=>' + orientation + '\n Screen/Map dpi scale factor: ' + dpi_scale_factor);


	// Math.round(dpi_scale_factor * map_x_px / 25.4);
	// img_y_mm = Math.round(dpi_scale_factor * map_y_px / 25.4);

	//var width = Math.round((dim[0] * resolution) / 25.4);
	//var height = Math.round((dim[1] * resolution) / 25.4);

	//var size = map.getSize();
	var map_extent = map.getView().calculateExtent(map.getSize());

	//var mapPointResolution = ol.proj.getPointResolution(mapProjection, mapResolutionAtEquator, viewCenter);
	//var mapResolutionFactor = mapResolutionAtEquator / mapPointResolution;

	// alert('map: ' + map_x_px + '/' + map_y_px + 'px@' + screen_dpi + 'dpi\n' + 'img: ' + img_x_px + '/' + img_y_px + 'px@' + img_dpi + '\n paper: ' + paper_x_px + '/' + paper_y_px + 'px@' + img_dpi + 'dpi\n View resolution:' + map_px_resolution + '\n Map extent: ' + map_extent + '\n Map size: ' + map_size );

	map.once('rendercomplete', function () {

		var mapCanvas = document.createElement('canvas');
		mapCanvas.width = Math.round(img_px[0]); // mm_to_px(img_mm[0], img_dpi);
		mapCanvas.height = Math.round(img_px[1]); // mm_to_px(img_mm[1], img_dpi);

		// alert('image to: ' + mapCanvas.width + '/' + mapCanvas.height + 'px');

		var mapContext = mapCanvas.getContext('2d');

		Array.prototype.forEach.call(
			document.querySelectorAll('.ol-layer canvas'),
			function (canvas) {
				if (canvas.width > 0) {

					// var opacity = canvas.parentNode.style.opacity;
					//var opacity = 1;
					//mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);

					var transform = canvas.style.transform;
					// Get the transform parameters from the style's transform matrix
					var matrix = transform
						.match(/^matrix\(([^\(]*)\)$/)[1]
						.split(',')
						.map(Number);

					// Apply the transform to the export map context
					CanvasRenderingContext2D.prototype.setTransform.apply( mapContext, matrix);

					mapContext.drawImage(canvas, 0, 0);
				}
			}
		);

		// var pdf = new jsPDF('landscape', undefined, format);
		var pdf = new jsPDF({
			orientation: orientation,
			unit: 'mm', // "pt", "mm", "cm", "m", "in" or "px"
			// undefined,
			format: 'a4',
			compress: true,
			putOnlyUsedFonts: true
		});

		// addImage(imageData, format, x, y, width, height, alias, compression, rotation)
		// pdf.addImage(mapCanvas.toDataURL('image/jpeg'), 'JPEG', 0, 0, dim[0], dim[1]);


		// add the map to the center of the page
		pdf.addImage(mapCanvas.toDataURL('image/jpeg'), 'JPEG', print_paper_border_mm + img_d_mm[0], print_paper_border_mm + img_d_mm[1], img_mm[0], img_mm[1], 'map', 'MEDIUM', 0);

		// add some text
		pdf.setFontSize(8);

		const date_options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
		pdf.text((new Date()).toLocaleTimeString(undefined, date_options), print_paper_border_mm, paper_mm[1] - print_paper_border_mm)

		pdf.save('map.pdf');

		// Reset original map size
		map.setSize(map_size);
		//map.getView().setResolution(map_px_resolution);
		map.getView().fit(map_extent);

		// exportButton.disabled = false;
		document.body.style.cursor = 'auto';
	});

	// Set print size
	// var img_px_x = mm_to_px(img_mm[0], img_dpi); // ( img_mm[0] + 2 * print_paper_border_mm)  * img_dpi / 24.5
	// var printSize = img_px; // [ (img_px_x), (img_px_x/map_aspect_ratio) ];
	// alert('printSize: ' + printSize);
	map.setSize(img_px);

	// map.getView().setZoom(map.getView().getZoom() * dpi_scale_factor);

	// var zoom = map.getView().getZoom();
	var to_map_px_resolution = map_px_resolution / dpi_scale_factor;
	// alert(map_px_resolution + ' -> ' + to_map_px_resolution);

	map.getView().fit(map_extent);
	// map.getView().setZoom(to_zoom);

	// var scaling = Math.min(img_px[0] / map_size[0], img_px[1] / map_size[1]);
	map.getView().setResolution(to_map_px_resolution);


};


// add tooltip to pdfexport custom control
//$('.ol-zoom-in, .ol-zoom-out').tooltip({
//	placement: 'right'
//});



/*
$('.ol-rotate-reset, .ol-attribution button[title]').tooltip({
	placement: 'left'
});
*/
