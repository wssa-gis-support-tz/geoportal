from django.conf import settings
import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.DEBUG)


from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound

from django.db.models.fields import Field as django_model_field

from django.contrib.gis.shortcuts import render_to_kml

from geometries.models import get_requested_model_categories_or_none

from datetime import datetime

from .styles import get_style_def_or_default
from geometries.apps import get_html_parameter_or_none, set_rgba_opacity

from django.contrib.gis.geos import Point
import math
import zipfile
from io import BytesIO

import os

from django.template.loader import render_to_string

# from .apps import get_html_parameter_or_none, set_rgba_opacity

# from django.utils.translation import ugettext as _
from django.utils.translation import ugettext as _

from .styles import expand_classification_to_style # get_style_def_or_default

from PIL import Image, ImageDraw # icons on the fly

if settings.DEBUG:
	import time



def parse_labeling_definition_or_none(labelling, objekt, label_line_separator='\n'):
	"""
	Parses categorie's ['labelling'] definitions, as set up under geometries/db_views
	FIXME: speed critical, improve
	"""
	label_lines = []

	for line in labelling:

		# logger.debug('Check label line {}'.format(line))
		line_str = []

		for item in line:

			value = None

			# logger.debug('Label item: {} is of type {}'.format(item, type(item)))
			if isinstance(item, django_model_field) and item.value_from_object(objekt):
				value = str(item.value_from_object(objekt))
			elif isinstance(item, str):
				value = item

			# logger.debug('Value is {}'.format(value))

			if value: line_str.append(value)
			else:
				line_str = None
				break

		# logger.debug('line_str is: {}'.format(line_str))
		if line_str:
			# logger.debug('{} appended'.format(''.join(line_str)))
			label_lines.append(''.join(line_str))

	label = label_line_separator.join(label_lines)
	return label



def generate_kml(request, layer_slug, absolute_icon_urls=True, skip_style=False):
	"""
	Returns a dict with a KMl and it's proposed filename

	The KML
	- considers a design horizon; if not set then today - html parameter "design_horizon"
	- considers a given style - html parameter "style"

	absolute_icon_urls -> kml will have to use absolute urls, kmz allows to embed icons and refer to them in a relative way
	absolute_icon_urls = True works for both, but is not reliable over the different infrastructures (webmapping, google earth, no internet connection, ...)
	"""

	if settings.DEBUG: start_time = time.time()

	# logger.debug('Request KML for table "{}", geometry_field "{}"'.format(layer_slug, geometry_field))


	# get html parameters
	design_horizon = get_html_parameter_or_none(request, 'design_horizon', datetime.now().year, 'integer' )
	category_slug = get_html_parameter_or_none(request, 'category', None, 'text') # FIXME: take category variable name from settings

	# check if the requested layer is available
	# layer_name = name_the_layer(table_name, geometry_field)
	potential_layer_category = get_requested_model_categories_or_none(layer_slug, category_slug, design_horizon)

	if not potential_layer_category:
		# this is fatal for creation of the requested KML the requested layer
		logger.error('KML requested Layer/Style combination "{}/{}" not found, skipping'.format(layer_slug, category_slug))
		return None


	template_name = 'kml/folders.kml'

	# set if icons that will be embedded into the KMZ
	kmz_embedded_icons = set()
	folders = []

	existing_only = get_html_parameter_or_none(request, 'existing_only', False, 'boolean')


	model = potential_layer_category['model']
	layer = potential_layer_category['layer']
	category = potential_layer_category['category']

	visible_layer_name = ' '.join([layer['name'], category['name']])

	# all classes are dropped into the same list of features

	for classification in category['classifications']:

		folder = layer['topic']
		layer_classes = []
		kml_folder_name = ' '.join([ layer['topic'], visible_layer_name ])

		expanded_classes = expand_classification_to_style( layer['slug'], category['slug'], classification, design_horizon )

		#for expanded_class in expanded_classes:
		#	logger.info('EXPANDED_CLASSE: {}'.format(expanded_class))

		for the_class in expanded_classes:

			features = []
			feature_query = the_class['queryset']

			# style for this class is set when querying the features, as the geometry type matters
			kml_style_definition = None

			# now inspect each geometry feature
			for objekt in feature_query:

				geometry = None
				name = None
				description = None
				year_start = None
				year_end = None

				# FIXME: geometry selection should be generic
				if layer['geometry_field'] == 'geom':
					geometry = objekt.geom

				elif layer['geometry_field'] == 'geom_proposed':
					geometry = objekt.geom_proposed

				else:
					logger.error('Requested geometry_field is "{}", which is not configured for layer "{}"'.format(geometry_field, visible_layer_name))
					return None

				if not geometry:
					# skip entries with empty geometries
					logger.info('No geometries in {}, geometry_field: {}'.format(objekt, layer['geometry_field']))
					continue


				# kml will use epsg: 4326
				geometry = geometry.transform(4326, clone=True)


				# a label must be defined in the class and could still fail
				#if 'label' in the_class and the_class['label']:
				if 'labelling' in category and category['labelling']:

					# do not use linebreak for LineString geometry labels
					label_line_separator = ' ' if geometry.dims == 1 else '\n'

					# do not show any part of the label, if value is None
					#name = separator.join(''.join( (lambda e: e if not None in e else '')([
					#		#(lambda e: (str(e.value_from_object(objekt)) if e.value_from_object(objekt) else None) if not isinstance(e, str) else e)(e)
					#		(str(e.value_from_object(objekt)) if e.value_from_object(objekt) else None) if not isinstance(e, str) else e
					#		for e in the_label_tuple if e
					#		])
					#	) for the_label_tuple in category['labelling'] if the_label_tuple
					#)

					# name = separator.join('{}'.format(e if isinstance(e, str) else e.value_from_object(objekt)) for e in category['labelling'] or '')


					# label is a list of rows, that contains of a list of line elements
					# if any element of a line is null, the entire line will be skipped.

					# label = None
					name = parse_labeling_definition_or_none(category['labelling'], objekt, label_line_separator=label_line_separator)
					# logger.debug('Final Label: {}'.format(name))

					#name = separator.join(
					#	line for line in
					#	(
					#		item for item in category['labelling'] if isinstance(item, str) # else item.value_from_object(objekt)
					#	)
					#	# e if isinstance(e, str) else e.value_from_object(objekt)) for e in category['labelling'] or ''
					#)

				if hasattr(model, 'the_description'):
					description = model.the_description(objekt)

				# for KML timeline
				if hasattr(objekt, 'year_start'):
					year_start = objekt.year_start

				if hasattr(objekt, 'year_end'):
					year_end = objekt.year_end

				feature = {
					'name': name,
					'description': description,
					'geometry': geometry.kml,
					'dims': geometry.dims, # dimensions; geometry.geom_type for verbose
					'style_id': the_class['slug'],
					'year_start': year_start,
					'year_end': year_end,
				}

				features.append(feature)

			layer_classes.append({
				'name': the_class['name'],
				'description': the_class['name'] + ' (hardcoded fake class description)',
				'features': features,
				'style_description_kml': kml_style_definition,
			})

		# one folder for each base_class (before splitting to past, current, future)
		folder = {
			'name': kml_folder_name,
			'description': 'Hardcoded default folder description',
			'layers': layer_classes, # we submid the classes of a style as a "layer"!
			}

		folders.append(folder)

	context = {
		'name': ' '.join([settings.ORGANIZATION_NAME_ABBREVIATED, 'infrastructure', str(design_horizon) ]),
		'folders': folders,
	}


	# determine a proposed filename
	organization = settings.ORGANIZATION_NAME_ABBREVIATED

	style_name = category['name'].lower().replace(' ', '_')
	date_string = datetime.now().strftime('%Y%m%d')

	# filename = 'SUWASA_2020_water_tanks_geom_by_structure_20200422.kmz'
	filename = '_'.join(list(filter(None, [organization, str(design_horizon) if design_horizon else None, layer_slug, style_name, date_string])))

	response = {
		'kml_file': render_to_string(template_name, context),
		'file_name': filename, # download file name
		'icons': kmz_embedded_icons, # icons listed here will be copied into the kmz
	}

	if settings.DEBUG: logger.info('--- Generating the KML took {} msec ({})---'.format(((time.time() - start_time) * 1000), layer_slug))

	return response



def compress(kml_string, icons = []):
	"""
	Return compressed KMZ from the given KML string.
	The KML is submitted as string, the icon_paths is a list of URLs (text) to the icons
	"""
	kmz = BytesIO()

	icons_subdir = 'files'

	with zipfile.ZipFile(kmz, 'a', zipfile.ZIP_DEFLATED) as zf:

		# zip the kml as "doc.kml"
		zf.writestr('doc.kml', kml_string.encode(settings.DEFAULT_CHARSET))

		# embed the icons
		for icon in set(icons):

			def find_a_single_icon():
				"""
				Embedded function to skip out of nested for when the first icon was found
				"""
				for static_dir in settings.STATICFILES_DIRS:

					# find the file in the file system (static dirs) and add it, if found
					for root, dirs, files in os.walk(static_dir):

						for the_file in files:

							#if the_file.startswith(icon):
							if icon == the_file:

								# found_icon_dir, found_icon_name = os.path.split(icon)
								logger.debug('Icon file "{}" found under "{}"'.format(icon, files))
								return {
									'fs_path': os.path.join(root, the_file),
									'zip_path': os.path.join(icons_subdir, the_file),
								}

				return None

			found_icon = find_a_single_icon()

			if found_icon:
				# in the zip file, the icon is placed flat; right unter /files/
				zf.write(found_icon['fs_path'], found_icon['zip_path'])
			else:
				logger.warn('Requested icon "{}" could not be found under "{}"'.format(icon, settings.STATICFILES_DIRS))

		# fix for linux files under windows?
		for file_entry in zf.filelist:
			file_entry.create_system = 0

	kmz.seek(0)

	response = {
		'zipfile': kmz.read(),
	}

	return response



# fixme: turn into class based view
def get_kml(request, layer_slug):
	"""
	Returns a KML of the requested table name/geometry (= layer)
	Icons will be referred by absolute URLs, so take care the path can be reached from the target client computer
	"""
	kml = generate_kml(request, layer_slug, absolute_icon_urls=True)
	logger.debug('KML generated!')

	# ['Content-Disposition'] = 'attachment'
	http_response = HttpResponse(kml['kml_file'], content_type='application/vnd.google-earth.kml+xml',)
	http_response['Content-Disposition'] = 'inline; filename="{}"'.format(kml['file_name'] + '.kml')

	return http_response



# fixme: turn into class based view
def get_kmz(request, layer_slug):
	"""
	Returns a KMZ of the requested table name/geometry (= layer)
	The KMZ will include the referred icons
	"""
	kml = generate_kml(request, layer_slug, absolute_icon_urls=False)

	icons = kml['icons']

	archive = compress(kml['kml_file'], icons)
	logger.debug('KMZ generated!')

	# ['Content-Disposition'] = 'attachment'
	http_response = HttpResponse(archive['zipfile'], content_type='application/vnd.google-earth.kmz', )
	http_response['Content-Disposition'] = 'inline; filename="{}"'.format(kml['file_name'] + '.kmz')

	return http_response



def make_frame(feature_outline_color, feature_outline_width, dash_line=None, dash_space=None):

	return [
		(0.1, 0.1, 0.9, 0.1, feature_outline_color, feature_outline_width, dash_line, dash_space),
		(0.9, 0.1, 0.9, 0.9, feature_outline_color, feature_outline_width, dash_line, dash_space),
		(0.9, 0.9, 0.1, 0.9, feature_outline_color, feature_outline_width, dash_line, dash_space),
		(0.1, 0.9, 0.1, 0.1, feature_outline_color, feature_outline_width, dash_line, dash_space),
	]




# def serve_icon(request, layer_slug, category_slug, classification_slug, style_slug):
def serve_icon(request, layer_slug, category_slug, style_slug, image_format='png'):
	"""
	Returns a icon for the requested style, suitable for the legend and mapping symbol
	Pass optional "x_size" and "y_size" HTML parameters for target icon size
	"""

	"""
	Step one: extract the requested style
	"""

	# requested_layer_name = name_the_layer(table_name, geometry_field)
	potential_layer_category = get_requested_model_categories_or_none(layer_slug, category_slug, design_horizon=None)

	# Barrier 1: Wrong request
	if not potential_layer_category:
		# this is fatal for creation of the requested KML the requested layer
		msg = 'Requested Layer/Style combination "{}/{}" not found, skipping'.format(layer_slug, category_slug)
		logger.error(msg)
		return HttpResponseNotFound('<h1>{}</h1>'.format(msg))


	model = potential_layer_category['model']
	layer = potential_layer_category['layer']
	category = potential_layer_category['category']

	dims = layer['dims']

	style = None
	base_color = None
	circle = {}

	# find the matching classification
	for classification in category['classifications']:

		# probe for matching style_slug
		for the_style in expand_classification_to_style(layer['slug'], category['slug'], classification, design_horizon=None):

			#logger.error('{} != == {}'.format(the_style['slug'], style_slug))

			if the_style['slug'] == style_slug:

				# WE have now identified the requested style:
				style = the_style

				# every style should have a base_color defined
				# as the "classification" is only accessible from within this loop, capture it now!
				base_color = classification['base_color'] if 'base_color' in classification else None

				# stop when the style was found
				break

		# no further iterations over classifications if we found our style already
		if style:
			break


	# Barrier 2: No or empty style found
	if not style:
		msg = 'Configuration error - Style "{}" not found or empty'.format(style_slug)
		logger.error(msg)
		return HttpResponseNotFound('<h1>{}</h1>'.format(msg))


	"""
	At this point, the target syle is identified
	Step two: create the icon by parsing the symbolizers
	"""

	# return variable initialization
	#feature_fill_color = None
	#feature_outline_color = None
	#feature_outline_width = None
	#feature_stroke_width = None
	#feature_stroke_color = None
	#icon = None

	# define target icon size by parsing HTML parameters (x_size and y_size) and otherwise use default from settings
	icon_size_x = get_html_parameter_or_none(request, 'x_size', settings.GUI_ICON_SIZE_PX, 'integer' )
	icon_size_y = get_html_parameter_or_none(request, 'y_size', settings.GUI_ICON_SIZE_PX, 'integer' )

	proposed_color = '#008800ff'
	decomissioned_color = '#FF0000ff'

	# ul, lr, color, line_width, dash_line, dash_space
	red_crossout = [
		(0.1, 0.1, 0.9, 0.9, decomissioned_color, 2, None, None),
		(0.1, 0.9, 0.9, 0.1, decomissioned_color, 2, None, None),
	]

	# ul, lr, color, line_width, dash_line, dash_space
	proposed_frame = [
		(0, 0, 1, 0, proposed_color, 1, None, None),
		(1, 0, 1, 1, proposed_color, 1, None, None),
		(1, 1, 0, 1, proposed_color, 1, None, None),
		(0, 1, 0, 0, proposed_color, 1, None, None),
	]

	response = HttpResponse(content_type="image/{}".format(image_format))
	list_of_line_tuples = []

	# default values
	icon = None
	icon_opacity = 1
	backfill_color = None

	if dims == 0:
		"""
		"icon" smybolizer, currently allowes
		- 'img': the icon stored in settings.STATIC_DIR and
		- 'fill': the backfill color
		- 'frame_color': color of a frame
		- 'frame_width': width of a frame
		- 'frame_dash_line': length of a dash line for frame
		- 'frame_dash_space': distance of dashed lines for frame
		"""

		radius = get_style_def_or_default(style['style_defs'], 'icon', 'radius', default=None)

		if radius:
			circle_fill = get_style_def_or_default(style['style_defs'], 'icon', 'circle_fill', default=set_rgba_opacity(base_color, 0.5 ))
			outline_color = get_style_def_or_default(style['style_defs'], 'icon', 'circle_outline_color', default=base_color)
			outline_width = get_style_def_or_default(style['style_defs'], 'icon', 'circle_outline_width', default=1)

			circle = {'radius': radius, 'circle_fill': circle_fill, 'outline_color': outline_color, 'outline_width': outline_width, }


		"""
		Put image icon
		"""

		icon = get_style_def_or_default(style['style_defs'], 'icon', 'img', default=None)

		# FIXME: better handling of image SOURCE format
		if icon: icon = '{}.{}'.format(icon, 'png')

		backfill_color = get_style_def_or_default(style['style_defs'], 'icon', 'fill', default=None)

		frame_color = get_style_def_or_default(style['style_defs'], 'icon', 'frame_color', default=None)
		frame_width = get_style_def_or_default(style['style_defs'], 'icon', 'frame_width', default=None)
		frame_dash_line = get_style_def_or_default(style['style_defs'], 'icon', 'frame_dash_line', default=None)
		frame_dash_space = get_style_def_or_default(style['style_defs'], 'icon', 'frame_dash_space', default=None)

		if frame_color or frame_width:
			list_of_line_tuples += make_frame(frame_color, frame_width, frame_dash_line, frame_dash_space)

		# for a planned/proposed feature; add a frame to the icon and turn the icon itseld 50% transparent
		if style['status'] > 0:
			list_of_line_tuples += proposed_frame
			icon_opacity = settings.PROPOSED_FEATURES_OPACITY

		# for a decomissioned feature; turn the icon itseld 50% transparent and cross it out
		elif style['status'] < 0:
			list_of_line_tuples += red_crossout
			icon_opacity = settings.DECOMISSIONED_FEATURES_OPACITY


	if dims == 1:
		"""
		"line" smybolizer, currently allowes
		- 'frame_color': color of a frame
		- 'frame_width': width of a frame
		- 'width': width of the line
		- 'color': color of the line
		- 'dash_line': N/A, as used for status indicator!
		- 'dash_space': N/A, as used for status indicator!

		"""

		stroke_color = get_style_def_or_default(style['style_defs'], 'line', 'color', default=base_color)
		stroke_width = get_style_def_or_default(style['style_defs'], 'line', 'width', default=5) # FIXME: fake "5"

		dash_space = None
		dash_line = None

		# FIXME: not DRY: requires manual syncronize with openlayers.py
		# for a planned/proposed feature; make the line dashed; make the the line dotted and 50% transparent
		if style['status'] > 0:
			dash_space = 3
			dash_line = 3
			stroke_color = set_rgba_opacity(stroke_color, settings.PROPOSED_FEATURES_OPACITY)


		# FIXME: not DRY: requires manual syncronize with openlayers.py
		# for a decomissioned feature; make the the line dotted and 50% transparent
		elif style['status'] < 0:
			dash_space = 3
			dash_line = 1
			stroke_color = set_rgba_opacity(stroke_color, settings.DECOMISSIONED_FEATURES_OPACITY)

		horizontal_line = [(0, 0.5, 1, 0.5, stroke_color, stroke_width, dash_line, dash_space),]
		list_of_line_tuples += horizontal_line


	# TODO: a polygon is requested
	if dims == 2:
		"""

		"""

		# not DRY: requires sync with openlayers.py
		backfill_color = get_style_def_or_default(style['style_defs'], 'polygon', 'fill', default=None)

		stroke_color = get_style_def_or_default(style['style_defs'], 'polygon', 'color', default=base_color)
		stroke_width = get_style_def_or_default(style['style_defs'], 'polygon', 'width', default=5) # FIXME: fake "5"

		if stroke_color and stroke_width:

			dash_space = None
			dash_line = None

			# FIXME: not DRY: requires manual syncronize with openlayers.py
			# for a planned/proposed feature; make the line dashed; make the the line dotted and 50% transparent
			if style['status'] > 0:
				dash_space = 3
				dash_line = 3
				stroke_color = set_rgba_opacity(stroke_color, 0.5)

			# FIXME: not DRY: requires manual syncronize with openlayers.py
			# for a decomissioned feature; make the the line dotted and 50% transparent
			elif style['status'] < 0:
				dash_space = 3
				dash_line = 1


			polygon_frame = make_frame(stroke_color, stroke_width, dash_line, dash_space)
			list_of_line_tuples += polygon_frame


	return create_icon_response(response, x_size=icon_size_x, y_size=icon_size_y, backfill_color=backfill_color or backfill_color, icon=icon, list_of_line_tuples=list_of_line_tuples, image_format=image_format, icon_opacity=icon_opacity, circle=circle )



def create_icon_response(response, x_size, y_size, backfill_color=None, icon=None, list_of_line_tuples=[], image_format='png', icon_opacity=1, circle={} ):
	"""
	Returns the composed icon
	"""

	source_img = BytesIO()
	img = Image.new('RGBA', (x_size, y_size))
	draw = ImageDraw.Draw(img)


	# first: the backfill, if any
	if backfill_color:

		draw.rectangle([(0,0), (x_size-1, y_size-1)], fill=backfill_color)


	# second: draw a centered circle, if defined
	if circle and 'radius' in circle and circle['radius'] and circle['radius'] > 0:

		scale_factor = ((x_size / settings.GUI_ICON_SIZE_PX + y_size / settings.GUI_ICON_SIZE_PX) / 2)

		real_radius = round(circle['radius'] * scale_factor)
		real_width = round(circle['outline_width'] * scale_factor)

		ul_x = round(x_size/2 - real_radius - real_width)
		ul_y = round(y_size/2 - real_radius - real_width)
		lr_x = round(x_size/2 + real_radius + real_width)
		lr_y = round(y_size/2 + real_radius + real_width)

		# logger.info('radius: {} circle_fill: {}'.format(real_radius, circle['circle_fill']))

		# {'radius': None, 'fill', 'outline_color': None, 'outline_width': None, }
		draw.ellipse((ul_x, ul_y, lr_x, lr_y), fill=circle['circle_fill'], width=real_width, outline=circle['outline_color'])



	# third: the icon, if any
	if icon:

		# the local path to the icon
		icon_path = os.path.join(settings.STATIC_ROOT, settings.MAP_ICON_STATIC_SUBPATH, icon)
		original_icon = Image.open(icon_path).convert("RGBA")

		# resize original icon
		overlay_icon = original_icon.resize((x_size, y_size))
		alpha_icon = overlay_icon.copy()
		alpha_icon.putalpha(round(icon_opacity * 256))
		#overlay_icon = Image.blend(img, overlay_icon, 0.5)

		# paste image to center of the blank icon
		sx, sy = overlay_icon.size
		dx = round((x_size - sx)/2)
		dy = round((y_size - sy)/2)

		# paste image with transparent background
		img.paste(alpha_icon, (dx, dy), overlay_icon )


	# forth: line symbols in sequence of appearance (status decoration usually at last), if any
	# list_of_line_tuples = [ ( x1, y0, x1, y1, line_color=None, line_width=None, ), ... ]
	for x_y_width_color_tuple in list_of_line_tuples:

		# logger.info(x_y_width_color_tuple)

		width = x_y_width_color_tuple[5]
		color = x_y_width_color_tuple[4]

		# Barrier: every line requires a width and color
		if not width or not color: break

		# if a larger/smaller icon then default is requested, scale all given pixel sizes for drawn lines by this factor which compares the actual size to the standard size
		line_scale_factor = (x_size / settings.GUI_ICON_SIZE_PX + y_size / settings.GUI_ICON_SIZE_PX) / 2
		width = round(line_scale_factor * width)
		# logger.info('{} vs {}'.format((x_y_width_color_tuple[5]), (round(line_scale_factor * width))))

		# x, y coordinates as percent of image size
		# coordinates start counting from zero, so substract one
		# FIXME: for even number of width, the frame in x-direction is too small
		x_ul = round(max(x_y_width_color_tuple[0] * (x_size - 1 ), 0 ))
		y_ul = round(max(x_y_width_color_tuple[1] * (y_size - 1 ), 0 ))
		x_lr = round(min(x_y_width_color_tuple[2] * (x_size - 1 ), (x_size - 1 ) ))
		y_lr = round(min(x_y_width_color_tuple[3] * (y_size - 1 ), (y_size - 1 ) ))

		dash_space = x_y_width_color_tuple[7]
		dash_line = x_y_width_color_tuple[6]

		# TODO: wrap the following into a punchy function
		# FIXME: weird error, when icon is larger then about 50 px. try out with http://elito:8000/geometry/icon/water_tanks_geom/by_structure/water_tanks_geom_unknown_structure_distance_from_ground_to_low_water_level_not_set_existing/?x_size=60&y_size=60
		if dash_space and dash_line:

			# FIXME: the relation of lengthes (space vs. point) is not fully ok; most likely due to end cap (check with a 1:1 dotted line)
			# not sure why, but with addig/substracting "width/5". it looks the best
			dash_line = dash_line * width - width/5 if dash_line else None
			dash_space = dash_space * width + width/5 if dash_space else None

			xmin = min(x_ul, x_lr)
			xmax = max(x_ul, x_lr)
			ymin = min(y_ul, y_lr)
			ymax = max(y_ul, y_lr)

			# min max ranges
			rx=xmax-xmin
			ry=ymax-ymin

			if rx != 0:
				angle = math.atan(ry/rx)
			else:
				angle = math.pi/2

			dx_line = round(math.cos(angle) * dash_line, 2)
			dy_line = round(math.sin(angle) * dash_line, 2)

			dx_space = math.cos(angle) * dash_space
			dy_space = math.sin(angle) * dash_space

			x = xmin
			y = ymin

			# iterate to draw the requested dashed line
			while x <= xmax and y <= ymax:

				# logger.debug('{}|{} ({}-{})'.format(x, y, dx_line, dy_line))
				# pillow bug https://github.com/python-pillow/Pillow/issues/4579
#				draw.line((x, y, x + dx_line, y + dy_line), fill=color, width=width, joint='curve')
				draw.line((x, y, x + dx_line, y + dy_line), fill=color, width=width)

				x += dx_line + dx_space
				y += dy_line + dy_space

		else:
			# logger.error('{}, {} - {}, {} - {} {}'.format(x_ul, y_ul, x_lr, y_lr, color, width))

			# pillow bug https://github.com/python-pillow/Pillow/issues/4579
			# draw.line((x_ul, y_ul, x_lr, y_lr), fill=color, width=width, joint='curve')
			draw.line((x_ul, y_ul, x_lr, y_lr), fill=color, width=width)


	img.save(response, image_format)
	del draw

	# now the response contains the image
	return response
