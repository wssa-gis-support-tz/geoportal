from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL) # settings.DEFAULT_LOG_LEVEL; INFO

from django.contrib.gis.db import models

from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from geometries.queries import DefaultQuerySets
from geometries.layers import get_layer_or_none
from geometries.models import get_impl_period_str
from geometries import categories
from geometries.classifications import create_classification, icon_specs, line_specs, polygon_specs, circle_specs, label_specs, default_label_style, by_age
from geometries.queries import query_for_icontains_in_field


class water_fittings(models.Model):
	type = models.CharField(max_length=64)
	label = models.CharField(max_length=64, blank=True, null=True)
	specs = models.CharField(max_length=64, blank=True, null=True)
	azimuth = models.FloatField()
	# operator = models.CharField(max_length=64, blank=True, null=True)
	year_start = models.IntegerField(blank=True, null=True)
	year_end = models.IntegerField(blank=True, null=True)
	remark = models.TextField(blank=True, null=True)
	geom = models.GeometryField()

	# FIXME: get rid of this definition to keep the entire model_base generic and put it under models instead
	objects = DefaultQuerySets.as_manager()

	class Meta:
		# TODO: localize table names; has side effects to html parameters
		verbose_name = 'Water fitting' # TODO: use as legend title
		verbose_name_plural = _('Water fittings')
		managed = False
		db_table = 'water_fittings_view'

	objects = DefaultQuerySets.as_manager()
	layers = []


	def __str__(self):
		"""
		String representation
		"""
		return self._meta.verbose_name + ' ' + self.the_label()


	def the_label(self):
		"""
		String representation for visualization
		"""
		return str(self.label)


	def the_description(self):
		"""
		Detailed information when querying a single feature at map
		"""

		elements = []

		impl_period = get_impl_period_str(self.year_start, self.year_end)

		if impl_period:
			elements.append(impl_period)

		if elements:
			description = '\n'.join(elements)
			return description
		else:
			return ''


	@classmethod
	def the_layers(cls, design_horizon):
		"""
		Each geometry field in a table is a separate layer
		"""

		if not cls.layers:

			geom_layer = get_layer_or_none(cls,
				topic=_('Water Supply'),
				geometry_field='geom',
				description=_('All sort of electro-mechanical (valves, meters, ...) and passive (bends, tees, ...) fittings'),
				list_of_data_tables = ['BulkMeter', 'Fitting', 'Valve', 'Hydrant', ],
				allow_zoom_to_extents=True,
			)

			# Barrier 0: interrupt if there is no geometry
			if not geom_layer: return None

			geom_layer['categories'] = [

				categories.get_category(cls, geom_layer, cls.by_fitting_type(geom_layer['geometry_field']),
					label=_('by type'),
					description=_('Fittings by their type'),
					labelling=[
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']), ],
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']),],
						],
					min_scale=None, max_scale=3 ),

				categories.get_category(cls, geom_layer, by_age(cls, geom_layer['geometry_field'], design_horizon),
					label=_('by age'),
					description=_('Show features by their age; from date of installation until the current design horizon.'),
					labelling=[
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']), ],
					],
					min_scale=None, max_scale=3 ),
				]

			cls.layers = [geom_layer, ]

			for layer in cls.layers:
				logger.info('Layer with slug "{}" defined'.format(layer['slug']) )

		return cls.layers



	@classmethod
	def by_fitting_type(cls, geometry_field, base_color=settings.WATER_SUPPLY_BASE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		"""
		"""

		endcaps_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['end', 'plug'] )
		endcaps = create_classification(
				label=_('End cap'),
				description = endcaps_query['description'],
				queryset = endcaps_query['queryset'],
				base_color = '#000000ff',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2, 'circle_fill': '#ffffffaa'}),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		saddles_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['saddle', ] )
		saddles = create_classification(
				label=_('Saddle'),
				description = saddles_query['description'],
				queryset = saddles_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 3 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		bend_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['bend', 'corner', 'elbow'] )
		bend = create_classification(
				label=_('Bend'),
				description = bend_query['description'],
				queryset = bend_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		junction_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['junction', 'tee' ] )
		junction = create_classification(
				label=_('Junction'),
				description = junction_query['description'],
				queryset = junction_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		av_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['air', 'av',] )
		av = create_classification(
				label=_('Air valve'),
				description = av_query['description'],
				queryset = av_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_fittings_air_valve', 'anchor_x': 0.5, 'anchor_y': 0.875 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		wo_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['wash', 'wo', 'flush'] )
		wo = create_classification(
				label=_('Wash out'),
				description = wo_query['description'],
				queryset = wo_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_fittings_washout', 'anchor_x': 0.5, 'anchor_y': 0.125 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		reducer_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['bm', 'bulk'] )
		bm = create_classification(
				label=_('Bulk meter'),
				description = reducer_query['description'],
				queryset = reducer_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_fittings_meter' }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		iv_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['isolati', 'sluice', 'gate', 'sv', 'gv', 'control'] )
		iv = create_classification(
				label=_('Isolation valve'),
				description = iv_query['description'],
				queryset = iv_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_fitting_isolation_valve', }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		hydrants_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['fh', 'hydrant', ] )
		hydrants = create_classification(
				label=_('Hydrants'),
				description = hydrants_query['description'],
				queryset = hydrants_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_fittings_hydrant', 'anchor_x': 0.5, 'anchor_y': 0.875 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		reducer_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['reducer', 'taper', ] )
		reducer = create_classification(
				label=_('Reducer'),
				description = reducer_query['description'],
				queryset = reducer_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_fitting_reducer' }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		unknown = create_classification(
				label=_('Undefined fitting type'),
				description=_('Features shown here miss a value in the "{}" field').format(base_field_name),
				queryset =  cls.objects.using('geodatabase').is_not_null(geometry_field).is_null(base_field_name),
				base_color = settings.UNKNOWN_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': 'water_unknown', 'frame_color': settings.UNKNOWN_STYLE_COLOR, 'frame_width': 2, 'frame_dash_line':1, 'frame_dash_space':1 }),
					**label_specs( { **default_label_style(settings.UNKNOWN_STYLE_COLOR), })
				}
			)


		other = create_classification(
				label=_('Other fittings'),
				description=_('Features shown here have their "{}" field set to a (yet) unknown value. This might be that the values do not correspond to the data model OR the value requires a new classification to be defined.').format(base_field_name),
				queryset = cls.objects.using('geodatabase').is_not_null(geometry_field).other_then([
						unknown['queryset'],
						av['queryset'],
						wo['queryset'],
						bm['queryset'],
						bend['queryset'],
						hydrants['queryset'],
						junction['queryset'],
						reducer['queryset'],
						iv['queryset'],
						endcaps['queryset'],
						saddles['queryset'],
					], geometry_field),
				base_color = settings.OTHER_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': 'water_unknown', }),
					**label_specs( { **default_label_style(settings.OTHER_STYLE_COLOR), })
				}
			)

		return [endcaps, bend, saddles, junction, reducer, hydrants, av, wo, bm, iv, other, unknown,  ]
