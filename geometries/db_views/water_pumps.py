from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL) # settings.DEFAULT_LOG_LEVEL; INFO

from django.contrib.gis.db import models

from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from geometries.queries import DefaultQuerySets
from geometries.layers import get_layer_or_none
from geometries.models import get_impl_period_str
from geometries import categories

from geometries.classifications import create_classification, icon_specs, line_specs, polygon_specs, circle_specs, label_specs, default_label_style, by_age
from geometries.queries import query_for_icontains_in_field



class water_pumps(models.Model):
	"""
	An extension to the plain generic data model
	"""

	#type = models.CharField(max_length=64, blank=True, null=True, help_text='')
	label = models.CharField(max_length=64, blank=True, null=True, help_text='')
	#yld_cbm_d = models.FloatField(blank=True, null=True, help_text='')
	# head_gnd_m = models.FloatField(blank=True, null=True, help_text='')
	# hours_per_d = models.FloatField(blank=True, null=True, help_text='')
	# operator = models.CharField(max_length=64, blank=True, null=True, help_text='')
	year_start = models.IntegerField(blank=True, null=True, help_text='')
	year_end = models.IntegerField(blank=True, null=True, help_text='')
	remark = models.TextField(blank=True, null=True, help_text='')
	geom = models.GeometryField()

	class Meta:
		# TODO: localize table names
		verbose_name = 'Water pump' # TODO: use as legend title
		verbose_name_plural = _('Water pumps')
		managed = False
		db_table = 'water_pumps_view'

	objects = DefaultQuerySets.as_manager()
	layers = []


	def __str__(self):
		"""
		String representation
		"""
		return self._meta.verbose_name + ' ' + self.the_label()


	def the_label(self):
		"""
		String representation for visualization
		"""
		return str(self.label)


	def the_description(self):
		"""
		Detailed information when querying a single feature at map
		"""

		elements = []
		#if self.type:
		#	elements.append(_('Type: {}').format(self.type))

		#if self.yld_cbm_d:
		#	elements.append(_('Yield: {} cbm/d').format(self.yld_cbm_d))

		#if self.hours_per_d:
		#	elements.append(_('Operation hours: {} per day').format(self.hours_per_d))

		#if self.grnd2lwl_m:
		#	elements.append(_('Water table to ground: {} metre').format(self.grnd2lwl_m))

		impl_period = get_impl_period_str(self.year_start, self.year_end)

		if impl_period:
			elements.append(impl_period)

		if elements:
			description = '\n'.join(elements)
			return description
		else:
			return ''


	@classmethod
	def the_layers(cls, design_horizon=None):
		"""
		Each geometry field in a table is a separate layer
		"""

		if not cls.layers:

			geom_layer = get_layer_or_none(cls,
				topic=_('Water Supply'),
				geometry_field='geom',
				description=_('Pumps that are relevant for water supply: booster pumps, low lift pumps, etc.'),
				list_of_data_tables = ['WaterPump',],
				allow_zoom_to_extents=True,
			)

			# Barrier 0: interrupt if there is no geometry
			if not geom_layer: return None

			geom_layer['categories'] = [

				categories.get_category(cls, geom_layer, cls.by_pump_type('geom', settings.WATER_SUPPLY_BASE_COLOR),
					label=_('by type'),
					description=_('TODO: separate pumps typ type; e.g. their pumping capacity.'),
					labelling=[
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']), ],
						],
					min_scale=None, max_scale=3 ),

				categories.get_category(cls, geom_layer, by_age(cls, 'geom', design_horizon),
					label=_('by age'),
					description=_('Show features by their age at the requested design horizon'),
					labelling=[ # each of the list in this list defines one row in the feature's label.
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['YEAR_START']), ' *' , ], # 2001 *
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['YEAR_END']), ' †' , ], # 2019 †
						],
					min_scale=None, max_scale=None ),
			]

			cls.layers = [geom_layer, ]

			for layer in cls.layers:
				logger.info('Layer with slug "{}" defined'.format(layer['slug']) )

		return cls.layers



	@classmethod
	def by_pump_type(cls, geometry_field,base_color=settings.WATER_SUPPLY_BASE_COLOR):

		# the label's contents is composed by each of the list's entries; separated by linebreaks
		# each line of the label consists of a prefix, a value and a postfix
		# FIXME: processing of the value required an instance of the class and is processed by the view class. Result is limited formatting capabilities
		#default_label = [
		#	(None, cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']), None),
	#
	#			#(cls._meta.get_field('label'),) if 'label' in [f.name for f in cls._meta.get_fields()] else None,
	#			# (cls._meta.get_field('type'),) if 'type' in [f.name for f in cls._meta.get_fields()] else None,
	#			# ('x', cls._meta.get_field('year_end'), None) if 'year_end' in [f.name for f in cls._meta.get_fields()] else None,
	#		]

		all_pumps = create_classification(
			label=_('All pumps'),
			queryset = cls.objects.using('geodatabase'),
			base_color = base_color,
			style_defs = {
					**icon_specs({ 'img': 'water_pump-booster', }),
					# **circle_specs({ 'radius': None, 'fill': None, 'color': None, 'width': None, }),
					# **line_specs({ 'width': 1, 'dash_line': None, 'dash_space': None, }),
					# **polygon_specs({ 'fill': settings.UNKNOWN_STYLE_COLOR }), # { 'fill': settings.UNKNOWN_STYLE_COLOR }
					**label_specs( { **default_label_style(base_color), })
				}
			)


		#unknown = {
		#	'name': 'Unknown water pump', # Will be used for legend as well as as the kml style name, so the slugified string, concatenated with verbose_name, should be unique over all models
		#	'queryset': cls.objects.using('geodatabase').is_not_null(geometry_field).is_null('type'),
		#	'label': default_label,
		#	'icon': 'water_unknown', # base icon name. Will be used for legend and kml files
		#	'color': settings.OTHER_STYLE_COLOR,
		#	'style_defs':  {
		#		# when icon is set, the icon href will be added automatically to point geometries
		#		#'IconStyle': [ ('scale', 3), ],
		#		# for a polygon, "LineStyle" and "PolyStyle" will be used, if present.
		#		#'LineStyle': [ ('width', '1'), ],
		#		#'PolyStyle': [ ('width', '1'), ],
		#		# a label style will be added, if present
		#		'LabelStyle': [ ],
		#	}
		#}

		#other = {
		#	'name': 'Other type of water source', # Will be used for legend as well as as the kml style name, so the slugified string, concatenated with verbose_name, should be unique over all models
		#	'queryset': cls.objects.using('geodatabase').is_not_null(geometry_field).other_then([unknown['queryset'],], geometry_field),
		#	'label': default_label,
		#	'icon': 'water_unknown', # base icon name. Will be used for legend and kml files
		#	'color': settings.OTHER_STYLE_COLOR,
		#	'style_defs': {
		#		# when icon is set, the icon href will be added automatically to point geometries
		#		# 'IconStyle': [ ('scale', 3), ],
		#		# for a polygon, "LineStyle" and "PolyStyle" will be used, if present.
		#		# 'LineStyle': [ ('color',settings.CONSUMER_LINE_STYLE_COLOR), ('width', '1'), ],
		#		# 'PolyStyle': [ ('color',settings.CONSUMER_LINE_STYLE_COLOR), ],
		#		# a label style will be added, if present
		#		'LabelStyle': [ ],
		#	}
		#}

		return [ all_pumps ]
