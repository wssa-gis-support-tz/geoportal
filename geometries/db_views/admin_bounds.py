from django.conf import settings
import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.INFO)

from django.contrib.gis.db import models

from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from geometries.queries import DefaultQuerySets
from geometries.layers import get_layer_or_none
from geometries.models import get_impl_period_str
from geometries import categories

from geometries.classifications import create_classification, icon_specs, line_specs, polygon_specs, circle_specs, label_specs, default_label_style, by_age
from geometries.queries import query_for_icontains_in_field

from geometries.apps import set_rgba_opacity


class admin_bounds(models.Model):
	"""
	An extension to the plain generic data model
	"""

	adm_level = models.IntegerField(blank=True, null=True, help_text='')
	adm_label = models.CharField(max_length=64, blank=True, null=True, help_text='')
	label = models.CharField(max_length=64, blank=True, null=True, help_text='')
	year_start = models.IntegerField(blank=True, null=True, help_text='')
	year_end = models.IntegerField(blank=True, null=True, help_text='')

	geom = models.GeometryField()

	objects = DefaultQuerySets.as_manager()
	layers = []


	class Meta:
		# TODO: localize table names
		verbose_name = 'Administrative boundary' # TODO: use as legend title
		verbose_name_plural = _('Administrative boundaries')
		managed = False
		db_table = 'admin_bounds_view'


	def __str__(self):
		"""
		String representation
		"""
		return self._meta.verbose_name + ' ' + self.the_label()


	def the_label(self):
		"""
		String representation
		"""
		return ' '.join(list(filter(None, [self.label, self.adm_label ])))


	def the_description(self):
		"""
		Detailed information when querying a single feature at map
		"""
		return ''


	@classmethod
	def the_layers(cls, design_horizon=None):
		"""
		Each geometry field in a table is a separate layer
		"""

		if not cls.layers:

			geom_layer = get_layer_or_none(cls,
				topic=_('External references'),
				label=_('Administrative Boundaries'), # FIXME: how to parse "cls._meta.verbose_name_plural" with "manage.py makemessages"?
				geometry_field='geom',
				description=str(_('Administrative boundaries')) + ' ' + str(_('of Tanzania')) + ' (' + str(_('maybe imprecise')) + '!)',
				list_of_data_tables = ['District', 'Region', 'WardShehia', ],
				allow_zoom_to_extents=False,
			)


			# Barrier 0: interrupt if there is no geometry
			if not geom_layer: return None


			geom_layer['categories'] = [

				categories.get_category(cls, geom_layer, cls.by_regions(geom_layer['geometry_field'], settings.ADMIN_BOUNDS_STYLE_COLOR),
					label=_('Regions') ,
					description=' '.join([str(_('Regions')), str(_('of Tanzania'))]),
					labelling=[
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']),],
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['ADMIN_LEVEL_FIELDNAME']),],
					],
					min_scale=None, max_scale=None ),

				categories.get_category(cls, geom_layer, cls.by_districts(geom_layer['geometry_field'], settings.ADMIN_BOUNDS_STYLE_COLOR),
					label=_('Districts'),
					description=' '.join([str(_('Regions')), str(_('of Tanzania'))]),
					labelling=[
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']),],
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['ADMIN_LEVEL_FIELDNAME']),],
					],
					min_scale=None, max_scale=250 ),

				categories.get_category(cls, geom_layer, cls.by_wards(geom_layer['geometry_field'], settings.ADMIN_BOUNDS_STYLE_COLOR),
					label=_('Wards/Shehias'),
					description=' '.join([str(_('Regions')), str(_('of Tanzania'))]),
					labelling=[
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']),],
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['ADMIN_LEVEL_FIELDNAME']),],
					],
					min_scale=None, max_scale=250 ),
			]


			cls.layers = [geom_layer, ]

			for layer in cls.layers:
				logger.info('Layer with slug "{}" defined'.format(layer['slug']) )

		return cls.layers


	@classmethod
	def by_wards(cls, geometry_field, base_color=settings.ADMIN_BOUNDS_STYLE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['ADMIN_LEVEL_FIELDNAME']):
		"""
		The administration boundaries
		"""

		wards_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['ward', 'shehia'] )
		wards = create_classification(
			label=_('Wards/Shehias'), # FIXME: determine from name
			description = wards_query['description'],
			queryset = wards_query['queryset'],
			base_color = base_color,
			style_defs = {
					# **icon_specs({ 'img': None, }),
					# **circle_specs({ 'radius': None, 'fill': None, 'color': None, 'width': None, }),
					# **line_specs({ 'width': 1, 'dash_line': None, 'dash_space': None, }),
					**polygon_specs({ 'width': 1, 'fill': set_rgba_opacity('#FFFFFF00', 1 - settings.BACKGROUND_IMAGE_OPACITY)  }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		return [wards]


	@classmethod
	def by_districts(cls, geometry_field, base_color=settings.ADMIN_BOUNDS_STYLE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['ADMIN_LEVEL_FIELDNAME']):
		"""
		The administration boundaries
		"""

		districts_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['district'] )
		districts = create_classification(
			label=_('Districts'),
			description = districts_query['description'],
			queryset = districts_query['queryset'],
			base_color = base_color,
			style_defs = {
					# **icon_specs({ 'img': None, }),
					# **circle_specs({ 'radius': None, 'fill': None, 'color': None, 'width': None, }),
					# **line_specs({ 'width': 2, 'dash_line': None, 'dash_space': None, }),
					**polygon_specs({ 'width': 2, 'fill': set_rgba_opacity('#FFFFFF00', 1 - settings.BACKGROUND_IMAGE_OPACITY) }), # { 'fill': settings.UNKNOWN_STYLE_COLOR }
					**label_specs( { **default_label_style(base_color), })
				}
			)

		return [districts]


	@classmethod
	def by_regions(cls, geometry_field, base_color=settings.ADMIN_BOUNDS_STYLE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['ADMIN_LEVEL_FIELDNAME']):
		"""
		The administration boundaries
		"""

		regions_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['region'] )
		regions = create_classification(
			label=_('Regions'),
			description = regions_query['description'],
			queryset = regions_query['queryset'],
			base_color = base_color,
			style_defs = {
					# **icon_specs({ 'img': None, }),
					# **circle_specs({ 'radius': None, 'fill': None, 'color': None, 'width': None, }),
					#**line_specs({ 'width': 4, 'dash_line': None, 'dash_space': None, }),
					**polygon_specs({ 'width': 4, 'fill': set_rgba_opacity('#FFFFFF00', 1 - settings.BACKGROUND_IMAGE_OPACITY) }), # { 'fill': settings.UNKNOWN_STYLE_COLOR }
					**label_specs({ **default_label_style(base_color), })
				}
			)

		return [regions]
