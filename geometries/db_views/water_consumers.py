from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL) # settings.DEFAULT_LOG_LEVEL; INFO

from django.contrib.gis.db import models

from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from geometries.queries import DefaultQuerySets
# from geometries.classifications import Classifications
from geometries.layers import get_layer_or_none
from geometries.models import get_impl_period_str
from geometries import categories
from django.db.models import Q
from geometries.queries import query_for_icontains_in_field


from geometries.classifications import create_classification, icon_specs, line_specs, polygon_specs, circle_specs, label_specs, default_label_style, by_age


class water_consumers(models.Model):
	label = models.CharField(max_length=64, blank=True, null=True)
	tariff = models.CharField(max_length=64, blank=True, null=True)
	#demd_cbm_d = models.FloatField(blank=True, null=True)
	#demd_pattn = models.CharField(max_length=64, blank=True, null=True)
	#operator = models.CharField(max_length=64, blank=True, null=True)
	year_start = models.IntegerField(blank=True, null=True)
	year_end = models.IntegerField(blank=True, null=True)
	remark = models.TextField(blank=True, null=True)
	geom = models.GeometryField()

	# FIXME: get rid of this definition to keep the entire model_base generic and put it under models instead
	objects = DefaultQuerySets.as_manager()

	class Meta:
		managed = False
		verbose_name = 'Water consumer' # TODO: use as legend title
		verbose_name_plural = _('Water consumers')
		managed = False
		db_table = 'water_consumers_view'

	objects = DefaultQuerySets.as_manager()
	layers = []


	def __str__(self):
		"""
		String representation
		"""
		return '{} {}'.format(self._meta.verbose_name, self.the_label())


	def the_label(self):
		"""
		String representation for visualization
		"""
		return str(self.label)


	def the_description(self):
		"""
		Detailed information when querying a single feature at map
		"""

		elements = []

		impl_period = get_impl_period_str(self.year_start, self.year_end)

		if impl_period:
			elements.append(impl_period)

		if elements:
			description = '\n'.join(elements)
			return description
		else:
			return ''


	@classmethod
	def the_layers(cls, design_horizon=None):
		"""
		Each geometry field in a table is a separate layer
		"""

		if not cls.layers:

			geom_layer = get_layer_or_none(cls,
				topic=_('Water Supply'),
				geometry_field='geom',
				description=_('Consumers of water. This layer has NO SYNC/LINK to the billing system.'),
				list_of_data_tables = ['CustomerConn', 'WaterKiosk', 'WaterPoint',],
				allow_zoom_to_extents=True,
			)

			# Barrier 0: interrupt if there is no geometry
			if not geom_layer: return None

			geom_layer['categories'] = [

				categories.get_category(cls, geom_layer, cls.by_tariff(geom_layer['geometry_field']),
					label=_('by tariff'),
					description=_('Consumers by their tariff'),
					labelling=[
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['TARIFF_FIELDNAME'])],
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']),],
						],
					min_scale=None, max_scale=3 ),

				categories.get_category(cls, geom_layer, by_age(cls, geom_layer['geometry_field'], design_horizon),
					label=_('by age'),
					description=_('Show features by their age at the requested design horizon'),
					labelling=[
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['YEAR_START']), ' *', ],# 2001 *
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['YEAR_END']), ' †',], # 2019 †
					],
					min_scale=None, max_scale=10 ),
				]

			cls.layers = [geom_layer, ]

			for layer in cls.layers:
				logger.info('Layer with slug "{}" defined'.format(layer['slug']) )

		return cls.layers



	@classmethod
	def by_tariff(cls, geometry_field, base_color=settings.WATER_SUPPLY_BASE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['TARIFF_FIELDNAME']):
		"""
		"""

		commercial_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['com', ] )
		commercial = create_classification(
				label=_('Commercial tariff'),
				description = commercial_query['description'],
				queryset = commercial_query['queryset'],
				base_color = '#0000ffff',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		domestic_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['dom', ] )
		domestic = create_classification(
				label=_('Domestic tariff'),
				description = domestic_query['description'],
				queryset = domestic_query['queryset'],
				base_color = '#008800ff',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		water_kiosk_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['kio', ] )
		water_kiosk = create_classification(
				label=_('Water kiosk'),
				description = water_kiosk_query['description'],
				queryset = water_kiosk_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_consumers_water_kiosk', 'anchor_x': 0.5, 'anchor_y': 0.8125 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		industrial_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['ind', ] )
		industrial = create_classification(
				label=_('Industrial tariff'),
				description = industrial_query['description'],
				queryset = industrial_query['queryset'],
				base_color = '#aaaa00ff',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		institutional_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['ins', ] )
		institutional = create_classification(
				label=_('Institutional tariff'),
				description = institutional_query['description'],
				queryset = institutional_query['queryset'],
				base_color = '#ff0000ff',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		unknown = create_classification(
				label=_('Undefined tariff'),
				description=_('Features shown here miss a value in the "{}" field').format(base_field_name),
				queryset =  cls.objects.using('geodatabase').is_not_null(geometry_field).is_null(base_field_name),
				base_color = settings.UNKNOWN_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': 'water_unknown', 'frame_color': settings.UNKNOWN_STYLE_COLOR, 'frame_width': 2, 'frame_dash_line':1, 'frame_dash_space':1 }),
					**label_specs( { **default_label_style(settings.UNKNOWN_STYLE_COLOR), })
				}
			)


		other = create_classification(
				label=_('Other tariff'),
				description=_('Features shown here have their "{}" field set to a (yet) unknown value. This might be that the values do not correspond to the data model OR the value requires a new classification to be defined.').format(base_field_name),
				queryset = cls.objects.using('geodatabase').is_not_null(geometry_field).other_then([
						commercial['queryset'],
						domestic['queryset'],
						water_kiosk['queryset'],
						industrial['queryset'],
						institutional['queryset'],
						unknown['queryset'],
					], geometry_field),
				base_color = settings.OTHER_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': 'water_unknown', }),
					**label_specs( { **default_label_style(settings.OTHER_STYLE_COLOR), })
				}
			)


		return [ commercial, domestic, water_kiosk, industrial, institutional, other, unknown ]


	# TODO
	@classmethod
	def by_connection_status(cls, geometry_field, base_color=settings.WATER_SUPPLY_BASE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['TARIFF_FIELDNAME']):

		def disconnected(self, geometry_field):
			"""
			Customers: inactive customer
			"""
			return self.is_not_null(geometry_field).filter(disconnected=True)


		def active(self, geometry_field):
			"""
			Customers: active customer
			"""
			return self.is_not_null(geometry_field).filter(disconnected=False)
