from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL) # settings.DEFAULT_LOG_LEVEL; INFO

from django.contrib.gis.db import models

from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from geometries.queries import DefaultQuerySets
from geometries.layers import get_layer_or_none
from geometries.models import get_impl_period_str
from geometries import categories

from geometries.classifications import create_classification, icon_specs, line_specs, polygon_specs, circle_specs, label_specs, default_label_style, by_age
from geometries.queries import query_for_icontains_in_field, query_for_field_value_between



class sewer_conduits(models.Model):
	"""
	An extension to the plain generic data model
	"""

	# mandantory for all layers
	label = models.CharField(max_length=64, blank=True, null=True, help_text='')
	year_start = models.IntegerField(blank=True, null=True, help_text='')
	year_end = models.IntegerField(blank=True, null=True, help_text='')
	geom = models.GeometryField()

	scheme = models.CharField(max_length=64, blank=True, null=True, help_text='')
	project = models.CharField(max_length=64, blank=True, null=True, help_text='')
	purpose = models.CharField(max_length=64, blank=True, null=True, help_text='')
	diam_label = models.CharField(max_length=64, blank=True, null=True, help_text='')
	material = models.CharField(max_length=64, blank=True, null=True, help_text='')

	#gnd_otlt_m = models.FloatField(blank=True, null=True, help_text='')
	#gnd_inlt_m = models.FloatField(blank=True, null=True, help_text='')
	#profile = models.CharField(max_length=64, blank=True, null=True, help_text='')
	#roughness = models.FloatField(blank=True, null=True, help_text='')
	#minorloss = models.FloatField(blank=True, null=True, help_text='')
	#operator = models.CharField(max_length=64, blank=True, null=True, help_text='')
	di_mm = models.IntegerField(blank=True, null=True, help_text='')
	# year_start = models.IntegerField(blank=True, null=True, help_text='')
	# year_end = models.IntegerField(blank=True, null=True, help_text='')
	remark = models.TextField(blank=True, null=True, help_text='')
	# geom = models.GeometryField()

	objects = DefaultQuerySets.as_manager()
	layers = []

	class Meta:
		# TODO: localize table names
		verbose_name = 'Sewer conduit' # TODO: use as legend title
		verbose_name_plural = _('Sewer conduits')
		managed = False
		db_table = 'sewer_conduits_view'


	def __str__(self):
		"""
		String representation
		"""
		return self._meta.verbose_name + ' ' + self.the_label()


	def the_label(self):
		"""
		String representation
		"""
		return ' '.join(list(filter(None, [self.material, self.diam_label ])))


	def the_description(self):
		"""
		Detailed information when querying a single feature at map
		"""

		elements = []
		elements.append(str(self))

		if self.purpose:
			elements.append(_('{} pipe').format(self.purpose))

		if self.scheme:
			elements.append(_('Schema: {}').format(self.scheme))

		if self.project:
			elements.append(_('Implemented under: "{}" project').format(self.project))

		impl_period = get_impl_period_str(self.year_start, self.year_end)

		if impl_period:
			elements.append(impl_period)

		if elements:
			description = '\n'.join(elements)
			return description
		else:
			return ''


	@classmethod
	def the_layers(cls, design_horizon=None):
		"""
		Each geometry field in a table is a separate layer
		"""

		if not cls.layers:

			geom_layer = get_layer_or_none(cls,
				topic=_('Sanitation'),
				geometry_field='geom',
				description=_('All sort of sewer pipework'),
				list_of_data_tables = ['Sewerline',],
				allow_zoom_to_extents=True,
			)

			# Barrier 0: interrupt if there is no geometry
			if not geom_layer: return None

			geom_layer['categories'] = [

				categories.get_category(cls, geom_layer, cls.by_pipe_size('geom', settings.SEWER_TRUNK_LINE_STYLE_COLOR),
					label=_('by size'),
					description=_('Show pipes by their size'),
					labelling=[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']), ],
					min_scale=None, max_scale=250 ),

				categories.get_category(cls, geom_layer, cls.by_hierarchy('geom'),
					label=_('by hierarchy'),
					description=_('by hierarchy'),
					labelling=[
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['PURPOSE_FIELDNAME']),],
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']),],
						],
					min_scale=None, max_scale=250 ),

				categories.get_category(cls, geom_layer, by_age(cls, 'geom', design_horizon),
					label=_('by age'),
					description=_('Show features by their age at the requested design horizon'),
					labelling=[ # each of the list in this list defines one row in the feature's label.
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['YEAR_START']), ' *',],  # 2001 *
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['YEAR_END']), ' †',],  # 2019 †
						],
					min_scale=None, max_scale=250 ),
			]

			cls.layers = [geom_layer, ]

			for layer in cls.layers:
				logger.info('Layer with slug "{}" defined'.format(layer['slug']) )

		return cls.layers



	@classmethod
	def by_hierarchy(cls, geometry_field, base_color=settings.UNKNOWN_STYLE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['PURPOSE_FIELDNAME']):
		"""
		Water and sewer pipes "purpose" fields
		"""

		tertiary_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['tertiary', 'customer', 'service', ] )
		tertiary = create_classification(
				label=_('Tertiary (Service line)'),
				description = tertiary_query['description'],
				queryset = tertiary_query['queryset'],
				base_color = settings.SEWER_LATERAL_LINE_STYLE_COLOR,
				style_defs = {
					**line_specs({'width': 2, }),
					**label_specs( { **default_label_style(settings.SEWER_LATERAL_LINE_STYLE_COLOR), })
				}
			)


		secondary_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['secondary', 'lateral' ] )
		secondary = create_classification(
				label=_('Secondary (Lateral pipe)'),
				description = secondary_query['description'],
				queryset = secondary_query['queryset'],
				base_color = settings.SEWER_TRUNK_LINE_STYLE_COLOR,
				style_defs = {
					**line_specs({ 'width': 2, }),
					**label_specs( { **default_label_style(settings.SEWER_TRUNK_LINE_STYLE_COLOR), })
				}
			)


		primary_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['primary', 'trunk', ] )
		primary = create_classification(
				label=_('Primary (Trunk main)'),
				description = primary_query['description'],
				queryset = primary_query['queryset'],
				base_color = settings.SEWER_TRUNK_LINE_STYLE_COLOR,
				style_defs = {
					**line_specs( {'width': 4, }),
					**label_specs( { **default_label_style(settings.SEWER_TRUNK_LINE_STYLE_COLOR), })
				}
			)


		unknown = create_classification(
				label=_('Undefined'),
				description=_('Features shown here miss a value in the "{}" field').format(base_field_name),
				queryset = cls.objects.using('geodatabase').is_not_null(geometry_field).is_null(base_field_name),
				base_color = settings.UNKNOWN_STYLE_COLOR,
				style_defs = {
					**line_specs({'width': 2, }),
					**label_specs( { **default_label_style(settings.UNKNOWN_STYLE_COLOR), })
				}
			)


		other = create_classification(
				label=_('Other'),
				description=_('Features shown here have their "{}" field set to a (yet) unknown value. This might be that the values do not correspond to the data model OR the value requires a new classification to be defined.').format(base_field_name),
				queryset = cls.objects.using('geodatabase').other_then([
						unknown['queryset'],
						secondary['queryset'],
						tertiary['queryset'],
						primary['queryset'],
					], geometry_field),
				base_color = settings.OTHER_STYLE_COLOR,
				style_defs = {
					**line_specs({ 'width': 2, }),
					**label_specs( { **default_label_style(settings.OTHER_STYLE_COLOR), })
				}
			)


		return [primary, secondary, tertiary, other, unknown]


	@classmethod
	def by_pipe_size(cls, geometry_field, base_color, base_field_name=settings.DB_VIEW_FIELDNAMES['INNER_DIAMETER_FIELDNAME']):
		"""
		Classification by pipe size; applies to water supply and sanitation pipework.
		Therefore, a base_color parameter is mandantory
		"""

		tiny_query = query_for_field_value_between(cls.objects.using('geodatabase'), field_name=base_field_name, minimum=None, maximum=settings.PIPE_DIAMETER_SMALL_MM)
		tiny = create_classification(
			label=_('Pipes of tiny diameters'),
			description = tiny_query['description'],
			queryset = tiny_query['queryset'],
			base_color = base_color,
			style_defs = {
					**line_specs({ 'width': 1, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		small_query = query_for_field_value_between(cls.objects.using('geodatabase'), field_name=base_field_name, minimum=settings.PIPE_DIAMETER_SMALL_MM, maximum=settings.PIPE_DIAMETER_MEDIUM_MM)
		small = create_classification(
			label=_('Pipes of small diameters'),
			description = small_query['description'],
			queryset = small_query['queryset'],
			base_color = base_color,
			style_defs = {
					**line_specs({ 'width': 2, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		medium_query = query_for_field_value_between(cls.objects.using('geodatabase'), field_name=base_field_name, minimum=settings.PIPE_DIAMETER_MEDIUM_MM, maximum=settings.PIPE_DIAMETER_LARGE_MM)
		medium = create_classification(
			label=_('Pipes of medium diameters'),
			description = medium_query['description'],
			queryset = medium_query['queryset'],
			base_color = base_color,
			style_defs = {
					**line_specs({ 'width': 3, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		large_query = query_for_field_value_between(cls.objects.using('geodatabase'), field_name=base_field_name, minimum=settings.PIPE_DIAMETER_LARGE_MM, maximum=settings.PIPE_DIAMETER_HUGE_MM)
		large = create_classification(
			label=_('Pipes of large diameters'),
			description = large_query['description'],
			queryset = large_query['queryset'],
			base_color = base_color,
			style_defs = {
					**line_specs({ 'width': 5, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		huge_query = query_for_field_value_between(cls.objects.using('geodatabase'), field_name=base_field_name, minimum=settings.PIPE_DIAMETER_HUGE_MM)
		huge = create_classification(
			label=_('Pipes of huge diameters'),
			description = huge_query['description'],
			queryset = huge_query['queryset'],
			base_color = base_color,
			style_defs = {
					**line_specs({ 'width': 8, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		unknown = create_classification(
			label=_('Pipes of unknown diameters'),
			description=_('Features shown here miss a value in the "{}" field').format(base_field_name),
			queryset = cls.objects.using('geodatabase').is_not_null(geometry_field).is_null(base_field_name),
			base_color = settings.UNKNOWN_STYLE_COLOR,
			style_defs = {
					**line_specs({ 'width': 2, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		other = create_classification(
			label=_('Pipes of other diameters'),
			description=_('Features shown here have their "{}" field set to a (yet) unknown value. This might be that the values do not correspond to the data model OR the value requires a new classification to be defined.').format(base_field_name),
			queryset = cls.objects.using('geodatabase').is_not_null(geometry_field).other_then([
					unknown['queryset'],
					huge['queryset'],
					large['queryset'],
					medium['queryset'],
					small['queryset'],
					tiny['queryset'],
				], geometry_field),
			base_color = settings.OTHER_STYLE_COLOR,
			style_defs = {
					**line_specs({ 'width': 2, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		# return order matters and equals to the order in the picklist in layer selector
		return [ tiny, small, medium, large, huge, other, unknown ]
