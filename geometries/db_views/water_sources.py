from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL) # settings.DEFAULT_LOG_LEVEL; INFO

from django.contrib.gis.db import models

from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from geometries.queries import DefaultQuerySets
# from geometries.classifications import Classifications
from geometries.layers import get_layer_or_none
from geometries.models import get_impl_period_str
from geometries import categories

from geometries.classifications import create_classification, icon_specs, line_specs, polygon_specs, circle_specs, label_specs, default_label_style, by_age
from geometries.queries import query_for_icontains_in_field



class water_sources(models.Model):
	"""
	An extension to the plain generic data model
	"""

	type = models.CharField(max_length=64, blank=True, null=True, help_text='')
	label = models.CharField(max_length=64, blank=True, null=True, help_text='')
	yld_cbm_d = models.FloatField(blank=True, null=True, help_text='')
	# head_gnd_m = models.FloatField(blank=True, null=True, help_text='')
	# hours_per_d = models.FloatField(blank=True, null=True, help_text='')
	# operator = models.CharField(max_length=64, blank=True, null=True, help_text='')
	year_start = models.IntegerField(blank=True, null=True, help_text='')
	year_end = models.IntegerField(blank=True, null=True, help_text='')
	remark = models.TextField(blank=True, null=True, help_text='')
	geom = models.GeometryField()


	class Meta:
		# TODO: localize table names
		verbose_name = 'Water source' # TODO: use as legend title
		verbose_name_plural = _('Water sources')
		managed = False
		db_table = 'water_sources_view'

	objects = DefaultQuerySets.as_manager()
	layers = []


	def __str__(self):
		"""
		String representation
		"""
		return '{} {}'.format(self._meta.verbose_name, self.the_label())


	def the_label(self):
		"""
		String representation for visualization
		"""
		return str(self.label)


	def the_description(self):
		"""
		Detailed information when querying a single feature at map
		"""

		elements = []
		if self.type:
			elements.append(_('Type: {}').format(self.type))

		if self.yld_cbm_d:
			elements.append(_('Yield: {} cbm/d').format(self.yld_cbm_d))

		#if self.hours_per_d:
		#	elements.append(_('Operation hours: {} per day').format(self.hours_per_d))

		#if self.grnd2lwl_m:
		#	elements.append(_('Water table to ground: {} metre').format(self.grnd2lwl_m))

		impl_period = get_impl_period_str(self.year_start, self.year_end)

		if impl_period:
			elements.append(impl_period)

		if elements:
			description = '\n'.join(elements)
			return description
		else:
			return ''


	@classmethod
	def the_layers(cls, design_horizon=None):
		"""
		Each geometry field in a table is a separate layer
		"""

		if not cls.layers:

			geom_layer = get_layer_or_none(cls,
				topic=_('Water Supply'),
				geometry_field='geom',
				description=_('Sources of water, either raw or treated'),
				list_of_data_tables = ['WaterSourcePt',],
				allow_zoom_to_extents=True,
			)

			# Barrier 0: interrupt if there is no geometry
			if not geom_layer: return None

			geom_layer['categories'] = [

				categories.get_category(cls, geom_layer, cls.by_water_source_type(geom_layer['geometry_field']),
					label=_('by type'),
					description=_('Show water sources by their type'),
					labelling=[
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['WATER_SOURCE_TYPE_FIELDNAME']), ],
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']), ],
						],
					min_scale=None, max_scale=None ),

				categories.get_category(cls, geom_layer, by_age(cls, geom_layer['geometry_field'], design_horizon),
					label=_('by age'),
					description=_('Show features by their age at the requested design horizon'),
					labelling=[
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['YEAR_START']), ' *', ], # 2001 *
						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['YEAR_END']), ' †', ], # 2019 †
					],
					min_scale=None, max_scale=None ),
				]

			cls.layers = [geom_layer, ]

			for layer in cls.layers:
				logger.info('Layer with slug "{}" defined'.format(layer['slug']) )

		return cls.layers



	@classmethod
	def by_water_source_type(cls, geometry_field, base_color=settings.WATER_SUPPLY_BASE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['WATER_SOURCE_TYPE_FIELDNAME']):
		"""
		"""
		borehole_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['bh', 'borehole', ] )
		borehole = create_classification(
				label=_('Borehole with electric pump'),
				description = borehole_query['description'],
				queryset = borehole_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_source-borehole_electrified', }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		intake_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['dam','spring', 'intake',] )
		intake = create_classification(
				label=_('Surface water intake'),
				description = intake_query['description'],
				queryset = intake_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_source_dam', }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		wtp_query = query_for_icontains_in_field(cls.objects.using('geodatabase'), field_name=base_field_name, icontains_list=['wtp', 'treatment'] )
		wtp = create_classification(
				label=_('Water treatment plant'),
				description = wtp_query['description'],
				queryset = wtp_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_source_water_treatment', }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		unknown = create_classification(
				label=_('Undefined water source type'),
				description=_('Features shown here miss a value in the "{}" field').format(base_field_name),
				queryset =  cls.objects.using('geodatabase').is_not_null(geometry_field).is_null(base_field_name),
				base_color = settings.UNKNOWN_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': 'water_unknown', 'frame_color': settings.UNKNOWN_STYLE_COLOR, 'frame_width': 2, 'frame_dash_line':1, 'frame_dash_space':1 }),
					**label_specs( { **default_label_style(settings.UNKNOWN_STYLE_COLOR), })
				}
			)


		other = create_classification(
				label=_('Other type of water source'),
				description=_('Features shown here have their "{}" field set to a (yet) unknown value. This might be that the values do not correspond to the data model OR the value requires a new classification to be defined.').format(base_field_name),
				queryset = cls.objects.using('geodatabase').is_not_null(geometry_field).other_then([
						unknown['queryset'],
						wtp['queryset'],
						borehole['queryset'],
						intake['queryset'],
					], geometry_field),
				base_color = settings.OTHER_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': 'water_unknown', }),
					**label_specs( { **default_label_style(settings.OTHER_STYLE_COLOR), })
				}
			)


		return [ borehole, intake, wtp, other, unknown ]
