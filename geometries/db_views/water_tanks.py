from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL) # settings.DEFAULT_LOG_LEVEL; INFO

from django.contrib.gis.db import models

from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from geometries.queries import DefaultQuerySets
from geometries.layers import get_layer_or_none
from geometries.models import get_impl_period_str
from geometries import categories
from geometries.classifications import create_classification, icon_specs, line_specs, polygon_specs, circle_specs, label_specs, default_label_style, by_age
from geometries.queries import query_for_icontains_in_field, query_for_field_value_between


class water_tanks(models.Model):
	"""
	The production model; on DB side this is a materialized view.
	"""

	label = models.CharField(max_length=64, blank=True, null=True, help_text='')
	volume_cbm = models.FloatField(blank=True, null=True, help_text='')
	grnd2lwl_m = models.FloatField(blank=True, null=True, help_text='')
	tnk_dpth_m = models.FloatField(blank=True, null=True, help_text='')
	#operator = models.CharField(max_length=64, blank=True, null=True, help_text='')
	year_start = models.IntegerField(blank=True, null=True, help_text='')
	year_end = models.IntegerField(blank=True, null=True, help_text='')
	remark = models.TextField(blank=True, null=True, help_text='')
	geom = models.GeometryField()

	class Meta:
		verbose_name = 'Water tank' # TODO: use as legend title
		verbose_name_plural = _('Water tanks')
		managed = False
		db_table = 'water_tanks_view'

	objects = DefaultQuerySets.as_manager()
	layers = []


	def __str__(self):
		"""
		String representation
		"""
		return self._meta.verbose_name + ' ' + self.the_label()


	def the_label(self):
		"""
		String representation for visualization
		"""
		return str(self.label)


	def the_description(self):
		"""
		Detailed information when querying a single feature at map
		"""

		elements = []
		if self.volume_cbm:
			elements.append(_('Volume: {} cubic metre').format(self.volume_cbm))

		if self.grnd2lwl_m:
			elements.append(_('Low water level: {} metre to ground').format(self.grnd2lwl_m))

		if self.tnk_dpth_m:
			elements.append(_('Basin depth: {} metre basin depth').format(self.tnk_dpth_m))

		impl_period = get_impl_period_str(self.year_start, self.year_end)

		if impl_period:
			elements.append(impl_period)

		if elements:
			description = '\n'.join(elements)
			return description
		else:
			return ''


	@classmethod
	def the_layers(cls, design_horizon):
		"""
		Each geometry field in a table is a separate layer
		"""

		if not cls.layers:

			geom_layer = get_layer_or_none(cls,
				topic=_('Water Supply'),
				geometry_field='geom',
				description=_('Man-made water storages, such as ground tanks, elevated tanks and pump sumps, but also break pressure tanks.'),
				list_of_data_tables=['StorageFacility', 'BreakPressTank',],
				allow_zoom_to_extents=True,
			)

			# Barrier 0: interrupt if there is no geometry
			if not geom_layer: return None

			geom_layer['categories'] = [

				categories.get_category(cls, geom_layer, cls.by_structure(geom_layer['geometry_field']),
					label=_('by structure'),
					description=_('Show tanks by their low water level to ground: if it\'s below ground, consider it as underground tank/pump sump, at a reasonable height over ground as an elevated tank. All other tanks are shown as ground tanks.'),
					labelling=[
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']), ] ,
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['TANK_CAPACITY_FIELDNAME']), ' m³'],
						],
					min_scale=None, max_scale=None ),

				categories.get_category(cls, geom_layer, by_age(cls, geom_layer['geometry_field'], design_horizon),
					label=_('by age'),
					description=_('Show features by their age at the requested design horizon'),
					labelling=[
							[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']), ],
						],
					min_scale=None, max_scale=None ),
				]

			cls.layers = [geom_layer, ]

			for layer in cls.layers:
				logger.info('Layer with slug "{}" defined'.format(layer['slug']) )

		return cls.layers



	@classmethod
	def by_structure(cls, geometry_field, base_color=settings.WATER_SUPPLY_BASE_COLOR, base_field_name=settings.DB_VIEW_FIELDNAMES['TANK_STRUCTURE_HEIGHT_FIELDNAME']):
		"""
		"""

		elevated_query = query_for_field_value_between(cls.objects.using('geodatabase'), field_name=base_field_name, minimum=3)
		elevated = create_classification(
				label=_('Elevated tank'),
				description = elevated_query['description'],
				queryset = elevated_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_tank-elevated', 'anchor_x': 0.5, 'anchor_y': 0.875 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		ground_query = query_for_field_value_between(cls.objects.using('geodatabase'), field_name=base_field_name, minimum=-1, maximum=3)
		ground = create_classification(
				label=_('Ground tank'),
				description = ground_query['description'],
				queryset = ground_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_tank-ground', }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		underground_query = query_for_field_value_between(cls.objects.using('geodatabase'), field_name=base_field_name, maximum=-1)
		underground = create_classification(
				label=_('Underground basin'),
				description = underground_query['description'],
				queryset = underground_query['queryset'],
				base_color = base_color,
				style_defs = {
					**icon_specs({ 'img': 'water_tank-underground', } ),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		unknown = create_classification(
				label=_('Unknown tank structure'),
				# description=_('Features shown here miss some attributes that are required for the choosen classification. For tanks: the field indicating "{}" is not set.').format(base_field_name),
				description=_('Features shown here miss a value in the "{}" field').format(base_field_name),
				queryset = cls.objects.using('geodatabase').other_then([
					elevated['queryset'],
					ground['queryset'],
					underground['queryset']
				], geometry_field),
				base_color = settings.UNKNOWN_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': 'water_tank-ground', 'frame_color': settings.UNKNOWN_STYLE_COLOR, 'frame_width': 2, 'frame_dash_line':1, 'frame_dash_space':1 } ),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		result = [underground, ground, elevated, unknown ]

		return result
