from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL) # settings.DEFAULT_LOG_LEVEL; INFO

from django.contrib.gis.db import models

from django.utils.translation import ugettext_lazy as _
from django.apps import apps

from geometries.queries import DefaultQuerySets
from geometries.layers import get_layer_or_none
from geometries.models import get_impl_period_str
from geometries import categories
from geometries.classifications import create_classification, icon_specs, line_specs, polygon_specs, circle_specs, label_specs, default_label_style, by_age
from geometries.queries import query_for_icontains_in_field


class hm_water_nodes(models.Model):
	pipe_count = models.IntegerField(blank=True, null=True)
	specs = models.CharField(max_length=64, blank=True, null=True)
	# azimuth = models.FloatField()
	# operator = models.CharField(max_length=64, blank=True, null=True)
	year_start = models.IntegerField(blank=True, null=True)
	year_end = models.IntegerField(blank=True, null=True)
	# remark = models.TextField(blank=True, null=True)
	geom = models.GeometryField()

	# FIXME: get rid of this definition to keep the entire model_base generic and put it under models instead
	objects = DefaultQuerySets.as_manager()

	class Meta:
		# TODO: localize table names; has side effects to html parameters
		verbose_name = 'Water network node' # TODO: use as legend title
		verbose_name_plural = _('Water network nodes')
		managed = False
		db_table = 'raw_water_nodes'

	objects = DefaultQuerySets.as_manager()
	layers = []


	def __str__(self):
		"""
		String representation
		"""
		return self._meta.verbose_name + ' ' + self.the_label()


	def the_label(self):
		"""
		String representation for visualization
		"""
		return str(self.label)


	def the_description(self):
		"""
		Detailed information when querying a single feature at map
		"""

		elements = []

		if elements:
			description = '\n'.join(elements)
			return description
		else:
			return ''


	@classmethod
	def the_layers(cls, design_horizon):
		"""
		Each geometry field in a table is a separate layer
		"""

		if not cls.layers:

			geom_layer = get_layer_or_none(cls,
				topic=_('Hydraulic modelling'),
				geometry_field='geom',
				description=_('Nodes for hydraulic modelling, derived from analyzing the spatial relation of the pipework.'),
				list_of_data_tables = ['WaterPipe', 'ServiceLine', ],
				allow_zoom_to_extents=True,
			)

			# Barrier 0: interrupt if there is no geometry
			if not geom_layer: return None

			geom_layer['categories'] = [

				categories.get_category(cls, geom_layer, cls.by_node_type(geom_layer['geometry_field']),
					label=_('by node type'),
					description=_('Hydraulic modelling nodes by their type = number of connected pipes'),
					labelling=[
# 							[ cls._meta.get_field('pipe_count'), ],
							[ cls._meta.get_field('specs'), '°'],
						],
					min_scale=None, max_scale=None ),

#				categories.get_category(cls, geom_layer, by_age(cls, geom_layer['geometry_field'], design_horizon),
#					name='by age',
#					description=_('Show features by their age; from date of installation until the current design horizon.'),
#					labelling=[
#						[ cls._meta.get_field(settings.DB_VIEW_FIELDNAMES['LABEL_FIELDNAME']), ],
#					],
#					min_scale=None, max_scale=3 ),
				]

			cls.layers = [geom_layer, ]

			for layer in cls.layers:
				logger.info('Layer with slug "{}" defined'.format(layer['slug']) )

		return cls.layers



	@classmethod
	def by_node_type(cls, geometry_field, base_color=settings.WATER_SUPPLY_BASE_COLOR, base_field_name='pipe_count'):
		"""
		"""

		endcaps_query = cls.objects.using('geodatabase').filter(pipe_count=1)
		endcaps = create_classification(
				label=_('End cap'),
				description = _('The end point of a pipe'),
				queryset = endcaps_query,
				base_color = '#000000aa',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2, 'circle_outline_width': 1, 'circle_fill': '#ffffffaa'}),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		two_pipes_query = cls.objects.using('geodatabase').filter(pipe_count=2)
		bends_query = two_pipes_query.filter(specs__gt=0)
		bends = create_classification(
				label=_('Bend'),
				description = _('A connection of two pipes at some degree'),
				queryset = bends_query,
				base_color = '#000000aa',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		# all other two-pipe-connections, but bends
		couplings_query = two_pipes_query.exclude(id__in=bends_query)
		couplings = create_classification(
				label=_('Coupling'),
				description = _('Streight connection of two pipes'),
				queryset = couplings_query,
				base_color = '#000000aa',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 2, 'circle_outline_width': 1, 'circle_outline_color': '#ffffff' }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		tees_query = cls.objects.using('geodatabase').filter(pipe_count=3)
		tees = create_classification(
				label=_('Tee/Wye'),
				description = _('The connection of exact three (3) pipes'),
				queryset = tees_query,
				base_color = '#0000ffaa',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 4 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)

		crossing_query = cls.objects.using('geodatabase').filter(pipe_count=4)
		crossing = create_classification(
				label=_('Crossing'),
				description = _('The connection of exact four (4) pipes'),
				queryset = crossing_query,
				base_color = '#aa0000aa',
				style_defs = {
					**icon_specs({ 'img': None, 'radius': 4 }),
					**label_specs( { **default_label_style(base_color), })
				}
			)


		unknown = create_classification(
				label=_('Undefined fitting type'),
				description=_('Features shown here miss a value in the "{}" field').format('pipe_count'),
				queryset =  cls.objects.using('geodatabase').is_not_null(geometry_field).is_null('pipe_count'),
				base_color = settings.UNKNOWN_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'radius': 3, 'frame_color': settings.UNKNOWN_STYLE_COLOR, 'frame_width': 2, 'frame_dash_line':1, 'frame_dash_space':1 }),
					**label_specs( { **default_label_style(settings.UNKNOWN_STYLE_COLOR), })
				}
			)


		other = create_classification(
				label=_('Other fittings'),
				description=_('Features shown here have their "{}" field set to a (yet) unknown value. This might be that the values do not correspond to the data model OR the value requires a new classification to be defined.').format(base_field_name),
				queryset = cls.objects.using('geodatabase').is_not_null(geometry_field).other_then([
						unknown['queryset'],
						endcaps['queryset'],
						bends['queryset'],
						couplings['queryset'],
						crossing['queryset'],
						tees['queryset'],
					], geometry_field),
				base_color = '#aa0000aa',
				style_defs = {
					**icon_specs({ 'radius': 4, }),
					**label_specs( { **default_label_style(settings.OTHER_STYLE_COLOR), })
				}
			)

		return [endcaps, couplings, bends, tees, crossing, other, unknown,  ]
