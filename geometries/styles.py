from django.conf import settings
import logging
logger = logging.getLogger(__name__)
# logger.setLevel(settings.DEFAULT_LOG_LEVEL)
logger.setLevel(logging.INFO)

import copy # deepcopy classes
from django.utils.translation import ugettext as _

from django.urls import reverse

from geometries.apps import get_internal_translation_of, slugify_javascript_var



def expand_icon_to_icon_url_or_none(style_def, layer_slug, category_slug, style_slug):
	"""
	Determines the URL the map/legend icon
	"""

	# if an icon is set for this style, determine it's URL
	#icon = get_style_def_or_default(style_def, 'icon', 'img', default=None)

	#if icon:

	# the icon under this link is dynamically created, not served statically
	icon_url = reverse('geometry:icon', kwargs={
		'layer_slug': layer_slug,
		'category_slug': category_slug,
		'style_slug': style_slug,
	})

	return icon_url

	# return None



def get_style_def_or_default(style_defs, symbolizer, key, default=None):
	"""
	Convinient access to the classification styles properties
	Result is the value of the requested key
	- OR None - if the requested symbolizer is not found (... and should not be used)
	- OR the default - if no valid value is set for the key
	"""

	# Barrier 1: if the symbolizer is not a key, then this symbolizer should not be used, even not with a default value
	if not symbolizer in style_defs:
		# logger.warn('Requested symbolizer {} not found, returning None, which means this symbolizer should not be used for this style'.format(symbolizer, symbolizer))
		return None

	style_def = style_defs[symbolizer]

	if key in style_def:
		logger.debug('Found value "{}" to key "{}" for style "{}"'.format(style_def[key], key, style_defs))
		return style_def[key]

	logger.debug('Key "{}" not found in symbolizer "{}", will return default value "{}"'.format(key, symbolizer, default))
	return default



def expand_classification_to_style(layer_slug, category_slug, classification, design_horizon):
	"""
	Here, each classification will be expanded to a fully-styelable feature.
	A design horizon is used to mofidy the query and separate past, current and future infrastructure.
	The result is a list of one (1) or three (3) styles, which basically equal the input classification expanded by some additional information.
	"""

	classification_modifications = []

	existing = {
		'label': _('Existing'),
		'name': 'Existing',
		'status': 0, # 0=existing
		} # existing_by

	decomissioned = {
		# 'queryset': classification['queryset'].removed_by(design_horizon),
		'label': _('Decomissioned'),
		'name': 'Decomissioned',
		'status': -1, # < 0 former/decomissioned/past
	} # removed_by

	proposed = {
		#'queryset': classification['queryset'].planned_by(design_horizon),
		'label': _('Proposed'),
		'name': 'Proposed',
		'status': 1, # > 0: proposed/planned/future
	} # planned_by

	#for modification_dict in classification_modifications:
	#	# for key, value in modification.items():
	#	modification_dict['name'] = get_internal_translation_of(modification_dict['label'])

	if design_horizon:
		existing['queryset'] = classification['queryset'].existing_by(design_horizon)
		proposed['queryset'] = classification['queryset'].planned_by(design_horizon)
		decomissioned['queryset'] = classification['queryset'].removed_by(design_horizon)

	classification_modifications.append(existing)
	classification_modifications.insert(0, decomissioned)
	classification_modifications.append(proposed)

	styles = []

	for modification in classification_modifications:

		# work on a copy with copies of all it's objects
		modified_style = copy.deepcopy(classification)

		# no design horizon is set e.g. when the style is only requested to display an icon
		if design_horizon:
			modified_style['queryset'] = modification['queryset'] # modify class query

		modified_style['name'] = '{} {}'.format(modified_style['name'], modification['name'])  # modify style label
		modified_style['label'] = '{} ({})'.format(modified_style['label'], modification['label'])  # modify style label
		modified_style['slug'] = slugify_javascript_var('_'.join([ layer_slug, modified_style['name'] ]))
		modified_style['status'] = modification['status']

		# set icon's URL as part of the style
		modified_style['icon_url'] = expand_icon_to_icon_url_or_none(modified_style['style_defs'], layer_slug, category_slug, modified_style['slug'])

		# logger.debug('Modified style: {}'.format(modified_style))

		styles.append(modified_style)

	return styles
