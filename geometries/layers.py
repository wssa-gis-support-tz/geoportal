from django.conf import settings
import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.INFO)

from django.utils.translation import ugettext_lazy as _

from geometries.apps import get_internal_translation_of, slugify_javascript_var


def get_crs_and_dims_from_first_geometry_or_none(cls, geometry_field='geom'):
	"""
	Queries the first geometry in the given class for the provided geoemtry field
	Returns the CRS and the number of dimensions

	Fails with "None" if no geometry feature is in table
	Approach needs to be modified, if applied on geometrycollections
	"""
	first_geometries = (cls.objects.using('geodatabase').is_not_null(geometry_field))

	# logger.debug(first_geometries)

	if first_geometries:

		first_geometry = first_geometries[0]

		# crs of the feature
		epsg = first_geometry.geom.srid

		# dimension of the geometry: 0=point, 1=line, 2=polygon
		dims = first_geometry.geom.dims

		result = {'epsg': epsg, 'dims': dims}
		logger.debug('Determined {}'.format(result))

		return result

	else:
		logger.warn('Layer {} returned no spatial reference for geometry {}, most likely due to no features in this layer, maybe because of missing EPSG. Will skip it.'.format(cls.__name__, 'geom'))

	return None




def name_the_layer(requested_model, geometry_field):
	"""
	Returns a generic name for the layer (from table's verbose name + geometry field)
	Accepts a table name or the model as "requested_model"
	"""

	the_model = None

	if isinstance(requested_model, str):

		# if the model is submitted as a string, it is the table name
		# so identify the correct model first:
		for table_name, potential_model in inspect.getmembers(sys.modules[__name__]):

			if table_name == requested_model:
				the_model = potential_model
				logger.debug('Resolved (suspected) table name "{}" to model "{}"'.format(requested_model, the_model))
				break
	else:
		# the requeted model was the model itself
		the_model = requested_model

	if not the_model:
		logger.error('Can\'t identify model with table_name {}'.format(requested_model))
		return None

	try:
		verbose_name = the_model._meta.verbose_name_plural
		return ' '.join([verbose_name, geometry_field])
	except:
		logger.error('Can\'t name layer of model {} with geometry field {}'.format(the_model, geometry_field))
		return None



def get_layer_or_none(cls, topic, geometry_field, description='A GIS layer', list_of_data_tables=[], list_of_data_field_tuples=[], label=None, allow_zoom_to_extents=True):
	"""
	Defines a layer
	"""

	label = cls._meta.verbose_name_plural

	name = get_internal_translation_of(label)
	layer_name = '{} {}'.format(name, geometry_field)

	geometry_info = get_crs_and_dims_from_first_geometry_or_none(cls, geometry_field)

	desc = str(description)
	data_source_description = _('Information based on GIS tables: [ {} ]').format(', '.join(list_of_data_tables)) if list_of_data_tables else None

	# data_field_description = ': '.join( [ field_tuple.name for field_tuple in list_of_data_field_tuples ] if list_of_data_field_tuples else None )
	#for field_tuple in list_of_data_field_tuples:
	data_field_description = _('Fields: {}').format('<br>'.join([ '{}: {}'.format(field_tuple[0], field_tuple[1]) for field_tuple in list_of_data_field_tuples ])) if list_of_data_field_tuples else None


	if geometry_info:
		layer = {
			'topic': str(topic),
			'name': layer_name,
			'label': label,
			'slug': slugify_javascript_var(layer_name),
			# 'description': '<br>'.join(list(filter(None, [desc, data_source_description, data_field_description]))),
			'description': desc,
			'description_data_source_tables': data_source_description,
			'description_data_source_fields': data_field_description,
			'model_name': cls.__name__,
			'geometry_field': geometry_field,
			'epsg': geometry_info['epsg'] if geometry_info else None,
			'dims': geometry_info['dims'] if geometry_info else None,
			'zoom_to_extents': allow_zoom_to_extents,
		}

		logger.info('Layer {} set up'.format(layer))

		return layer

	return None
