from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL)

from django.utils.translation import ugettext as _
from geometries.apps import get_internal_translation_of, slugify_javascript_var


def get_category(cls, layer, classifications, description, labelling, label, min_scale=None, max_scale=None, ):
	"""
	Creates a category dictionary
	"""

	name = get_internal_translation_of(label)

	category = {
		'name': name, # name remains untranslated
		'label': label, # label is translated
		'slug': slugify_javascript_var(name),
		'description': description,
		'labelling': labelling,
		'min_scale': min_scale,
		'max_scale': max_scale,
		'classifications': classifications,
	}

	return category
