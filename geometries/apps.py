# -*- coding: utf-8 -*-

"""
This script is called once at start of the Geoportal (= reboot of the server) and defined the database views, the Geoportal works on.

This allows the Geoportal to work on a generic datamodel and changes at the user datamodel have to be reflected here only, instead
of having a side effect to the rest of the Python code.
"""

from django.conf import settings

import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.DEBUG)


from django.template.loader import render_to_string
from django.apps import AppConfig
from django.db import connections, connection

from slugify import Slugify

from datetime import datetime

if settings.DEBUG:
	import time

import psycopg2.extensions
import select

import concurrent.futures
import multiprocessing
import threading
from django.apps import apps

from django.utils import translation

import os
from tempfile import gettempdir



def get_internal_translation_of(string, language=settings.INTERNAL_LANGUAGE_CODE):
	"""
	Supposed to return the given string in the internal translation; so: untranslated
	"""

	with translation.override(language):
		untranslated = translation.gettext(string)
		logger.debug('{}->({})->{}'.format(string, language, untranslated))
		return str(untranslated)



def get_list_of_models_with_layer_definitions():
	"""
	Returns a list of all models whose geospatial features can be mapped
	"""

	models_with_layers = []

	for model in apps.get_app_config('geometries').get_models():

		# determine, if the model is styled and skip it, if not
		if callable(getattr(model, "the_layers", None)):
			models_with_layers.append(model)
			# logger.debug('{} valid layer definitions in for model "{}"'.format(len(model.the_layers()), model))
			continue
		else:
			logger.warn('Model "{}" has no layer definitions OR it\'s queries refer to invalid fields, skipping'.format(model))

	if not len(models_with_layers) > 0:
		logger.warn('{} models have layers defined, NOTHING could potentially be mapped!'.format(len(models_with_layers)))
	else:
		logger.info('{} models have "the_layers(...)" defined and could potentially be mapped.'.format(len(models_with_layers)))

	return models_with_layers



def set_rgba_opacity(color_string, opacity):
	# FIXME ugly: check if color is defined in RGB or RGBA style and modify opacity at string-level ...

	hex_opacity_str = str(hex(round(opacity * 256)))[2:]
	result = color_string

	if len(color_string) == 4:
		# '#FFF' style
		result =  '#FFFFFF' + hex_opacity_str

	elif len(color_string) == 7:
		# '#FFFFFF' (RGB) style
		result = color_string + hex_opacity_str

	else:
		# guessed: RGBA style
		result =  color_string[:-2] + hex_opacity_str

	# logger.debug('Opacity: {} -> {} (mask {})'.format(color_string, result, hex_opacity_str))
	return result




def get_html_parameter_or_none(request, key, default=None, expected_datatype=None, list_of_key_value_tuples=None ):
	"""
	Check the request for value of requested key and returns the value or it's default
	"""

	if not request:
		logger.error('Configuration error: no request submitted. Return {}=None'.format(key))
		return None

	wert = request.GET.get(key, default)

	if wert:
		if expected_datatype:
			try:
				if expected_datatype in ('int', 'integer'):
					wert = int(wert)

				elif expected_datatype in ('string', 'text'):
					wert = str(wert)

				elif expected_datatype in ('bool', 'boolean'):
					# default value can be a bool or string
					if not isinstance(wert, bool):
						wert = False if wert.lower() in ('false', '0', 'no', 'n') else True

				if isinstance(list_of_key_value_tuples, list):
					list_of_key_value_tuples.append((key, wert))

				logger.debug('HTML parameter "{}" mapped to {} of value "{}".'.format(key, expected_datatype, wert))
				return wert

			except:
				# configuration error: default could not be mapped to requested datatype!? Then, better don't set a datatype
				if default: logger.error('HTML parameter "{}": parameter value {} or provided default value "{}" could not be mapped to {}.'.format(key, wert, default, expected_datatype))
				else: logger.error('HTML parameter "{}": parameter value {} or provided default value "{}" could not be mapped to {}.'.format(key, wert, default, expected_datatype))
				return None

		if wert == default: logger.debug('HTML parameter "{}" set to "{}", which equals the provided default value'.format(key, wert))
		else: logger.debug('HTML parameter "{}" found and set to "{}" (default was set to "{}")'.format(key, wert, default))

		if isinstance(list_of_key_value_tuples, list):
			list_of_key_value_tuples.append((key, wert))

		return wert

	logger.debug('HTML parameter "{}" not set or no value submitted. Default is set to "{}", will return "None".'.format(key, default))
	return None



def slugify_javascript_var(text):
	"""
	Prepares a text so it can be used as a Javascript variable name
	Django template tag "|slug" returns dashes, so this takes places
	"""

	to_js_slug = Slugify(separator='_', to_lower=True)
	return to_js_slug(text)



class GenericDataModelConfig(AppConfig):
	"""
	"""

	name = 'geometries'
	verbose_name = ' '.join([settings.ORGANIZATION_NAME_ABBREVIATED, 'Geoportal', 'Version', settings.ORGANIZATION_NAME_ABBREVIATED])

	def ready(self):
		import os
		if os.environ.get('RUN_MAIN'):
			pass
			setup_db_views()
			# on_notification_to_refresh_materialized_view()


def send_refresh_mv_to_db(params_dict=None):
	"""
	When meterialized views are created, refresh them now
	most likely this will be called, when GIS contents have changed and should be reflected in the geoportal views
	"""
	# performance benchmarking
	if settings.DEBUG: start_time = time.time()

	view_name = params_dict['view_name']

	with connections['geodatabase'].cursor() as cursor:

		# refresh requested only
		logger.debug('Refresh materialized view "{}"'.format(view_name))
		cursor.execute('REFRESH MATERIALIZED VIEW CONCURRENTLY "{}";'.format(view_name))

	if settings.DEBUG: logger.info('--- Refreshing materialized views of {} at {} took {} msec ---'.format(view_name, settings.DATABASES['geodatabase']['NAME'], ((time.time() - start_time) * 1000)))



def run_sql(script, cursor=None):
	"""
	"""

	# performance benchmarking
	if settings.DEBUG: start_time = time.time()

	context = {
		#'materialized_views': True, # speed critical setting
		**settings.DB_VIEW_FIELDNAMES,
		'WGS84_TO_UTM_SCALE_FACTOR': settings.WGS84_TO_UTM_SCALE_FACTOR,
		'SOURCE_TABLES_CRS': settings.SOURCE_TABLES_CRS['epsg'],
	}

	# 'utf-8-sig' removes BOM if present
	substituted_sql = render_to_string(script, context).encode().decode("utf-8-sig")

	if not cursor:
		logger.info('New DB connection opened')
		with connections['geodatabase'].cursor() as cursor:
			cursor.execute(substituted_sql)
	else:
		cursor.execute(substituted_sql)

	if settings.DEBUG: logger.info('--- SQL {} took {} msec ---'.format(script, ((time.time() - start_time) * 1000)))

	return script



def run_sql_dict(params_dict=None):
	"""
	"""

	# performance benchmarking
	if settings.DEBUG: start_time = time.time()

	context = {
		# 'materialized_views': True, # speed critical setting
		**settings.DB_VIEW_FIELDNAMES,
		'WGS84_TO_UTM_SCALE_FACTOR': settings.WGS84_TO_UTM_SCALE_FACTOR,
		'SOURCE_TABLES_CRS': settings.SOURCE_TABLES_CRS['epsg'],
	}

	# 'utf-8-sig' removes BOM if present
	substituted_sql = render_to_string(params_dict['script'], context).encode().decode("utf-8-sig")


	# save SQL as a file in debug mode
	if settings.DEBUG:
		sqlfile = os.path.join(gettempdir(), os.path.basename(params_dict['script']))

		with open(sqlfile, 'w') as sql:
			sql.write(substituted_sql)

		logger.debug('Wrote SQL file to "{}"'.format(sqlfile))


	if not params_dict['cursor']:
		# logger.info('New DB connection opened')
		with connections['geodatabase'].cursor() as cursor:
			cursor.execute(substituted_sql)
			# logger.debug(substituted_sql)
	else:
		params_dict['cursor'].execute(substituted_sql)

	if settings.DEBUG: logger.info('--- SQL dict run {} took {} msec ---'.format(params_dict['script'], ((time.time() - start_time) * 1000)))

	return params_dict['script']



# def setup_db_notification_to_refresh_materialized_view():
	# """
	# """
	# """
	# Listens to Notifications events from the db to initiate some action

	# Currently: update the materialized view, which is a costly operation

	# REFRESH MATERIALIZED VIEW CONCURRENTLY my_mv;
	# """

	# with connections['geodatabase'].cursor() as cursor:

		# pg_con = connection.connection
		# pg_con.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

		# # cursor.execute('LISTEN test;');
		# cursor.execute('NOTIFY test;');
		# cursor.execute('SELECT pg_sleep(6); NOTIFY test;');


# def on_notification_to_refresh_materialized_view():
	# """
	# """

	# connection = connections['geodatabase'].connection
	# connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

	# with connection.cursor() as cursor:

		# #pg_con = connection.connection
		# #pg_con.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

		# cursor.execute('LISTEN test;');

		# # print "Waiting for notifications on channel 'test'"
		# logger.error('Waiting for notifications on channel <test>')
		# while 1:
			# if select.select([connection],[],[],5) == ([],[],[]):
				# # print "Timeout"
				# logger.error('Timeout')
			# else:
				# connection.poll()
				# while connection.notifies:
					# notify = connection.notifies.pop()
					# logger.error( "Got NOTIFY: {}".format(notify))



# def on_just_refreshed_materialized_view():
	# """
	# Set a variable with the date & time of last view materialization
	# This allows to check, wheather a new refresh is required or not, as the SQL sends this notification:

		# CREATE OR REPLACE FUNCTION notify_refreshed() RETURNS void LANGUAGE 'plpgsql' AS
		# $BODY$ BEGIN
			# execute format('notify test, ''%s''', cast(to_char((current_timestamp)::TIMESTAMP,'yyyymmddhhmiss') as BigInt) );
		# END $BODY$;

		# select notify_refreshed();
	# """

	# datetime_integer = int(datetime.now().strftime("%Y%m%d%H%M%S"))
	# settings.MATERIALIZED_VIEW_UPDATE_DATETIME = datetime_integer
	# logger.info('View materialized at {}'.format(datetime_integer))

	# return



def setup_db_views(db=settings.DATABASES['geodatabase']['NAME'], refresh_existing=False, refresh_single_mv=None):
	"""
	This piece of code renders some template as SQL and executes it on the server
	Aim is to setup a view with a the generic data model the database works on, instead of using the user data model directly
	This is supposed to keep the code basis robust when the user data model changes, as adjustments will be done at the sql script, wich will
	create the same generic data model from the changed input
	"""

	settings.DATABASES['geodatabase']['NAME'] = db

	# performance benchmarking
	if settings.DEBUG: start_time = time.time()


	scripts = {
			# execution in order
			'pre-one-by-one': [
				'sql/0.1_preparations.sql',
			],

			# as threads (network operations)
			'threading': [
				'sql/0.1_update_sewer_conduits_view.sql',
				'sql/0.1_update_water_pipes_view.sql',
				'sql/0.1_update_water_tanks_view.sql',
				'sql/0.1_update_water_pumps_view.sql',
				'sql/0.1_update_water_sources_view.sql',
				'sql/0.1_update_admin_bounds_view.sql',
				'sql/0.1_update_water_fittings_view.sql',
				'sql/0.1_update_water_consumers_view.sql',
				#'sql/0.1_update__view.sql',
			],

			# as processes (cpu intense operations!)
			'processes': [
			]
		}

	# testing purposes
	if settings.DEBUG:
		scripts['threading'].append('sql/0.1_update_hm_water_view.sql',)


	with connections['geodatabase'].cursor() as cursor:

		cursor.execute("""SET CLIENT_ENCODING TO 'utf8';""")


		# setup new views
		if not refresh_existing and not refresh_single_mv:

			# preparations scripts, to be run in order of appearence
			for script in scripts['pre-one-by-one']:

				run_sql(script, cursor)

				# sql as parallel thread (threads are fine here, no separate processes required)
				no_of_parallel_tasks = min(multiprocessing.cpu_count(), max(len(scripts['threading']), 1))
				logger.debug('Use {} threads for {}'.format(no_of_parallel_tasks, ', '.join([ sql for sql in scripts['threading'] ])))

				with concurrent.futures.ThreadPoolExecutor(max_workers=no_of_parallel_tasks) as executor:

					# result_futures = list(map(scripts['threading'], lambda x: executor.submit(run_sql, (x, cursor, ), iterable )))
					# it seems faster, if a new DB cursor is opened for each script:
					result_futures = {
							executor.submit(run_sql_dict, { 'script': script, 'cursor': None, } ) : script for script in scripts['threading']
						}

					for future in concurrent.futures.as_completed(result_futures):
						script = result_futures[future]
						# logger.debug(script)

						try:
							result = future.result()
						except Exception as exc:
							logger.error('SQL script "{}" dropped an exception: "{}"'.format(script, exc))

					logger.info('Views based on database "{}" have been set up'.format(settings.DATABASES['geodatabase']['NAME']))

		# refresh only:
		else:

			materialized_views = []

			if refresh_single_mv:
				# refresh only requested only
				materialized_views.append(refresh_single_mv)
			else:
				# refresh all but administrative boundaries (speed)
				materialized_views = [ value for key,value in settings.MATERIALIZED_VIEWS.items()
					if not key == 'VIEW_ADMIN_BOUNDS'
				]

			# sql as parallel thread (threads are fine here, no separate processes required)
			no_of_parallel_tasks = min(multiprocessing.cpu_count(), max(len(materialized_views), 1))
			logger.debug('Use {} threads for refreshing materialized views {}'.format(no_of_parallel_tasks, materialized_views))

			with concurrent.futures.ThreadPoolExecutor(max_workers=no_of_parallel_tasks) as executor:

				result_futures = {
					executor.submit(send_refresh_mv_to_db, { 'view_name': view_name, } ) : view_name for view_name in materialized_views
				}

				for future in concurrent.futures.as_completed(result_futures):
					view_name = result_futures[future]

					try:
						result = future.result()
					except Exception as exc:
						logger.error('Exception during refresh of {}: "{}"'.format(view_name, exc))

			logger.info('Views based on database "{}" have been set up'.format(settings.DATABASES['geodatabase']['NAME']))

	if settings.DEBUG: logger.info('--- Preparing/refreshing db views took {} msec in total ---'.format(((time.time() - start_time) * 1000)))
