from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL)

from django.utils.translation import ugettext as _
from os.path import join

from datetime import datetime
from django.urls import reverse

from geometries.apps import get_internal_translation_of, slugify_javascript_var, set_rgba_opacity



def icon_specs(specs):
	return { 'icon': specs }



def line_specs(specs):
	return { 'line': specs }



# def polygon_specs(fill=None, color=None, width=None, dash=None, dash_offset=None):
def polygon_specs(specs):
	return { 'polygon': specs }



def circle_specs(specs):
	return { 'circle': specs}



def label_specs(specs):
	return { 'text': specs}




def create_classification(label, queryset, base_color, style_defs, description=''):
	"""
	Create a classification dict
	"""

	name = get_internal_translation_of(label)

	classification_dict = {
			'name': name,
			'slug': slugify_javascript_var(name),
			'label': label,
			'queryset': queryset,
			'base_color': base_color,
			'style_defs': style_defs,
			'description': description, # a short explaination, what this classification reflects
		}

	return classification_dict



def default_label_style(color=settings.WATER_DISTRIBUTION_LINE_STYLE_COLOR): return {} # { 'color': color }



def by_age(cls, geometry_field, design_horizon):
	"""
	Age of the features
	"""

	all_radius = 4
	all_width = 3

	older_then_thirty = create_classification(
				label=_('Older then {} years features').format(30),
				queryset = cls.objects.using('geodatabase').older_then(30, geometry_field, design_horizon),
				base_color = settings.OLDEST_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': all_radius, }),
					**line_specs({'width': all_width, }),
					**polygon_specs({ 'fill': None, 'width': 1, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(settings.OLDEST_STYLE_COLOR), })
				}
			)

	max_thirty = create_classification(
				label=_('{} to {} years old features').format(20, 29),
				queryset = cls.objects.using('geodatabase').older_then(20, geometry_field, design_horizon).younger_then(30, geometry_field, design_horizon),
				base_color = settings.OLD_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': all_radius, }),
					**line_specs({'width': all_width, }),
					**polygon_specs({ 'fill': None, 'width': 1, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(settings.OLD_STYLE_COLOR), })
				}
			)


	max_twenty = create_classification(
				label=_('{} to {} years old features').format(10, 19),
				queryset = cls.objects.using('geodatabase').older_then(10, geometry_field, design_horizon).younger_then(20, geometry_field, design_horizon),
				base_color = settings.AGED_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': all_radius, }),
					**line_specs({'width': all_width, }),
					**polygon_specs({ 'fill': None, 'width': 1, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(settings.AGED_STYLE_COLOR), })
				}
			)


	max_ten = create_classification(
				label=_('Features younger then {} years').format(10),
				queryset = cls.objects.using('geodatabase').older_then(0, geometry_field, design_horizon).younger_then(10, geometry_field, design_horizon),
				base_color = settings.YOUNG_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': all_radius, }),
					**line_specs({'width': all_width, }),
					**polygon_specs({ 'fill': None, 'width': 1, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(settings.YOUNG_STYLE_COLOR), })
				}
			)


	unknown = create_classification(
				label=_('Features of undefined construction year'),
				queryset = cls.objects.using('geodatabase').is_null('year_start', geometry_field),
				base_color = settings.UNKNOWN_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': all_radius, }),
					**line_specs({'width': all_width, }),
					**polygon_specs({ 'fill': None, 'width': 1, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(settings.UNKNOWN_STYLE_COLOR), })
				}
			)


	other = create_classification(
				label=_('Future features'),
				queryset = cls.objects.using('geodatabase').other_then([
					unknown['queryset'],
					max_ten['queryset'],
					max_twenty['queryset'],
					max_thirty['queryset'],
					older_then_thirty['queryset']
				], geometry_field),
				base_color = settings.OTHER_STYLE_COLOR,
				style_defs = {
					**icon_specs({ 'img': None, 'radius': all_radius, }),
					**line_specs({'width': all_width, }),
					**polygon_specs({ 'fill': None, 'width': 1, 'dash_line': None, 'dash_space': None, }),
					**label_specs( { **default_label_style(settings.OTHER_STYLE_COLOR), })
				}
			)


	return [max_ten, max_twenty, max_thirty, older_then_thirty, other, unknown]
