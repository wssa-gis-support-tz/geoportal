from django.conf import settings
import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.DEBUG)

from django.contrib.gis.db import models
from django.db.models import Q
from django.utils.translation import ugettext as _


import datetime


def query_for_icontains_in_field(initial_query, field_name, icontains_list=[]):
	"""
	"""
	query = initial_query
	q = Q()

	for value in icontains_list:
		lookup = ('{}__icontains'.format(field_name))
		q.add(Q(**{ lookup : value } ), Q.OR)
		# logger.debug('############# {}'.format(q))

	# logger.debug('************ final Q {}'.format(q))
	response= {
			'queryset': query.filter(q),
			'description': _('Features belong to this class if their "{}" field contains: [{}]').format(field_name, _('or').join([ ' "' + a + '" ' for a in icontains_list])),
		}

	return response


def query_for_field_value_between(initial_query, field_name, minimum=None, maximum=None):
	"""
	Inclusive the given minimum value
	Exclusive the given maximum value
	"""

	query = initial_query

	#included_minimum = max(list(filter(None, value_one, value_two)))
	#excluded_maximum = max(list(filter(None, value_one, value_two)))

	#lookup_lower = ('{}__gte'.format(field_name))
	#lookup_upper = ('{}__lt'.format(field_name))

	q_one = Q(**{ '{}__gte'.format(field_name) : minimum } )
	q_two = Q(**{ '{}__lt'.format(field_name) : maximum } )

	if minimum and maximum:
		q = q_one.add(q_two, Q.AND)
	elif minimum:
		q = q_one
	elif maximum:
		q = q_two
	else:
		logger.error('The query for min/max values on field "{}" requires at least one value'.format(field_name))
		q = Q()

	response= {
			'queryset': query.filter(q),
			'description': _('Features belong to this class if their "{}" field is larger or equal to "{}" and less then "{}"').format(field_name, minimum, maximum),
		}

	return response



class DefaultQuerySets(models.QuerySet):
	"""
	Pre-defined Queries
	"""


	def is_not_null(self, field_name, geometry_field=None):
		"""
		Features with field_name != NULL
		Avoid this is field name is fix to avoid it's string operation with respective overhead
		"""
		# logger.debug('is_not_null on {}'.format(self))

		lookup = ('{}__isnull'.format(field_name))
		response = self.filter(**{ lookup : False })

		if geometry_field:
			response = response.is_not_null(geometry_field)

		return response


	def is_null(self, field_name, geometry_field=None):
		"""
		Features with field_name != NULL
		Avoid this is field name is fix to avoid it's string operation with respective overhead
		"""
		lookup = ('{}__isnull'.format(field_name))
		response = self.filter(**{ lookup : True })

		if geometry_field:
			response = response.is_not_null(geometry_field)

		return response



	# problems

	def no_year_start(self, geometry_field):
		return self.is_not_null(settings.DB_VIEW_FIELDNAMES['YEAR_START'], geometry_field)


	def invalid_year_start(self, geometry_field):
		return self.is_null(settings.YEAR_START, geometry_field)


	def no_diameter(self, geometry_field):
		return is_null(settings.DIAMETER_FIELDNAME, geometry_field)


	def no_material(self, geometry_field):
		return is_null(settings.MATERIAL_FIELDNAME, geometry_field)


	def no_purpose(self, geometry_field):
		return is_null(settings.PURPOSE_FIELDNAME, geometry_field)



	# db relations with spatial context

	def other_then(self, exclude_query_list, geometry_field):
		"""
		excludes all listed queries and limits to geometry_fields
		"""
		the_query = self.is_not_null(geometry_field)

		for query in exclude_query_list:
			the_query = the_query.exclude(id__in=query)

		return the_query


	# spatial relations

	def intersects_with(self, geometry):
		"""
		Returns all entries who's geom intersects with requested geometry
		"""
		return self.filter(geom__intersects=geometry)


	def disjoint_with(self, geometry):
		"""
		Returns all entries who's geom does NOT intersect with requested geometry
		"""
		return self.filter(geom__disjoint=geometry)



	# infrastructure age queries

	def older_then(self, age, geometry_field, design_horizon):
		"""
		returns features older or equal then "age"
		"""

		if isinstance(design_horizon, datetime.date):
			design_horizon = design_horizon.year

		if design_horizon:
			deadline = design_horizon - age
			#lookup = ('{}__lte'.format(settings.DB_VIEW_FIELDNAMES['YEAR_START']))
			# response = self.filter(**{ lookup : deadline })
			response = self.filter(id__in=self.filter(Q(year_start__lte=deadline) ) )
			return response
		else:
			logger.warn('older_then called with no design_horizon for {}'.format(self))
			return self.none()


	def younger_then(self, age, geometry_field, design_horizon):
		"""
		returns features younger then "age"
		"""

		if isinstance(design_horizon, datetime.date):
			design_horizon = design_horizon.year

		if design_horizon:
			deadline = design_horizon - age
			return self.filter(Q(year_start__gt=deadline) )
		else:
			#logger.warn('younger_then called with no design_horizon for {}'.format(self))
			return self.none()


	# Infrastructure implementation status used by implementation status
	# the geometry_field does not matter here

#	def removed_by(self, design_horizon=datetime.datetime.now()):
	def removed_by(self, design_horizon):
		"""
		Returns infrastructure that is removed by the END OF THE REQUESTED YEAR
		A feature planned_by year X and decomissioned by same year X, existed for one year, therefore
		"""

		#if not design_horizon:
		#	return self

		#else:
		if isinstance(design_horizon, datetime.date):
			design_horizon = design_horizon.year

		return self.filter(year_end__lt=design_horizon)



#	def planned_by(self, design_horizon=datetime.datetime.now()):
	def planned_by(self, design_horizon):

		"""
		Return features that are known to exist BY THE BEGINNING OF THE REQUESTED YEAR
		A feature planned_by year X and decomissioned by same year X, existed for one year, therefore
		"""

		#if not design_horizon:
		#	return self

		#else:
		if isinstance(design_horizon, datetime.date):
			design_horizon = design_horizon.year

		return self.filter(year_start__gt=design_horizon)



	#def existing_by(self, design_horizon=datetime.datetime.now()):
	def existing_by(self, design_horizon):

		"""
		Features that are not decomissioned and not planned
		"""

		#if not design_horizon:
		#	return self

		#else:
		if isinstance(design_horizon, datetime.date):
			# translate integer year to date
			design_horizon = design_horizon.year

		# negate planned_by and removed_by
		return self.exclude( Q(id__in=self.removed_by(design_horizon)) | Q(id__in=self.planned_by(design_horizon)) )



	# Water and sewer pipe purposes:

	# def consumer_connection(self, geometry_field):
		# """
		# Consumer line/house connection for water supply and/or sewer line
		# """
		# return self.is_not_null(geometry_field).filter(purpose__isnull=False).filter( Q(purpose__icontains='consumer') | Q(purpose__icontains='house') | Q(purpose__icontains='service'))


	# def transmission(self, geometry_field):
		# """
		# Transmission line, pumping main
		# """
		# return self.is_not_null(geometry_field).filter(purpose__isnull=False).filter( Q(purpose__icontains='transmission')  | Q(purpose__icontains='rising') )


	# def distribution(self, geometry_field):
		# """
		# water distribution line
		# """
		# return self.is_not_null(geometry_field).filter(purpose__isnull=False).filter( Q(purpose__icontains='distribution') )


	# def unknown_purpose(self, geometry_field):
		# """
		# Undefined purposes
		# """
		# return self.is_not_null(geometry_field).filter(purpose__isnull=True)


	def trunk(self, geometry_field):
		"""
		Sewer conduit/main
		"""
		return self.is_not_null(geometry_field).filter(purpose__isnull=False).filter( Q(purpose__icontains='trunk') )


	def lateral(self, geometry_field):
		"""
		Sewer conduit/lateral
		"""
		return self.is_not_null(geometry_field).filter(purpose__isnull=False).filter( Q(purpose__icontains='lateral') )


	# Administrative boundaries

	# def wards(self, geometry_field):
		# """
		# """
		# return self.is_not_null(geometry_field).filter(Q(adm_label__icontains='ward') | Q(adm_label__icontains='shehia') )


	# def districts(self, geometry_field):
		# """
		# """
		# return self.is_not_null(geometry_field).filter(adm_label__icontains='district')


	# def regions(self, geometry_field):
		# """
		# """
		# return self.is_not_null(geometry_field).filter(adm_label__icontains='region')


	# tank structure

	def elevated(self, geometry_field):
		"""
		Tanks: elevated
		"""
		return self.is_not_null(geometry_field).filter(grnd2lwl_m__gte = 3)


	def ground(self, geometry_field):
		"""
		Tanks: elevated
		"""
		# would threat unknown structure height as ground tank:
		return self.is_not_null(geometry_field).filter(grnd2lwl_m__lt = 3, grnd2lwl_m__gt = -2)


	def underground(self, geometry_field):
		"""
		Tanks: elevated
		"""
		return self.is_not_null(geometry_field).filter(grnd2lwl_m__lte = -2)


	def other_and_unknown_structure(self, geometry_field):
		return self.is_not_null(geometry_field).exclude(Q(id__in=self.elevated(geometry_field)) | Q(id__in=self.ground(geometry_field)) | Q(id__in=self.underground(geometry_field)))


	# customer connection status


	# # water source type

	# def borehole(self, geometry_field):
		# """
		# Identify boreholes
		# """
		# return self.is_not_null(geometry_field).filter(Q(type__icontains='bh') | Q(type__icontains='borehole') )


	# def wtp(self, geometry_field):
		# """
		# Identify water treatment plants
		# """
		# return self.is_not_null(geometry_field).filter(Q(type__icontains='wtp') | Q(type__icontains='water treatment') )


	# def intake(self, geometry_field):
		# """
		# Identify dam or river intakes
		# """
		# return self.is_not_null(geometry_field).filter(type__icontains='intake')


	# sanitation system


	def sanitation_area_is_pond(self, geometry_field):
		"""
		"""
		return self.is_not_null(geometry_field).filter(topic__icontains='pond')


	# # pipe diameters

	# def tiny_pipe(self, geometry_field, diameter_mm_field_name=settings.DB_VIEW_FIELDNAMES['INNER_DIAMETER_FIELDNAME']):
		# """
		# """
		# lookup_high = ('{}__lt'.format(diameter_mm_field_name))
		# response = self.filter(**{ lookup_high : settings.PIPE_DIAMETER_SMALL_MM })
		# return response.is_not_null(geometry_field)



	# def small_pipe(self, geometry_field, diameter_mm_field_name=settings.DB_VIEW_FIELDNAMES['INNER_DIAMETER_FIELDNAME']):
		# """
		# """
		# lookup_low = ('{}__gte'.format(diameter_mm_field_name))
		# lookup_high = ('{}__lt'.format(diameter_mm_field_name))
		# response = self.filter(**{ lookup_low : settings.PIPE_DIAMETER_SMALL_MM }).filter(**{ lookup_high : settings.PIPE_DIAMETER_MEDIUM_MM })
		# return response.is_not_null(geometry_field)


	# def medium_pipe(self, geometry_field, diameter_mm_field_name=settings.DB_VIEW_FIELDNAMES['INNER_DIAMETER_FIELDNAME']):
		# """
		# """
		# lookup_low = ('{}__gte'.format(diameter_mm_field_name))
		# lookup_high = ('{}__lt'.format(diameter_mm_field_name))
		# response = self.filter(**{ lookup_low : settings.PIPE_DIAMETER_MEDIUM_MM }).filter(**{ lookup_high : settings.PIPE_DIAMETER_LARGE_MM })
		# return response.is_not_null(geometry_field)


	# def large_pipe(self, geometry_field, diameter_mm_field_name=settings.DB_VIEW_FIELDNAMES['INNER_DIAMETER_FIELDNAME']):
		# """
		# """
		# lookup_low = ('{}__gte'.format(diameter_mm_field_name))
		# lookup_high = ('{}__lt'.format(diameter_mm_field_name))
		# response = self.filter(**{ lookup_low : settings.PIPE_DIAMETER_LARGE_MM }).filter(**{ lookup_high : settings.PIPE_DIAMETER_HUGE_MM })
		# return response.is_not_null(geometry_field)


	# def huge_pipe(self, geometry_field, diameter_mm_field_name=settings.DB_VIEW_FIELDNAMES['INNER_DIAMETER_FIELDNAME']):
		# """
		# """
		# lookup_low = ('{}__gte'.format(diameter_mm_field_name))
		# response = self.filter(**{ lookup_low : settings.PIPE_DIAMETER_HUGE_MM })
		# return response.is_not_null(geometry_field)

	# FITTINGS

	# def endcaps(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# values = ['end']

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)



	# def airvalves(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# values = ['air', 'av',]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)



	# def washouts(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# values = ['wash', 'wo', 'flush']

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)


	# def saddles(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# values = ['saddle', ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)


	# def bulkmeters(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# values = ['bm', 'bulk']

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)


	# def isolation_valves(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """

		# values = ['sluice', 'gate', 'sv', 'gv', 'control']

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)



	# def bends(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# values = ['bend', 'corner', 'elbow']

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)


	# def junctions(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# #return self.is_not_null(geometry_field).filter(type__icontains='junction')

		# values = ['junction', 'tee' ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )

		# return response.filter(q)



	# def reducer(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# values = ['reducer', ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)




	# def hydrants(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['FITTING_TYPE_FIELDNAME']):
		# """
		# """
		# values = ['fh', 'hydrant', ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)


	# TARIFF


	# def water_kiosk(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['TARIFF_FIELDNAME']):
		# """
		# """
		# values = ['kiosk', 'kio', ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)



	# def domestic(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['TARIFF_FIELDNAME']):
		# """
		# """
		# values = ['dom', ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)


	# def institutional(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['TARIFF_FIELDNAME']):
		# """
		# """
		# values = ['ins', ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)


	# def industrial(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['TARIFF_FIELDNAME']):
		# """
		# """
		# values = ['ind', ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)


	# def commercial(self, geometry_field, field_name=settings.DB_VIEW_FIELDNAMES['TARIFF_FIELDNAME']):
		# """
		# """
		# values = ['com', ]

		# response = self.is_not_null(geometry_field)
		# q = None

		# for value in values:
			# lookup = ('{}__icontains'.format(field_name))
			# if q: q.add(Q(**{ lookup : value } ), Q.OR)
			# else: q = Q(**{ lookup : value } )
		# return response.filter(q)

