from django.conf import settings
import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.DEBUG)

from django.contrib.gis.db import models
from django.utils.translation import ugettext as _
# from django.apps import apps
from datetime import datetime

import sys, inspect

from .queries import DefaultQuerySets
from . import categories
from .layers import get_layer_or_none

from geometries.apps import get_internal_translation_of, slugify_javascript_var, get_list_of_models_with_layer_definitions


def get_impl_period_str(year_start, year_end, compare_to_design_horizon=datetime.today().year):
	"""
	Returns:
	(2018-2025) or (?-2025) or (since 2018)
	                           (planned for 2018) # if in future
	"""

	if not year_start and not year_end:
		return _('No information about the year of construction')
	else:
		from_string = ''
		until_string = ''

		if year_start:
			from_string = 'Planned construction in year' if year_start > compare_to_design_horizon else 'Exists since'
			from_string = from_string + ' ' + str(year_start)
		else:
			from_string = 'Unknown year of construction'

		if year_end:
			until_string = ', planned decommissioning' if year_end > compare_to_design_horizon else ', decomissioned in'
			until_string = until_string + ' ' + str(year_end)
		else:
			until_string = ' to date (' + str(compare_to_design_horizon) + ')'

		return '{}{}'.format(from_string, until_string)







def get_requested_model_categories_or_none(requested_layer_slug, requested_category_slug = None, design_horizon=None):
	"""
	Returns a dict with instances of the requested model, layer, category and a list of all categories
	If no category is requested, the first in list will be returned.
	If no design horizon is requested,
	"""

	model = None # required for KML request only
	layer = None # keeps instance of requested layer
	category = None # keeps the requested category instance
	category_options = [] # keeps the list of all categories of this layer

	for potential_model in list(filter(None, get_list_of_models_with_layer_definitions())):

		# Barrier: skip requested layer, if the_layers failed
		if not potential_model.the_layers(design_horizon=design_horizon): continue

		for potential_layer in list(filter(None, potential_model.the_layers(design_horizon=design_horizon))):

			if not 'slug' in potential_layer:
				logger.error('Layer "{}" has no "slug" set! Can\'t identify it.'.format(potential_layer['name']))
				continue

			if potential_layer['slug'] == requested_layer_slug:
				layer = potential_layer
				model = potential_model

				# stop searching for other layers, we found what we want
				break

		if layer:
			logger.info('Found layer "{}" matches requested layer slug "{}"'.format(layer['name'], requested_layer_slug))
			break


	if not layer:
		logger.warn('No model has a layer name matching "{}", skipping'.format(requested_layer_slug))
		return None

	# iterate over all layer's categories, pick the one that is requested and list the others as options
	for potential_category in layer['categories']:

		# add found categories to the picklist of options
		category_options.append({
			'slug': potential_category['slug'],
			'name': potential_category['name'],
			'label': potential_category['label'],
		})

		if not 'slug' in potential_category:
			logger.error('Category "{}" has no "slug" set! Can\'t identify it.'.format(potential_category['name']))
			continue

		# take the first category matching the requested slug, or the first in list, if none was requested
		if not category and potential_category['slug'] == requested_category_slug or (not requested_category_slug and not category):
			category = potential_category
			logger.info('Select category "{}" for layer "{}". Requested was "{}"'.format(category['name'], layer['name'], requested_category_slug))
			# do not 'break' the for loop here, as subsequent categories still need to be added to the list of options


	# if no combination of model/layer/style was found, raise an error
	if not model or not layer or not category:
		logger.error('Configuration error: requested category "{}" could not be found (or no categories defined at all) for layer "{}"'.format(requested_category_slug, requested_layer_slug))
		return None


	result = {
		'model': model,
		'layer': layer,
		'category': category,
		'category_options': category_options,
	}

	logger.debug('Identified layer: "{}" and category: "{}" that has {} classifications'.format(result['model'], result['category']['name'], len(result['category']['classifications'])))

	return result






from .db_views import admin_bounds
from .db_views import water_tanks
from .db_views import water_sources
from .db_views import water_pipes
from .db_views import water_pumps
from .db_views import water_fittings
from .db_views import water_consumers
from .db_views import sewer_conduits

# hydraulic modelling
from .db_views import hm_water_nodes




