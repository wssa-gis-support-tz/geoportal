from django.conf import settings


from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.views.generic.base import RedirectView
from django.contrib.auth.decorators import login_required, permission_required


# from django.conf.urls.i18n import i18n_patterns

from . import views

app_name = 'geometries'

urlpatterns = [
	# path("__admin/", admin.site.urls),

	# redirect to app's index page, if it's a sub-page
	# path('', RedirectView.as_view(url='/', permanent=True), name='kml'),

	#path('<slug:table_name>/<slug:geometry_field>/kml/', login_required(views.get_kml), name='kml'), # style as html parameter; icons in kml are references only
	#path('<slug:table_name>/<slug:geometry_field>/kmz/', login_required(views.get_kmz), name='kmz'), # kmz has icons embedded

]

if settings.REQUIRE_AUTHENTICATION:
	urlpatterns.append(path('<slug:layer_slug>/kml/', login_required(views.get_kml), name='kml')) # style as html parameter; icons in kml are references only
	urlpatterns.append(path('<slug:layer_slug>/kmz/', login_required(views.get_kmz), name='kmz')) # kmz has icons embedded
	urlpatterns.append(path('icon/<slug:layer_slug>/<slug:category_slug>/<slug:style_slug>/', login_required(views.serve_icon), name='icon')) # submid status as a slug to allow negative values, which are currently not possible using "int
else:
	urlpatterns.append(path('<slug:layer_slug>/kml/', (views.get_kml), name='kml')) # style as html parameter; icons in kml are references only
	urlpatterns.append(path('<slug:layer_slug>/kmz/', (views.get_kmz), name='kmz')) # kmz has icons embedded
	urlpatterns.append(path('icon/<slug:layer_slug>/<slug:category_slug>/<slug:style_slug>/', (views.serve_icon), name='icon')) # submid status as a slug to allow negative values, which are currently not possible using "int"
