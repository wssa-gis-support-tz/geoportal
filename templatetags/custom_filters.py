# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL)

from django import template
register = template.Library()
import urllib.parse # für filter_string

from slugify import Slugify, GERMAN # js var name
from itertools import groupby # dynamic regroup


from psycopg2 import connect, OperationalError


# @register.simple_tag(name='geoserver_is_ok')
# def geoserver_is_ok():
	# # logger.info('Test connection to Geoserver')
	# try:
	# # start = datetime.now()
		# # connection = connect(database=database, user=user, host=host, port=port, password=password, connect_timeout=TIMEOUT_SECONDS,)
		# connection = connect(database=settings.DATABASES['geodatabase']['NAME'], host=settings.DATABASES['geodatabase']['HOST'], user=settings.DATABASES['geodatabase']['USER'], password=settings.DATABASES['geodatabase']['PASSWORD'])
		# # log.debug("at end of context manager")
		# # micro = (datetime.now() - start).microseconds
		# logger.info("Connection to Target database is ok!")
		# connection.close()
		# return True
	# except (OperationalError, KeyError) as ex:
		# logger.error("CONFIGURATION OR NETWORK PROBLEM: COULD NOT CONNECT TO GEOSERVER")
	# return False



@register.simple_tag(name='add_to_url_get_param')
def add_to_url_get_param(field_name, value):

	# dict_ = context['request'].GET.copy()
	# logger.info(str(dict_))
	dict_ = {}
	dict_[field_name] = value # urllib.parse.quote_plus(str(value))
	return urllib.parse.urlencode(dict_)



@register.simple_tag(takes_context = True)
def pass_next_and_listed_parameters_only(context, next_url, field_list):
	dict_ = context['request'].GET.copy()
	newdict = {}
	newdict['next_url'] = next_url
	if field_list:
		for field in field_list:
			# wert = self.request.GET.get(field, None)
			try:
				wert = dict_[field]
				newdict[field] = wert
				# logger.info('URLRepair - Relevanter Parameter: {} -> {}'.format(field, wert))
			except KeyError:
				logger.debug('{} nicht in {}'.format(field, ct))
				pass
	# logger.debug('Ergebnis: ' + str(newdict))
	return urllib.parse.urlencode(newdict)



class DynamicRegroupNode(template.Node):
	def __init__(self, target, parser, expression, var_name):
		self.target = target
		self.expression = template.Variable(expression)
		self.var_name = var_name
		self.parser = parser

	def render(self, context):
		obj_list = self.target.resolve(context, True)
		if obj_list == None:
			# target variable wasn't found in context; fail silently.
			context[self.var_name] = []
			return ''
		# List of dictionaries in the format:
		# {'grouper': 'key', 'list': [list of contents]}.

		"""
		Try to resolve the filter expression from the template context.
		If the variable doesn't exist, accept the value that passed to the
		template tag and convert it to a string
		"""

		try:
			exp = self.expression.resolve(context)
		except template.VariableDoesNotExist:
			exp = str(self.expression)

		# if regrouping field is None, return the unchanged list:
		if not exp:
			context[self.var_name] = [ {'grouper': None, 'list': list(obj_list)} ] # for val in obj_list ]
			return ''


		filter_exp = self.parser.compile_filter(exp)

		context[self.var_name] = [
			{'grouper': key, 'list': list(val)}
			for key, val in
			groupby(obj_list, lambda v, f=filter_exp.resolve: f(v, True))
		]

		return ''




#		try:
#			exp = self.expression.resolve(context)
#		except template.VariableDoesNotExist:
#			exp = str(self.expression)
#
#		filter_exp = self.parser.compile_filter(exp)
#
#		context[self.var_name] = [
#			{'grouper': key, 'list': list(val)}
#			for key, val in
#			groupby(obj_list, lambda v, f=filter_exp.resolve: f(v, True))
#		]
#
#		return ''



@register.tag
def dynamic_regroup(parser, token):
	"""
	https://djangosnippets.org/snippets/2511/
	"""
	firstbits = token.contents.split(None, 3)
	if len(firstbits) != 4:
		raise TemplateSyntaxError("'regroup' tag takes five arguments")
	target = parser.compile_filter(firstbits[1])
	if firstbits[2] != 'by':
		raise TemplateSyntaxError("second argument to 'regroup' tag must be 'by'")
	lastbits_reversed = firstbits[3][::-1].split(None, 2)
	if lastbits_reversed[1][::-1] != 'as':
		raise TemplateSyntaxError("next-to-last argument to 'regroup' tag must be 'as'")

	"""
	Django expects the value of `expression` to be an attribute available on
	your objects. The value you pass to the template tag gets converted into a
	FilterExpression object from the literal.

	Sometimes we need the attribute to group on to be dynamic. So, instead
	of converting the value to a FilterExpression here, we're going to pass the
	value as-is and convert it in the Node.
	"""
	expression = lastbits_reversed[2][::-1]
	var_name = lastbits_reversed[0][::-1]

	"""
	We also need to hand the parser to the node in order to convert the value
	for `expression` to a FilterExpression.
	"""
	return DynamicRegroupNode(target, parser, expression, var_name)



@register.filter(name='slugify_to_valid_js_var')
def slugify_to_valid_js_var(text):
	"""
	{{ text|slugify_to_valid_js_var }}
	"""

	jsvar = str(text)

	try:
		slugify_de = Slugify(pretranslate=GERMAN,separator='_')
		jsvar = slugify_de(text or 'irgendwas')
	except:
		pass

	jsvar = '_'.join(['var', jsvar])
	# logger.debug('Ermittelte JS-Variable: {}'.format(jsvar))
	return jsvar
