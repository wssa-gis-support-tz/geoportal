from django.conf import settings
import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.DEBUG)

from geometries.styles import get_style_def_or_default, expand_icon_to_icon_url_or_none
from geometries.apps import set_rgba_opacity

import json


def get_js_for_icon_style(icon_url, scale=1, key='image', rotation=0, anchor_x=0.5, anchor_y=0.5 ):
	"""
	image: new ol.style.Icon({	color: [255, 255, 255, 0.6], width: 2, }) // outline
	"""

	tuples = []

	# add ancor point definition
	if anchor_x != 0.5 or anchor_y != 0.5:
		tuples.append(('anchorXUnits','fraction'))
		tuples.append(('anchorYUnits','fraction'))
		tuples.append(('anchor', [ anchor_x, anchor_y ] ))

	if rotation > 0:
		tuples.append(('rotation', rotation))

	tuples.append(('src', icon_url))

	return ''.join( [ key + ': new ol.style.Icon('
			, json.dumps( { t[0]: t[1]  for t in tuples } ), ')'
			] )

#	return ''.join( [ key + ': new ol.style.Icon('
#			, json.dumps( { t[0]: t[1]  for t in [
#					#('anchor', [0.5, 46]),
#					#('anchorXUnits', 'fraction'),
#					#('anchorYUnits', 'pixels'),
#					('src', icon_url),
#					('rotation', rotation),
#					#('color', '#' + color if color else settings.UNKNOWN_STYLE_COLOR ),
#					#('width', width if width else 1),
#				] } )
#			, ')' ] )
#


def get_js_for_stroke_style(color, width, dash_line=None, dash_space=None, key='stroke'):
	"""
	stroke: new ol.style.Stroke({	color: [255, 255, 255, 0.6], width: 2, }) // outline
	"""

	stroke_styles = []
	stroke_styles.append(('color', hex_to_rgb_css(color) ),)
	stroke_styles.append(('width', width),)

	if dash_line:

		# ensure line length min 1 px
		line_length = max(dash_line*(width) - width, 1)

		# ensure space width min 1 px
		line_space = max(dash_space*(width) + width, 1)

		stroke_styles.append(('lineDash', [ line_length, line_space ]),)
		# stroke_styles.append(('lineOffset', (width) ),)
		stroke_styles.append(('lineCap', 'round'),) # 'butt', 'round'

	return ''.join( [ key + ': new ol.style.Stroke('
			, json.dumps( { t[0]: t[1]  for t in stroke_styles } )
			, ')' ] )



def get_js_for_fill_style(color, key='fill'):
	"""
	fill: new new ol.style.Fill( {	color: [255, 255, 255, 0.6], })
	"""

	color = color if color else settings.UNKNOWN_STYLE_COLOR

	return ''.join( [ key + ': new ol.style.Fill('
			, json.dumps( { t[0]: t[1]  for t in [
					# ( 'color', '#' + color if color else settings.UNKNOWN_STYLE_COLOR ),
					( 'color', hex_to_rgb_css(color) ),
				] } )
			, ')' ] )


# def get_js_for_circle_style(radius, color, outline_color, outline_width, key='circle'):
	# """
	# Puts a circle as an icon
	# """
	# return ''.join( [ key + ': new ol.style.Circle('
			# , get_js_for_stroke_style(outline_color if outline_color else settings.DEFAULT_LABEL_OUTLINE_COLOR, outline_width if outline_width else settings.DEFAULT_LABEL_OUTLINE_WIDTH), ','
			# , get_js_for_fill_style(color), ','
			# , ')' ] )
# """
		# new ol.style.Circle({
		  # radius: 20,
		  # fill: new ol.style.Fill({
			# color: '#ff9900',
			# opacity: 0.6
		  # }),
		  # stroke: new ol.style.Stroke({
			# color: '#ffcc00',
			# opacity: 0.4
		  # })
		# })
# """


def hex_to_rgb_css(hex_string):
	"""
	Converts color expression from HEX ('#FFEEFFAA') notation to a list of RGB(A) values
	"""

	hex = str(hex_string).lstrip('#')
	hlen = len(hex)

	rgb_tuple = list(int(hex[i:i + hlen // 3], 16) for i in range(0, hlen, hlen // 3))

	# convert alpha value to decimal <= 1
	if len(rgb_tuple) > 3:
		rgb_tuple[3] = rgb_tuple[3] / 255

	logger.debug('COLOR {} > {}'.format(hex_string, rgb_tuple))

	# rgba(255, 255, 255, 0.5)
	return 'rgba({})'.format(','.join(str(i) for i in rgb_tuple))

	#[255, 255, 255, 0.6]
	#return '[{}]'.format(','.join(str(i) for i in rgb_tuple))


def get_js_for_text_style(color, dims, scale=None, outline_color=None, outline_width = None, key='text'):

	offset_x = round(settings.GUI_ICON_SIZE_PX / 2)

	return ''.join( [ key + ': new ol.style.Text({'
			, 'offsetX: ' + str(offset_x) + ',' if dims == 0 else ''
			, 'placement: "line", textBaseline: "bottom",' if dims == 1 else 'textAlign: "end",'
			, 'scale: {},'.format(scale if scale else 1)
			, get_js_for_stroke_style(outline_color if outline_color else settings.DEFAULT_LABEL_OUTLINE_COLOR, outline_width if outline_width else settings.DEFAULT_LABEL_OUTLINE_WIDTH), ','
			, get_js_for_fill_style(color), ','
			#, json.dumps( { t[0]: t[1]  for t in [
			#		('color', '#' + color if color else settings.DEFAULT_LABEL_SIZE_OUTLINE_COLOR ),
			#		('width', width if width else settings.DEFAULT_LABEL_SIZE_OUTLINE_WIDTH),
			#	] } )
			, '})' ] )





def js_var_style_text_for_openlayers_or_none(style, style_defs, status, dims, base_color):
	"""
	'var style_id_slug = new ol.style.Style(...);'

	style: new Style({
		fill: new Fill({
		  color: 'red'
		}),
		stroke: new Stroke({
		  color: 'white'
		})
	})
	"""

	slug = style['slug']

	# 'LineStyle': [ ('width', '1'), ('color', settings.WATER_DISTRIBUTION_LINE_STYLE_COLOR ), ],
	# , stroke: new ol.style.Stroke({	color: [255, 255, 255, 0.6], width: 2, }) // outline

	elements = []

	# check for the "text" style_def; if set the request for "color" will result in a non-"None" value
	label_color = get_style_def_or_default(style_defs, 'text', 'color', default=base_color)

	if label_color:
		label_scale = get_style_def_or_default(style_defs, 'text', 'scale', default=1)
		elements.append(get_js_for_text_style(label_color, dims, label_scale, ))


	# Point geometries
	if dims == 0:

		# put an image as icon, if available
		icon_url = style['icon_url'] if 'icon_url' in style else None

		anchor_x = get_style_def_or_default(style_defs, 'icon', 'anchor_x', default=0.5)
		anchor_y = get_style_def_or_default(style_defs, 'icon', 'anchor_y', default=0.5)

		if icon_url:
			elements.append(get_js_for_icon_style(icon_url, rotation=0, anchor_x=anchor_x, anchor_y=anchor_y))


	# Line geometries
	if dims == 1:

		width = get_style_def_or_default(style_defs, 'line', 'width', default=1)

		if width:
			color = get_style_def_or_default(style_defs, 'line', 'color', default=base_color)

			dash_space = None
			dash_line = None

			# FIXME: ugly, as it requires manual sync with geometries.views.py
			if status > 0:
				dash_space = 3
				dash_line = 3
				color = set_rgba_opacity(color, settings.PROPOSED_FEATURES_OPACITY)

			elif status < 0:
				dash_space = 3
				dash_line = 1
				color = set_rgba_opacity(color, settings.DECOMISSIONED_FEATURES_OPACITY)

			elements.append(get_js_for_stroke_style(color, width, dash_line=dash_line, dash_space=dash_space, key='stroke'))


	# polygon geometries
	if dims == 2:

		feature_fill_color = get_style_def_or_default(style['style_defs'], 'polygon', 'fill', default=None)

		if feature_fill_color:
			elements.append(get_js_for_fill_style(feature_fill_color))

		feature_outline_color = get_style_def_or_default(style['style_defs'], 'polygon', 'color', default=base_color)
		feature_outline_width = get_style_def_or_default(style['style_defs'], 'polygon', 'width', default=None)
		# width = get_style_def_or_default(style_defs, 'polygon', 'width', default=None)

		elements.append(get_js_for_stroke_style(feature_outline_color, feature_outline_width, dash_line=None, dash_space=None, key='stroke'))

	response = 'var {} = new ol.style.Style({{ {} }});'.format(
		slug,
		','.join(elements)
	)

	return response

