from django.conf import settings
import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.DEBUG)

from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.views.generic.base import RedirectView
from django.contrib.auth.decorators import login_required, permission_required

# from django.conf.urls.i18n import i18n_patterns

from . import views


app_name = 'mapping'


urlpatterns = [
	# redirect to app's index page
	path('', RedirectView.as_view(url='water_supply/', permanent=True), name='map'),

	# plain map without preset layers
	path('watsan/', views.WatSanMap.as_view(), name='watsan'),
]


# register the map views.
def map_tabs():

	tabs = []

	if settings.REQUIRE_AUTHENTICATION:
		tabs.append(path('water_supply/', login_required(views.Water_Supply.as_view()), name='water_supply'),)
		tabs.append(path('sanitation/', login_required(views.Sanitation.as_view()), name='sanitation'),)
		tabs.append(path('hm_water_supply/', login_required(views.HM_Water_Supply.as_view()), name='hm_water_supply'),)
	else:
		tabs.append(path('water_supply/', views.Water_Supply.as_view(), name='water_supply'),)
		tabs.append(path('sanitation/', views.Sanitation.as_view(), name='sanitation'),)
		tabs.append(path('hm_water_supply/', views.HM_Water_Supply.as_view(), name='hm_water_supply'),)



	return tabs


urlpatterns += map_tabs()
