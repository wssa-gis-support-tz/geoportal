from django.conf import settings
import logging
logger = logging.getLogger(__name__)

if not settings.DEBUG: logger.setLevel(settings.DEFAULT_LOG_LEVEL)
else: logger.setLevel(logging.DEBUG)

from django.views.generic.base import View
from django.http import Http404

from django.shortcuts import render
from django.utils.translation import ugettext as _

from geometries import models
from geometries.apps import setup_db_views

from django.contrib.gis.db.models import Extent, Transform
from django.contrib.gis.db.models.functions import Transform

from datetime import datetime

from geometries.models import get_requested_model_categories_or_none
from geometries.apps import get_list_of_models_with_layer_definitions

# from geometries.views import expand_classification_to_style

from geometries.apps import get_html_parameter_or_none

from .openlayers import get_style_def_or_default
from geometries.styles import expand_classification_to_style

import json

from . import openlayers

if settings.DEBUG:
	import time



def navbar_context(self):
	"""
	The navbar is visible on all views, so some context is always required to be rendered.
	"""
	return { 'settings': settings }



def get_buffered_extents_or_empty_list(extents, absolute_buffer=0, buffer_percent=0):
	"""
	Adds spatial buffers to a given extent to not show the outer ones tied to the map boundary
	Relative buffer first, then the absolute
	"""

	if not extents or float('inf') in extents or float('-inf') in extents:
		logger.warn('Got infinite values for buffered extents!')
		return []

	relative_x = 0
	relative_y = 0

	if buffer_percent > 0:
		relative_x = (extents[2] - extents[0]) * buffer_percent/100
		relative_y = (extents[3] - extents[1]) * buffer_percent/100

	new_extents = list([
		extents[0] - (absolute_buffer + relative_x),
		extents[1] - (absolute_buffer + relative_y),
		extents[2] + (absolute_buffer + relative_x),
		extents[3] + (absolute_buffer + relative_y),
	])

	logger.debug('Buffered extents: "{}"'.format(new_extents))
	return new_extents


def get_extents_from_list_of_dict_or_empty_list(the_list, key_name):

	all_items_with_extents = [ item for item in the_list if 'extents' in item and item['extents'] ]

	extents = []

	if all_items_with_extents:
		extents = list([
			min(all_items_with_extents, key=lambda x: x['extents'][0])['extents'][0],
			min(all_items_with_extents, key=lambda x: x['extents'][1])['extents'][1],
			max(all_items_with_extents, key=lambda x: x['extents'][2])['extents'][2],
			max(all_items_with_extents, key=lambda x: x['extents'][3])['extents'][3],
		])

	if float('inf') in extents or float('-inf') in extents:
		logger.warn('Got infinite values for extents!')
		return []

	logger.debug('Got extents "{}"'.format(extents, key_name))
	return extents



class WatSanMap(View):
	"""
	This view generates all variables needed to create a webmapping and provides the elements for the side panel/legend of a webmapping page


	tables
	table			water_tanks
					water_pipes

	layers
	layer			water_tanks (geom)
					water_tanks (proposed_geom)

	categories
	category		by structure
					by age

	classifications
	classification	elevated
					ground

	styles
	style			elevated existing
					elevated proposed

	"""

	# variables modified by constructor
	template_name = 'page-webmapping.html'

	topic_layer_categories = []

	# activate this accordion by default
	topic_layer_main_feature = None


	def __init__(self, topic_layer_categories=topic_layer_categories, template_name=template_name):
		"""
		Classes inheriting from here will have to set these variables
		"""

		self.template_name = template_name
		self.topic_layer_categories = topic_layer_categories


	def get(self, request):
		"""
		Serving the user's GET request with a HTML Response
		"""

		# performance benchmarking
		if settings.DEBUG: start_time = time.time()


		# a fresh request means, layer classification might change: initiate a reload of cached layer definitions,
		# otherwise you may miss some features on the map
		for item in get_list_of_models_with_layer_definitions():
			item.layers = []
			logger.debug('LAYER reset, as DB change was initiated: "{}"'.format(item))


		# used to enrich the url parameter list with some key/value without reloading the page. This requires some JS lines in the template.
		injected_html_parameter = []

		db_has_changed = False

		# allow for a html parameter called "db" to switch the source geodatabase on-the-fly
		db = get_html_parameter_or_none(request, 'db', settings.DATABASES['geodatabase']['NAME'], expected_datatype='text', list_of_key_value_tuples=injected_html_parameter)
		#db = get_html_parameter_or_none(request, 'db', settings.DATABASES['geodatabase']['NAME'], expected_datatype='text')
		if db != settings.DATABASES['geodatabase']['NAME']:
			# On DB change, we must assume that also the meterialized views require a full setup
			setup_db_views(db=db)# vs a refresh only: setup_db_views(db=db, refresh_existing=True)
			db_has_changed = True


		# if the request was to refresh the metarialized views only; initiate it
		refresh = get_html_parameter_or_none(request, 'refresh', False, expected_datatype='boolean')
		if not db_has_changed and refresh:
			# refresh materializes views on request; if the views have not been setup anyway ...
			setup_db_views(db=db, refresh_existing=True)



		"""
		Prepare the design horizons
		"""

		design_horizon = {}

		# the set of design horizons will be populated when loading the shown layers
		set_of_design_horizons = set()

		# set an active design horizon, to be used for this mapping
		design_horizon['active'] = get_html_parameter_or_none(request, 'design_horizon', datetime.now().year, expected_datatype='integer', list_of_key_value_tuples=injected_html_parameter )
		set_of_design_horizons.add(design_horizon['active'])
		set_of_design_horizons.add(datetime.now().year)


		"""
		Picklist with the coordinate reference systems as a dict
		e.g. { 'name': 'ARC1960 UTM Z36S', 'code': 21036, 'utm': True, 'proj4_def': '+proj=utm +zone=36 +south +ellps=clrk80 +towgs84=-160,-6,-302,0,0,0,0 +units=m +no_defs'},
		"""

		crs = {}

		# get crs from settings
		try:
			crs['list'] = settings.CRS_OPTIONS

			# add FAILSAFE (= WGS84) to the list, if it's not there as it is used for the current viewport
			if not settings.FAILSAFE_CRS_OPTION in settings.CRS_OPTIONS:
				crs['list'].append(settings.FAILSAFE_CRS_OPTION)

		except:
			# do not fail here to keep page running, hoping the projection is set subsequently
			logger.warn('No entry for webmapping coordinate reference system in settings.CRS_OPTIONS = [ ... ] list. Will use settings.FAILSAFE_CRS_OPTION, ensure it is defined.')
			crs['list'] = [ settings.FAILSAFE_CRS_OPTION, ]


		# get active crs from html requst or otherwise set the active crs to the first entry in epsg list
		epsg = get_html_parameter_or_none(request, 'epsg', (crs['list'])[0]['epsg'], expected_datatype='integer', list_of_key_value_tuples=injected_html_parameter)
		crs['active'] = [ the_crs for the_crs in crs['list'] if the_crs['epsg'] == epsg ][0]
		crs['safe'] = [ the_crs for the_crs in crs['list'] if the_crs['epsg'] == settings.FAILSAFE_CRS_OPTION['epsg'] ][0]
		logger.debug('Active EPSG is {}, failsafe EPSG is {}'.format(crs['active']['epsg'], crs['safe']['epsg']))


		"""
		Determine, if Proposed and Former features should be shown
		"""

		existing_only = get_html_parameter_or_none(request, 'existing_only', False, expected_datatype='boolean', list_of_key_value_tuples=injected_html_parameter)

		"""
		Prepare the layers to show
		"""

		# Prepare list of requested layers
		requested_layer_categories = []

		# 1st: define layers that can be requested via HTML parameter:
		# layers are requested by their slug as the key, the category as it's value
		possible_models = get_list_of_models_with_layer_definitions()
		possible_layers = [ layer.the_layers(design_horizon['active']) for layer in possible_models ]


		for layer_list_of_models in list(filter(None, possible_layers)):

			# logger.info(layer_list_of_models)

			for layer_slug in [ layer['slug'] for layer in layer_list_of_models ]:

				# check if the layer is requested by testing for it's category
				category_slug = get_html_parameter_or_none(request, layer_slug, None, expected_datatype='text', list_of_key_value_tuples=injected_html_parameter )

				if category_slug:
					requested_layer_categories.append({
						'layer_slug': layer_slug,
						'category_slug': category_slug,
						'is_a_topic_layer_main_feature': False,
					})


		# 2nd: define default layers with their default categories that are always visible
		default_layer_categories = [
				{
					'layer_slug': 'administrative_boundaries_geom',
					'category_slug': 'regions',
					'is_a_topic_layer_main_feature': False,
				}
			]


		# finally, register the topic- and default layers
		for layer_category in self.topic_layer_categories + default_layer_categories:
			if not layer_category['layer_slug'] in [ already_in_list['layer_slug'] for already_in_list in requested_layer_categories ]:

				category_slug = get_html_parameter_or_none(request, layer_category['layer_slug'], layer_category['category_slug'], expected_datatype='text', list_of_key_value_tuples=injected_html_parameter )

				# logger.debug('Got category: {}'.format(category_slug))

				requested_layer_categories.append({
					'layer_slug': layer_category['layer_slug'],
					'category_slug': category_slug,
					'is_a_topic_layer_main_feature': layer_category['is_a_topic_layer_main_feature'] or False,
				})


		# at this point, we know what the user want to see on his map and how


		# topic = None
		layers = []


		"""
		layer:

		'topic': _('Water Supply'),
		'model_name': cls.__name__,
		'geometry_field': 'geom',
		'epsg': geometry_info['epsg'] if geometry_info else None,
		'dims': geometry_info['dims'] if geometry_info else None,
		'name': str(layer_name),
		'slug': slugify_javascript_var(layer_name),
		'zoom_to_extents': True,


		category:

		'name': 'by purpose',
		'description': _('Features are classified by their purpose ("PipePurpose field"). This helps to separate infrastructure belonging to the water transmission (= Water production) from the water distributon/supply (= water distribution and consumer connections) system.'),
		'min_scale': 100000,
		'max_scale': 1000,
		'classifications': cls.by_purpose(cls, 'geom'),
		"""

		# iterate over the requested layer categories
		for requested_layer_category in requested_layer_categories:
			"""
			'name': 'by structure',
			'description': _('Features are classified by the tank\'s structure. This refers to the tanks\'s distance from low water level to the ground ("Ground_to_LWL_M" field). Negative values refer to underground tanks and pump sumps, resonable high values identify elevated tanks'),
			'min_scale': 100000,
			'max_scale': 1000,
			'classifications':
			"""

			potential_layer_category_instance = get_requested_model_categories_or_none(requested_layer_category['layer_slug'], requested_layer_category['category_slug'], design_horizon['active'])

			if not potential_layer_category_instance:
				logger.warn('Requested Layer/Category combination "{}/{}" not found, skipping'.format(requested_layer_category['layer_slug'], requested_layer_category['category_slug']))
				continue

			# for convenience
			layer = potential_layer_category_instance['layer']
			layer['category_options'] = potential_layer_category_instance['category_options']


			# Barrier 0: skip all further action, if layer is empty
			if not 'epsg' in layer or not 'dims' in layer or not layer['epsg']:
				logger.warn('Layer "{}" has no CRS and/or EPSG, maybe it has no features. Skip it.'.format(layer['name']))
				continue

			"""
			layer['categories'] v
			layer['category']
				category['classifications']
					classification['styles']
			"""

			category = potential_layer_category_instance['category']

			classifications = []

			for classification in category['classifications']:
				"""
				'name': 'elevated'
				'queryset':
				'label':
				'icon':
				'color':
				'style_defs':
				---
				'styles': []
				'extents'
				"""

				styles = []

				# three variants of a classification form a style: past, current and future
				for style in expand_classification_to_style(layer['slug'], category['slug'], classification, design_horizon['active']):
					"""
					'name': 'elevated existing'
					'slug':
					'status': -1|0|1 = former, existing, proposed
					---
					'extents'
					"""

					features = style['queryset']


					# barrier 1: if query returned no features, skip it. Otherwise we'll end up in subsequent error messages
					style['number_of_features'] = features.count()

					if not style['number_of_features'] > 0:
						logger.info('Layer "{}", category "{}" / style "{}" has no features, skipping'.format(layer['name'], category['name'], style['name']))
						continue


					# populate design_horizon picklist with all possible start and end years of features in this layer
					unique_start_years = style['queryset'].values('year_start', 'year_end').distinct()
					for new_design_horizon in unique_start_years:
						set_of_design_horizons.add(new_design_horizon['year_start'])
						if not new_design_horizon['year_start'] == new_design_horizon['year_end']:
							set_of_design_horizons.add(new_design_horizon['year_end'])


					# barrier 2: do not add style if it is decomissioned or proposed and only existing features were requested
					if existing_only and not style['status'] == 0:
						logger.debug('Skip displaying layer "{}", category "{}" / style "{}", as only existing features are requested'.format(layer['name'], category['name'], style['name']))
						continue


					# at the point, style contains at least one feature and is existing/proposed/decomissioned as per request
					logger.debug('Layer "{}" with category "{}" will be shown with {} features in style "{}"'.format(layer['name'], category['name'], style['number_of_features'], style['name']))

					# add a "spatial extents" key and add a marginal buffer around the feature to ensure it's viewport is 2-dimensional
					# add this geometry to map view extents, if 'zoom_to_extents' is set in it's dict
					if 'zoom_to_extents' in layer and layer['zoom_to_extents']:

						# add some buffer around shown objects
						absolute_buffer = 100 # metre

						# we use a failsafe crs for bounding boxes
						the_extents = features.aggregate(extents=Extent(Transform(layer['geometry_field'], crs['safe']['epsg'] )))

						if not crs['safe']['utm']:
							# FIXME: if CRS is not UTM, make a good guess, but it's not so nice to do the translation with a hardcoded scale factor
							absolute_buffer = absolute_buffer / settings.WGS84_TO_UTM_SCALE_FACTOR

						style['extents'] = get_buffered_extents_or_empty_list(the_extents['extents'], absolute_buffer=absolute_buffer, buffer_percent=10)

					else:
						style['extents'] = []

					# openlayers style string
					# base_color here, because access to the base_color is not possible later
					# FIXME: base_color != label_color
					base_color = style['base_color'] if 'base_color' in style else None
					# style['var_ol_style'] = openlayers.js_var_style_text_for_openlayers_or_none(style['slug'], classification['style_defs'], style['status'], layer['dims'], base_color=base_color )
					style['var_ol_style'] = openlayers.js_var_style_text_for_openlayers_or_none(style, classification['style_defs'], style['status'], layer['dims'], base_color=base_color )


					"""
					'extents': None or BB
					'slug': ''
					"""

					styles.append(style)
					# logger.debug('Style "{}" added'.format(style))


				if not styles:
					logger.warn('No styles with features in classification "{}"'.format(classification['name']))
					continue


				classification['styles'] = styles
				classification['extents'] = get_extents_from_list_of_dict_or_empty_list(styles, 'extents')
				classifications.append(classification)
				logger.debug('Classification "{}" added'.format(classification['name']))

				"""
				layer['categories'] v
				layer['category']
					category['classifications']
						classification['styles'] v
				"""

			if not classifications:
				logger.warn('No classifications found for layer "{}", category "{}"'.format(layer['name'], category['name']))
				continue


			# overwrite
			category['classifications'] = classifications
			category['extents'] = get_extents_from_list_of_dict_or_empty_list(classifications, 'extents')

			layer['extents'] = category['extents']
			layer['category'] = category


			# identify the active accordion first matching is choosen
			if not self.topic_layer_main_feature and requested_layer_category['is_a_topic_layer_main_feature']:
				self.topic_layer_main_feature = layer['topic']


			layers.append({
				**layer,
				# **{'extents': get_extents_from_list_of_dict_or_empty_list() },
			})

			"""
			layer['categories'] v
			layer['category'] v
				category['classifications'] v
					classification['styles'] v
			"""

		if not layers:
			logger.error('No layer have been set up')
			raise Http404('Configuration error: No layers have been set up')


		# sort the layers by their group to allow for regrouping them in the template
		sorted_layers = sorted(layers, key=lambda x: (x['topic']), reverse=False) # if regroup_by else eintraege_dict


		maps = []
		maps.append({'href': '/map/water_supply', 'topic': _('Water Supply'), 'label': _('Water supply system') + ' ' + _('Overview'), 'disabled': False, })
		maps.append({'href': '/map/sanitation', 'topic': _('Sanitation'), 'label': _('Sanitation system') + ' ' + _('Overview'), 'disabled': False, })

		if 'test' in db:
			maps.append({'href': '/map/hm_water_supply', 'topic': _('Water network analysis'), 'label': _('Water network analysis'), 'disabled': False, })

		#if request.user.is_staff:
		#	map_tabs.append({'href': '#', 'topic': _('Incidents'), 'disabled': not request.user.is_staff, })
		#	map_tabs.append({'href': '#', 'topic': _('Hydraulic modelling'), 'disabled': not request.user.is_superuser, })


		# sort the list of design horizons and wurn it into a list
		design_horizon['list'] = sorted(list(filter(None, set_of_design_horizons)))


		# Determine the bounding boxes of current and full map extents
		extents = {}
		extents['full'] = get_extents_from_list_of_dict_or_empty_list(layers, 'extents')

		# if the db was just changed, reset the extents
		if not db_has_changed:
			extents['active'] = get_html_parameter_or_none(request, 'extents', str(extents['full']), list_of_key_value_tuples=injected_html_parameter)
		else:
			extents['active'] = extents['full']


		context = {
			'existing_only': existing_only, # boolean - show only existing features or also former and proposed
			'extents': extents,
			'crs': crs,
			'design_horizon': design_horizon,
			'layers': sorted_layers,
			'injected_html_parameter': injected_html_parameter, # add these parameters via javascript without reloading the page
			'maps': { 'list': maps },
			'topic_layer_main_feature': self.topic_layer_main_feature,
			'bg_map': get_html_parameter_or_none(request, 'bg_map', settings.DEFAULT_BG_MAP_VARIABLE_NAME, list_of_key_value_tuples=injected_html_parameter),
			}

		# merge other context dict into context
		context = {**context, **navbar_context(self)}

		if settings.DEBUG: logger.info('--- Preparing the webpage took {} msec ---'.format(((time.time() - start_time) * 1000)))

		return render(request, self.template_name, context)


class HM_Water_Supply(WatSanMap):
	"""
	Define the Water-related webmappig page
	"""

	def __init__(self):
		template_name = 'page-webmapping.html'
		topic_layer_categories = [
				{'layer_slug': 'water_network_nodes_geom', 'category_slug': 'by_node_type'},
				{'layer_slug': 'water_pipes_geom', 'category_slug': 'by_purpose'},
			]

		for topic_layer_category in topic_layer_categories:
			topic_layer_category['is_a_topic_layer_main_feature'] = True

		#logger.info('Default layers for water supply are shown.'.format(len(default_layers)))
		super(HM_Water_Supply, self).__init__(topic_layer_categories=topic_layer_categories, template_name=template_name)



class Water_Supply(WatSanMap):
	"""
	Define the Water-related webmappig page
	"""

	def __init__(self):
		template_name = 'page-webmapping.html'
		topic_layer_categories = [
				{'layer_slug': 'water_pipes_geom', 'category_slug': 'by_size'},
				{'layer_slug': 'water_sources_geom', 'category_slug': 'by_type'},
				{'layer_slug': 'water_tanks_geom', 'category_slug': 'by_structure'},
				{'layer_slug': 'water_pumps_geom', 'category_slug': None},
				{'layer_slug': 'water_fittings_geom', 'category_slug': 'by_type'},
				{'layer_slug': 'water_consumers_geom', 'category_slug': 'by_tariff'},
			]

		for topic_layer_category in topic_layer_categories:
			topic_layer_category['is_a_topic_layer_main_feature'] = True

		#logger.info('Default layers for water supply are shown.'.format(len(default_layers)))
		super(Water_Supply, self).__init__(topic_layer_categories=topic_layer_categories, template_name=template_name)



class Sanitation(WatSanMap):
	"""
	Define the Sanitation-related webmappig page
	"""

	def __init__(self):
		template_name = 'page-webmapping.html'
		topic_layer_categories = [
				{'layer_slug': 'sewer_conduits_geom', 'category_slug': 'by_hierarchy'},
#				{'layer_slug': 'sanitation_areas_geom', 'category_slug': 'by_type'},
			]

		for topic_layer_category in topic_layer_categories:
			topic_layer_category['is_a_topic_layer_main_feature'] = True

		#logger.info('Default layers for sanitation are shown.'.format(len(default_layers)))
		super(Sanitation, self).__init__(topic_layer_categories=topic_layer_categories, template_name=template_name)
