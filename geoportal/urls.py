"""geoportal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.urls import include, path
	2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.views.static import serve as staticserve


from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.views.generic.base import RedirectView
from django.contrib.auth.decorators import login_required, permission_required

from . import views


urlpatterns = [
	# admin, translations and login handling
	path('admin/', admin.site.urls), # administrate users
	path('i18n/', include('django.conf.urls.i18n')), # translations
	path('login/', views.LoginView.as_view(), name="login" ), # Login
	path('logout/', views.logout_user, name="logout" ), # Logout



	# subpages

	# webmapping
	path('map/', include('mapping.urls', namespace="map")),

	# geometry and icon source
	# TODO: separate icon into own app
	path('geometry/', include('geometries.urls', namespace="geometry")),
]


if settings.REQUIRE_AUTHENTICATION:
	urlpatterns.append(path('', login_required(RedirectView.as_view(url='map/', permanent=True)), name='index')) # index
else:
	urlpatterns.append(path('', RedirectView.as_view(url='map/', permanent=True), name='index')) # index


# The following code ensures Django will serve static files and media files with DEBUG=False turned on in settings.py (or .env). This avoids starting the development server with "--insecure" parameter
# TODO: disable the following lines when fully deploying the app with a separate webserver


if not settings.DEBUG:
	urlpatterns += [
		url(r'^media/(?P<path>.*)$', staticserve, { 'document_root': settings.MEDIA_ROOT, }),
		url(r'^static/(?P<path>.*)$', staticserve, { 'document_root': settings.STATIC_ROOT }),
	]
