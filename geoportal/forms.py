# -*- coding: utf-8 -*-

from django.conf import settings
import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL)

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.core.validators import validate_email
# from django.shortcuts import get_object_or_404

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Reset, HTML, Layout, Div, Field # Button, Row, , Fieldset, ButtonHolder

from django.utils.translation import gettext_lazy as _



class LoginForm(AuthenticationForm):

	def __init__(self, *args, **kwargs):
		super(LoginForm, self).__init__(*args, **kwargs)

		#language = translation.get_language_from_request(self.request)
		#for field in fields:
		#	field.activate(language)

		# set autofocus to first visible field
		self.visible_fields()[0].field.widget.attrs.update({'autofocus': 'autofocus', })

		# Form Layout
		self.helper = FormHelper(self)

		# self.helper.form_show_labels = False

		# manual field order
		self.helper.layout = Layout(
#			Div(
#				Div(
#					HTML("""<span class="input-group-text" style="width: 3em;" id="basic-addon1">@</span>""")
#					, css_class="input-group-prepend"
#				)
				Field('username', tabindex="-1", autofocus=True, placeholder=_("Login name"), css_class="form-control", aria_label="Login name", aria_describedby="basic-addon1", wrapper_class="")
#				, css_class="input-group my-3"
#			)
#			, Div(
#				Div(
#					HTML("""<span class="input-group-text" style="width: 3em;" id="basic-addon2">#</span>""")
#					, css_class="input-group-prepend"
#				)
				, Field('password', tabindex="0", placeholder=_("Password"), css_class="form-control", aria_label="Password", aria_describedby="basic-addon2", wrapper_class="")
#				, css_class="input-group my-3"
#			)
			, Submit('submit', _('Login'))
		)



	@staticmethod
	def resolve_username_or_none(login_string):
		"""
		If login appears to be an email address, resolve it to the corresponding username and continue
		"""
		try:
			validate_email(login_string)
			the_user = (User.objects.get(email=login_string)).username
			logger.info("Email <{0}> belongs to user: <{1}>".format(login_string, the_user))
		except User.DoesNotExist:
			return None
		except forms.ValidationError:
			# if no emailaddress, handle string as username
			the_user = login_string

		return the_user



	def clean_username(self):
		"""
		Differenzierte Meldungen an den user, warun der Login nicht klappt
		"""

		# force username lowercase/case insensitive
		# FIXME: tanke case all new registered users are lowercase, too!
		entered_login = self.cleaned_data['username'] #.lower()

		if not entered_login:
			logger.error('No Login entered')
			msg = _("No Login entered")
			err = forms.ValidationError(msg, code='invalid')
			raise err

		the_user = LoginForm.resolve_username_or_none(entered_login)

		# email not knows
		if not the_user:
			msg = _("Emailaddress is not assigned to any registered user")
			err = forms.ValidationError(msg, code='invalid')
			raise err

		# user not registered
		try:
			user = User.objects.get(username=the_user)
		except User.DoesNotExist:
			msg = _("User not registered")
			err = forms.ValidationError(msg, code='invalid')
			raise err

		# user inaktive
		if not user.is_active:
			msg = _("User exists, but it is inactive.")
			err = forms.ValidationError(msg, code='invalid')
			# self.add_error('username', err) # raise Field Error
			raise err # Non-Field Error

		return the_user
