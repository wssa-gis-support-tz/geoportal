from django.http import HttpResponse

from django.conf import settings

import logging
logger = logging.getLogger(__name__)
logger.setLevel(settings.DEFAULT_LOG_LEVEL)

from django.views.generic import FormView
from django.views.generic.base import View

from django.template import RequestContext
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render
from django.utils.translation import ugettext as _

from geometries.apps import setup_db_views

from . import forms
from geometries.apps import get_html_parameter_or_none



class LoginView(FormView):
	"""
	Logic to log in a user and redirect
	"""
	form_class = forms.LoginForm
	template_name = "page-login.html"
	extra_context = { 'settings': settings }

	def form_valid(self, form):
		"""
		Authenticate user, if credentials were valid
		TODO: always safe URLS!!!!
		"""
		username = form.cleaned_data['username']
		password = form.cleaned_data['password']
		user = auth.authenticate(username=username, password=password)

		if user is not None and user.is_active:
			auth.login(self.request, user)
			logger.info('{} authenticated'.format(user))
			return super(LoginView, self).form_valid(form)
		else:
			logger.warn("Login failed: {0}/{1}".format(username, password))
			return self.form_invalid(form)


	def get_success_url(self):
		"""
		Redirect page
		"""
		next_url = None

		if self.request.GET:
			next_url = self.request.GET.get('next_url', None)
			logger.debug('Will redirect to {} after login.'.format(next_url))

		if next_url:
			# handle redirect, if next_url is present
			return '{}'.format(urllib.parse.unquote(next_url))
		else:
			# without next_url: redirect to index page
			logger.info('No \"{0}\" at <get_succcess_url>, redirect to {1}'.format('next_url', self.request.resolver_match.namespace + ':index'))
			return reverse('index', current_app=self.request.resolver_match.namespace)



def logout_user(request):
	"""
	Logout user an redirect
	TODO: handle if user has no right to remain of redirected page
	"""
	context = RequestContext(request)
	auth.logout(request)
	next_url = request.GET.get('next_url', None)

	logger.info('About to log out user {}'.format(request.user))

	if next_url:
		return HttpResponseRedirect('{}'.format(next_url))
	else:
		return HttpResponseRedirect('/')



# class Chooser(View):
	# """
	# """

	# # variables modified by constructor
	# template_name = 'geoportal/db_chooser.html'


	# def get(self, request):
		# """
		# Serving the user's GET request with a HTML Response
		# """

		# # test id a db is already requested and redirect accordingly:
		# db = get_html_parameter_or_none(request, 'db', None, expected_datatype='text')

		# if db:

			# # setup production views and db
			# setup_db_views(db=db)

			# # redirect with a html parameter
			# return HttpResponseRedirect('{}?db={}'.format(reverse('mapping:water_supply'), db))


		# context = {
			# 'db_choices': [
					# { 'prompt': 'SUWASA', 'db_name': 'suwasa' },
					# { 'prompt': 'TANGAUWASA', 'db_name': 'tangauwasa' },
					# { 'prompt': 'TUWASA', 'db_name': 'tuwasa' },
					# { 'prompt': 'AUWSA', 'db_name': 'auwsa' },
					# { 'prompt': 'MUWSA', 'db_name': 'muwsa' },
					# { 'prompt': 'KUWASA', 'db_name': 'kuwasa' },
					# { 'prompt': 'IRUWASA', 'db_name': 'iruwasa' },
					# { 'prompt': 'KUWSA', 'db_name': 'kuwsa' },
					# { 'prompt': 'SOUWASA', 'db_name': 'souwasa' },
					# { 'prompt': 'GEUWASA', 'db_name': 'geuwasa' },
					# { 'prompt': 'SHUWASA', 'db_name': 'shuwasa' },
					# { 'prompt': 'DUWASA', 'db_name': 'duwasa' },
				# ]
			# }

		# # merge other context dict into context
		# # context = {**context, **navbar_context(self)}

		# return render(request, self.template_name, context)
