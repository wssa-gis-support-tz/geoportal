{% load i18n %}



{# REGISTER PROJ4 CRS DEFINITIONS, SEE SETTINGS.PY #}
{% for crs in crs.list %}
	{% if crs.proj4_def %}
		proj4.defs("EPSG:{{ crs.epsg }}", "{{ crs.proj4_def }}");
	{% endif %}
{% endfor %}
ol.proj.proj4.register(proj4);



{% if crs.active.utm %}
	var coordinateFormat_digits = 0
{% else %}
	var coordinateFormat_digits = 5
{% endif %}


{# define the initial extents "safe" to avoid transformation of decimal to comma #}

// extents of the current view
var initial_extents = ol.proj.transformExtent({{ extents.active|safe }}, 'EPSG:{{ crs.safe.epsg }}', 'EPSG:{{ crs.active.epsg }}' );

// extents of the interesting scene
// {{ extents.full }}
var full_extents = ol.proj.transformExtent({{ extents.full|safe }}, 'EPSG:{{ crs.safe.epsg }}', 'EPSG:{{ crs.active.epsg }}' );

// if there are reasons why not to browse the world
// var max_extents = ol.proj.transformExtent([ {{ extents.active.0|safe }}, {{ extents.active.1|safe }}, {{ extents.active.2|safe }}, {{ extents.active.3|safe }}], 'EPSG:{{ crs.active.epsg }}', 'EPSG:{{ crs.active.epsg }}' );


var interactions = ol.interaction.defaults({altShiftDragRotate:false, pinchRotate:false});


// Register the OpenLayers map itself
var {{ map }} = new ol.Map({
	layers: [],
	interactions: interactions,
	target: '{{ map }}',
	view: new ol.View({
		projection: 'EPSG:{{ crs.active.epsg }}',
	})
});


/*
 * Register map controls
 */

// add mouse cursor coordinates control
var mousePositionControl = new ol.control.MousePosition({
	coordinateFormat: function(coordinate) { return ol.coordinate.format(coordinate, '{x}, {y}', coordinateFormat_digits) + ' (EPSG:{{ crs.active.name }}{% if crs.active.utm %} UTM{% endif %})'; },
	projection: 'EPSG:{{ crs.active.epsg }}',
	undefinedHTML: "{% trans 'Mouse out of map' %}" // &nbsp;
});

{{ map }}.addControl(mousePositionControl);


// add control to reset the extents
var zoomToExtentControl = new ol.control.ZoomToExtent({ extent: full_extents });

{{ map }}.addControl(zoomToExtentControl);


// add control for fullscreen view
// fsc = new ol.control.FullScreen({ label: 'F', tipLabel: '{% trans "Fullscreen" %}' })
fsc = new ol.control.FullScreen({ label: '', tipLabel: "{% trans 'Fullscreen' %}" })


{{ map }}.addControl(fsc);





/*
 * Register background maps
 */

var osm = new ol.layer.Tile({
	//opacity: 1,
	preload: 2,
	title: '{% trans "Streetmap" %}: OSM',
	type: 'base',
	visible: false,
	source: new ol.source.OSM()
});

{# track changed base layers #}
osm.on('change:visible', function (e) { if( osm.getVisible())  { setGetParameter('bg_map', 'osm', reload_page = false ); }
});


{{ map }}.addLayer(osm);



{% if settings.BING_MAPS_KEY %}
	var bing_aerial = new ol.layer.Tile({
		//opacity: {{ settings.DEFAULT_BG_MAP_OPACITY|safe }},
		preload: 2,
		title: '{% trans "Satellite image" %}: Bing',
		type: 'base',
		visible: false,
		source: new ol.source.BingMaps({
			imagerySet: 'Aerial',
			key: '{{ settings.BING_MAPS_KEY }}'
		})
	});

	{# track changed base layers #}
	bing_aerial.on('change:visible', function (e) { if( bing_aerial.getVisible())  { setGetParameter('bg_map', 'bing_aerial', reload_page = false ); } });

	{{ map }}.addLayer(bing_aerial);


	var bing_roads = new ol.layer.Tile({
		//opacity: 1,
		preload: 2,
		title: '{% trans "Streetmap" %}: Bing',
		type: 'base',
		visible: false,
		source: new ol.source.BingMaps({
			imagerySet: 'RoadOnDemand',
			key: '{{ settings.BING_MAPS_KEY }}'
		})
	});

	{# track changed base layers #}
	bing_roads.on('change:visible', function (e) { if( bing_roads.getVisible())  { setGetParameter('bg_map', 'bing_roads', reload_page = false ); } });

	{{ map }}.addLayer(bing_roads);

{% endif %}





{% if settings.DEBUG %}
	var wms = new ol.layer.Tile(
		{
			//opacity: {{ settings.DEFAULT_BG_MAP_OPACITY|safe }},
			preload: 2,
			title: 'WMS',
			type: 'base',
			visible: false,
			source: new ol.source.TileWMS(
			{
				url: 'http://elito:8080/service?',
				projection: 'EPSG:4326',
				crossOrigin: 'anonymous',
				params: {
					'LAYERS': 'DOP_global',
					// 'TILED': true,
					// 'FORMAT': 'image/png',
				}
			}
		)}
	);
	{{ map }}.addLayer(wms);
{% endif %}

{{ map }}.getView().fit(initial_extents);


{# the visible background map. Also refer to settings.DEFAULT_BG_MAP_VARIABLE_NAME; use the JS variable name there #}
{{ bg_map }}.setVisible(true);


{# On map move or zoom, update the URL parameter with the new view extents #}
{{ map }}.on('moveend', function (e) {

	// TODO: toggle label visibility on zoom

	// update map extents in url parameter
	var map_extents = {{ map }}.getView().calculateExtent({{ map }}.getSize());
	var current_extents = '[' + ol.proj.transformExtent(map_extents, 'EPSG:{{ crs.active.epsg }}', 'EPSG:{{ crs.safe.epsg }}' ) + ']';

	// do not reload the page here, endless loop otherwise
	setGetParameter("extents", current_extents, false );

	{% if DEBUG %}
	console.log({{ map }}.getView().getResolution() * {{ settings.WGS84_TO_UTM_SCALE_FACTOR }});
	{% endif %}
});


// add control for layer switcher
var layerSwitcher = new ol.control.LayerSwitcher({
	tipLabel: '{% trans "Legende" %}', // Optional label for button
//	groupSelectStyle: 'none' // Can be 'children' [default], 'group' or 'none'
});

{{ map }}.addControl(layerSwitcher);




var ExportPDFControl = /*@__PURE__*/(function (Control) {
  function ExportPDFControl(opt_options) {
    var options = opt_options || {};

    var button = document.createElement('button');
    // button.innerHTML = 'N';

    var element = document.createElement('div');
    element.className = 'export-pdf ol-unselectable ol-control';
    element.appendChild(button);

    ol.control.Control.call(this, {
      element: element,
      target: options.target,
    });

    button.addEventListener('click', this.handleExportPDF.bind(this), false);
  }

  if ( Control ) ExportPDFControl.__proto__ = ol.control.Control;
  ExportPDFControl.prototype = Object.create( ol.control.Control && ol.control.Control.prototype );
  ExportPDFControl.prototype.constructor = ExportPDFControl;

  ExportPDFControl.prototype.handleExportPDF = function handleExportPDF () {
    // this.getMap().getView().setRotation(0);
	export_pdf();
  };

  return ExportPDFControl;
}(ol.control.Control));


{{ map }}.addControl(new ExportPDFControl());

