
---------------------------------------------------
-- Water Sources
---------------------------------------------------
DROP materialized VIEW IF EXISTS "{{ VIEW_WATER_SOURCES }}" cascade;
DROP VIEW IF EXISTS "{{ VIEW_WATER_SOURCES }}" cascade;

create materialized view "{{ VIEW_WATER_SOURCES }}" as
SELECT DISTINCT
	row_number() over (PARTITION BY true) as "{{ ID_FIELDNAME }}"
	--, "{{ SCHEME_FIELDNAME }}"
	--, "{{ OPERATOR_FIELDNAME }}"
	--, "{{ COMPONENT_FIELDNAME }}"
	, "{{ WATER_SOURCE_TYPE_FIELDNAME }}"
	, "{{ LABEL_FIELDNAME }}"
	, "{{ SCHEME_FIELDNAME }}"
	, "{{ WATER_SOURCE_YIELD_FIELDNAME }}"
	, "{{ CONDITION_FIELDNAME }}"
	, "{{ YEAR_START }}"
	, "{{ YEAR_END }}"
	, "{{ REMARK_FIELDNAME }}"
	, null::real as "{{ ELEVATION_FIELDNAME }}"
	--, round((tnk_dpth_m * 0.85)::numeric, 1) as initiallev
	--, 0 as minimumlev
	--, tnk_dpth_m as maximumlev
	--, 0::real as minimumvol
	--, null::text as volumecurv
	--, round((sqrt(volume_cbm/tnk_dpth_m*4/3.1415))::numeric,1) as diameter
	--, null::text as pattern
	---- , null::real as head -- qwater plugin wünscht es
	--, 0.0::real as result_dem
	--, 0.0::real as result_pre
	--, 0.0::real as result_hea
	--, 0.0::real as result_qua
	, "{{ GEOMETRY_FIELD }}"::geometry(POINT, {{ SOURCE_TABLES_CRS }})
FROM (
		(
			SELECT DISTINCT
				trim(both from coalesce("Name", '')) as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, "WatSourcePtType" as "{{ WATER_SOURCE_TYPE_FIELDNAME }}"
				, "YieldCapM3h" as "{{ WATER_SOURCE_YIELD_FIELDNAME }}"
--				, "Ground_to_LWL_M" as "{{ TANK_STRUCTURE_HEIGHT_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				-- "InitialLevelHmM" as
				, st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			FROM "WaterSources"."WaterSourcePt"
--		) UNION (
--			SELECT DISTINCT
--				trim(both from 'BPT ' || coalesce("Name", '')) as "{{ LABEL_FIELDNAME }}"
--				, "TankCapacityM3" as "{{ TANK_CAPACITY_FIELDNAME }}"
--				, "TankDepthM" as "{{ TANK_DEPTH_FIELDNAME }}"
--				, "Ground_to_LWL_M" as "{{ TANK_STRUCTURE_HEIGHT_FIELDNAME }}"
--				, "Condition" as "{{ CONDITION_FIELDNAME }}"
--				, "InstallationDate" as "{{ YEAR_START }}"
--				, "InstallationDate" + coalesce(40) as "{{ YEAR_END }}"
--				, "remark" as "{{ REMARK_FIELDNAME }}"
--				-- "InitialLevelHmM" as
--				, geom as "{{ geometry_field }}"
--			FROM "NetworkAssets"."BreakPressTank"
		)
	) as all_structures
;

create unique index on "{{ VIEW_WATER_SOURCES }}" ("{{ ID_FIELDNAME }}");


	-- execute the refresh of materialized view
	-- FIXME: let the geoportal initiate the refresh, because it's the only instance that is interested in the view; this avoids overhead when the geoportal is not running
	drop function if exists refresh_mv_{{ VIEW_WATER_SOURCES }}() CASCADE;
	-- CREATE OR REPLACE FUNCTION refresh_mv_{{ VIEW_WATER_SOURCES }}() RETURNS trigger LANGUAGE 'plpgsql' AS $BODY$
	-- BEGIN
		-- -- execute format('notify modification, ''%s''', cast(to_char((current_timestamp)::TIMESTAMP,'yyyymmddhhmiss') as BigInt) );
		-- REFRESH MATERIALIZED VIEW CONCURRENTLY "{{ VIEW_WATER_SOURCES }}";
		-- return NULL;
	-- END
	-- $BODY$;


-- --	-- capture the event of source table refresh can call trigger function
-- --	DROP TRIGGER if exists refresh_{{ VIEW_WATER_SOURCES }} on
-- --	"WaterSources"."WaterSourcePt"
-- --	;
	-- CREATE TRIGGER refresh_{{ VIEW_WATER_SOURCES }} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON
	-- "WaterSources"."WaterSourcePt"
	-- FOR EACH STATEMENT EXECUTE PROCEDURE refresh_mv_{{ VIEW_WATER_SOURCES }}();



