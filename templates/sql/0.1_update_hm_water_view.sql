
-- get bend angle
CREATE OR REPLACE FUNCTION get_winkel(s1 geometry, e1 geometry) RETURNS float language sql AS
$body$
	select degrees(ST_Azimuth(s1, e1));
$body$;


-- determine the bend angle
drop function if exists get_bend_degree(geometry, geometry);
CREATE OR REPLACE FUNCTION get_bend_degree(linee geometry, linez geometry) RETURNS real AS $$
	DECLARE
		winkel float;
	BEGIN
	winkel := abs(get_winkel(st_startpoint(linee), st_endpoint(linee)) - get_winkel(st_startpoint(linez),st_endpoint(linez)));
	winkel := case when st_startpoint(linee) = st_startpoint(linez) then abs(180-winkel)
		when st_endpoint(linee) = st_endpoint(linez)
			then abs(180-winkel)
			--	then abs(get_winkel(st_startpoint(linee), st_endpoint(linez)) - get_winkel(st_startpoint(linez),st_endpoint(linee)))
			else winkel end;
	return
		round((CASE WHEN winkel > 180
		THEN winkel-180
		else winkel END)::numeric, 1);
	END;
$$ LANGUAGE plpgsql;




drop table if exists raw_water_pipes;
CREATE table raw_water_pipes AS SELECT DISTINCT
		"{{ PURPOSE_FIELDNAME }}"
		, "{{ MATERIAL_FIELDNAME }}"
		, "{{ INNER_DIAMETER_FIELDNAME }}"
		, (st_dump(
			st_selfsplit(
				"{{ GEOMETRY_FIELD }}"
			)
		)).geom::geometry(LINESTRING, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
		, "{{ YEAR_START }}"
		, "{{ YEAR_END }}"
	from
		(
			(
				-- water pipes table
				SELECT DISTINCT
					"Name"
					, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
					, coalesce("PipePurpose", 'Distribution') as "{{ PURPOSE_FIELDNAME }}"
					, "Location" as "{{ COMPONENT_FIELDNAME }}"
					, "Condition" as "{{ CONDITION_FIELDNAME }}"
					, "Roughness" as "{{ ROUGHNESS_FIELDNAME }}"
					, "LossCoeffHm" as "{{ MINORLOSS_FIELDNAME }}"
					, "Installer" as "{{ OPERATOR_FIELDNAME }}"
					, "NominalPressure" as "{{ PRESSURECLASS_FIELDNAME }}"
					, "remark" as "{{ REMARK_FIELDNAME }}"
					, "Material" as "{{ MATERIAL_FIELDNAME }}"
					, get_diameter_in_mm_or_null("NomDiamMm", "NomDiamInch" ) as "{{ INNER_DIAMETER_FIELDNAME }}"
					--, "NomDiamMm" as "NomDiamMm"
					--, "NomDiamInch" as "NomDiamInch"
					, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
					, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
					, st_transform("geom", {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
				FROM "NetworkAssets"."WaterPipe"
			) UNION (
				-- service lines table
				SELECT DISTINCT
					"Name"
					, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
					, 'ServiceLine' as "{{ PURPOSE_FIELDNAME }}"
					, "Location" as "{{ COMPONENT_FIELDNAME }}"
					, "Condition" as "{{ CONDITION_FIELDNAME }}"
					, "Roughness" as "{{ ROUGHNESS_FIELDNAME }}"
					, 0 as "{{ MINORLOSS_FIELDNAME }}"
					, "Installer" as "{{ OPERATOR_FIELDNAME }}"
					, "NominalPressure" as "{{ PRESSURECLASS_FIELDNAME }}"
					, "remark" as "{{ REMARK_FIELDNAME }}"
					, "Material" as "{{ MATERIAL_FIELDNAME }}"
					, get_diameter_in_mm_or_null("NomDiamMm", "NomDiamInch" ) as "{{ INNER_DIAMETER_FIELDNAME }}"
					--, "NomDiamMm" as "NomDiamMm"
					--, "NomDiamInch" as "NomDiamInch"
					, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
					, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
					, st_transform("geom", {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
				FROM "NetworkAssets"."ServiceLine"
			)
		) as the_union_of_all_pipework
;


SELECT Populate_Geometry_Columns('raw_water_pipes'::regclass);
CREATE INDEX sidx_raw_water_pipes_geom ON raw_water_pipes USING GiST (geom);





---------------------------------------------------
-- Water Network Nodes
---------------------------------------------------


drop table if exists raw_water_nodes cascade;

create table raw_water_nodes as
	with clustered_points as (
		select
			unnest(ST_ClusterWithin(geom, grid() )) as geom
			, "{{ YEAR_START }}"
			, "{{ YEAR_END }}"
		from
		(
			select distinct
				"{{ YEAR_START }}"
				, "{{ YEAR_END }}"
				,
		--		grid_mm
		--		(st_dump(
		--			st_collectionextract(
		--			st_removerepeatedpoints(
						st_simplify(
							nodes."{{ GEOMETRY_FIELD }}"
							, 5 * grid())
		--				))
		--			)
		--		, 1)
		--		)).geom
				::geometry(POINT, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			from raw_water_pipes as pipes
				, ( select distinct
						st_removerepeatedpoints((ST_DumpPoints(geom))."{{ GEOMETRY_FIELD }}") as "{{ GEOMETRY_FIELD }}"
						, -- fist construction matters
						min("{{ YEAR_START }}")
						, -- last removal matters
						max("{{ YEAR_END }}")
					from raw_water_pipes
					group by "{{ GEOMETRY_FIELD }}"
				) as nodes
			where
				-- ST_touches shows not to be robust enough:
				ST_DWithin(nodes."{{ GEOMETRY_FIELD }}", pipes."{{ GEOMETRY_FIELD }}", grid())
			-- group by nodes."{{ GEOMETRY_FIELD }}"
			-- group by grid_mm
		) as raw_points
		GROUP BY "{{ YEAR_START }}", "{{ YEAR_END }}"
	)
	-- select st_centroid(geom) as geom
	select
		"{{ YEAR_START }}"
		, "{{ YEAR_END }}"
		, '' as "{{ REMARK_FIELDNAME }}"
		, ST_GeometricMedian(st_collectionextract(geom, 1))
		::geometry(POINT, {{ SOURCE_TABLES_CRS }}) as geom
	from clustered_points;



SELECT Populate_Geometry_Columns('raw_water_nodes'::regclass);


-- merge close points into one
-- = new network nodes for later snapping pipe ends
--alter table raw_water_nodes add column pipe_count integer default 0;
--update raw_water_nodes set "{{ GEOMETRY_FIELD }}" = (
--
--)



--alter table raw_water_nodes add column geom_2 geometry(POINT, {{ SOURCE_TABLES_CRS }});
--update raw_water_nodes set geom_2 = (
--	ST_GeometricMedian(st_collect(nodes."{{ GEOMETRY_FIELD }}"))
--	from
--)

-- add SERIAL KEY
alter table raw_water_nodes add column "{{ ID_FIELDNAME }}" serial primary key;



-- update nodes with pipe count
-- identifies tees, bend and endcaps
-- this is useful for debugging the network
--alter table raw_water_nodes add column pipe_count integer default 0;
--update raw_water_nodes set pipe_count = (
--select
--	count(pipes.geom)
--	from raw_water_pipes as pipes
--		, (select * from raw_water_nodes) as nodes
--	where ST_DWithin(nodes.geom, pipes.geom, grid())
--		and nodes.id = raw_water_nodes.id
--	group by nodes.geom
--);



-- update nodes with information about interconnections of different purposes
-- identifies tees, bend and endcaps
-- this is useful for debugging the network
alter table raw_water_nodes add column pipe_count integer default 0;
update raw_water_nodes set pipe_count = (
select
	count(pipes.geom)
	from raw_water_pipes as pipes
		, (select * from raw_water_nodes) as nodes
	where
		-- FIXME: ensure it's the pipe's endpoint, not the pipe itself
		ST_DWithin(nodes.geom, pipes.geom, grid())
		-- ST_Touches(nodes.geom, pipes.geom)
		and nodes.id = raw_water_nodes.id
	group by nodes.geom
);





-- update nodes with information about interconnections of different purposes
-- identifies tees, bend and endcaps
-- this is useful for debugging the network


-- update nodes with bend degrees
alter table raw_water_nodes add column specs real default 0;

update raw_water_nodes set specs = (
with
nodes as (
	select *
	from raw_water_nodes
	where pipe_count = 2
)
, pipes as (
	select id, geom
	from raw_water_pipes
)
-- TODO: round to: 15°, 22.5°, 30°, 45°, 60°, 90°, 180°
select select_closest_bend(
		get_bend_degree((array_agg(pipes.geom))[1], (array_agg(pipes.geom))[2])
	)
	from nodes, pipes
	where nodes.geom in (st_startpoint(pipes.geom), st_endpoint(pipes.geom))
		and nodes.id = raw_water_nodes.id
	group by nodes.id
);



CREATE INDEX sidx_raw_water_nodes_geom ON raw_water_nodes USING GiST (geom);



--update raw_water_nodes set bend_degree = 0
--where bend_degree is null;



-- DROP MATERIALIZED VIEW if exists "{{ VIEW_WATER_NODES }}" cascade;
-- DROP VIEW if exists "{{ VIEW_WATER_NODES }}" cascade;

-- create materialized view "{{ VIEW_WATER_NODES }}" as
-- SELECT DISTINCT
	-- row_number() over (PARTITION BY true) as "{{ ID_FIELDNAME }}"
	-- , "{{ SCHEME_FIELDNAME }}"
	-- , "{{ OPERATOR_FIELDNAME }}"
	-- , "{{ COMPONENT_FIELDNAME }}"
	-- , "{{ LABEL_FIELDNAME }}"
	-- , "{{ SCHEME_FIELDNAME }}"
	-- , "{{ CONDITION_FIELDNAME }}"
	-- , "{{ YEAR_START }}"
	-- , "{{ YEAR_END }}"
	-- , "{{ REMARK_FIELDNAME }}"
	-- , null::real as "{{ ELEVATION_FIELDNAME }}"
	-- --, round((tnk_dpth_m * 0.85)::numeric, 1) as initiallev
	-- --, 0 as minimumlev
	-- --, tnk_dpth_m as maximumlev
	-- --, 0::real as minimumvolselect relname, relkind
	-- --, null::text as volumecurv
	-- --, round((sqrt(volume_cbm/tnk_dpth_m*4/3.1415))::numeric,1) as diameter
	-- --, null::text as pattern
	-- ---- , null::real as head -- qwater plugin wünscht es
	-- --, 0.0::real as result_dem
	-- --, 0.0::real as result_pre
	-- --, 0.0::real as result_hea
	-- --, 0.0::real as result_qua
	-- , "{{ GEOMETRY_FIELD }}"::geometry(POINT, {{ SOURCE_TABLES_CRS }})
-- FROM (
		-- (
			-- -- storage facilities
			-- SELECT DISTINCT
				-- trim(both from coalesce("Name", '')) as "{{ LABEL_FIELDNAME }}"
				-- , trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				-- , "Condition" as "{{ CONDITION_FIELDNAME }}"
				-- , "MotorCapacityKw" as "{{ PUMP_POWER_FIELDNAME }}"
				-- , get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				-- , get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				-- , "remark" as "{{ REMARK_FIELDNAME }}"
				-- -- "InitialLevelHmM" as
				-- , st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			-- FROM "NetworkAssets"."WaterPump"
		-- )
	-- ) as all_structures
-- ;

-- create unique index on "{{ VIEW_WATER_NODES }}" ("{{ ID_FIELDNAME }}");

CLUSTER raw_water_nodes USING sidx_raw_water_nodes_geom;
CLUSTER raw_water_pipes USING sidx_raw_water_pipes_geom;




