
DO $$
    BEGIN
        BEGIN
            alter table "CustomerInformation"."CustomerConn" add column "WaterTariff" varchar(64) default NULL;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column <WaterTariff> already exists in <"CustomerInformation"."CustomerConn">.';
        END;
    END;
$$;


---------------------------------------------------
-- Water Consumers
---------------------------------------------------
DROP MATERIALIZED VIEW if exists "{{ VIEW_WATER_CONSUMERS }}" cascade;
DROP VIEW if exists "{{ VIEW_WATER_CONSUMERS }}" cascade;

create materialized view "{{ VIEW_WATER_CONSUMERS }}" as
SELECT DISTINCT
	row_number() over (PARTITION BY true) as "{{ ID_FIELDNAME }}"
	--, "{{ SCHEME_FIELDNAME }}"
	--, "{{ OPERATOR_FIELDNAME }}"
	--, "{{ COMPONENT_FIELDNAME }}"
	, "{{ LABEL_FIELDNAME }}"
	, "{{ SCHEME_FIELDNAME }}"
	, "{{ CONDITION_FIELDNAME }}"
	, "{{ TARIFF_FIELDNAME }}"
	, "{{ YEAR_START }}"
	, "{{ YEAR_END }}"
	, "{{ REMARK_FIELDNAME }}"
	, null::real as "{{ ELEVATION_FIELDNAME }}"
	--, round((tnk_dpth_m * 0.85)::numeric, 1) as initiallev
	--, 0 as minimumlev
	--, tnk_dpth_m as maximumlev
	--, 0::real as minimumvolselect relname, relkind
	--, null::text as volumecurv
	--, round((sqrt(volume_cbm/tnk_dpth_m*4/3.1415))::numeric,1) as diameter
	--, null::text as pattern
	---- , null::real as head -- qwater plugin wünscht es
	--, 0.0::real as result_dem
	--, 0.0::real as result_pre
	--, 0.0::real as result_hea
	--, 0.0::real as result_qua
	, "{{ GEOMETRY_FIELD }}"::geometry(POINT, {{ SOURCE_TABLES_CRS }})
FROM (
		(
			-- storage facilities
			SELECT DISTINCT
				"AccountNumber" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, coalesce("WaterTariff", 'Water consumer') as "{{ TARIFF_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, trim(both from coalesce("Name", '') || '\n' || coalesce("remark", '') ) as "{{ REMARK_FIELDNAME }}"
				, st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			FROM "CustomerInformation"."CustomerConn"
			-- exclude sewer-only customers:
			WHERE lower("CustomerType") ilike '%water%' or "CustomerType" is null
			) UNION (
			SELECT DISTINCT
				"AccountNumber" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, 'Water kiosk' as "{{ TARIFF_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				, st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			FROM "CustomerInformation"."WaterKiosk"
			) UNION (
			SELECT DISTINCT
				"Name" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, coalesce("WaterPointType", 'Water point') as "{{ TARIFF_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				, st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			FROM "CustomerInformation"."WaterPoint"
		)
	) as all_structures
;

create unique index on "{{ VIEW_WATER_CONSUMERS }}" ("{{ ID_FIELDNAME }}");

	-- execute the refresh of materialized view
	-- FIXME: let the geoportal initiate the refresh, because it's the only instance that is interested in the view; this avoids overhead when the geoportal is not running
	drop function if exists refresh_mv_{{ VIEW_WATER_CONSUMERS }}() CASCADE;
	-- CREATE OR REPLACE FUNCTION refresh_mv_{{ VIEW_WATER_CONSUMERS }}() RETURNS trigger LANGUAGE 'plpgsql' AS $BODY$
	-- BEGIN
		-- -- execute format('notify modification, ''%s''', cast(to_char((current_timestamp)::TIMESTAMP,'yyyymmddhhmiss') as BigInt) );
		-- REFRESH MATERIALIZED VIEW CONCURRENTLY "{{ VIEW_WATER_CONSUMERS }}";
		-- return NULL;
	-- END
	-- $BODY$;


	-- -- capture the event of source table refresh can call trigger function
	-- CREATE TRIGGER refresh_{{ VIEW_WATER_CONSUMERS }} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON
	-- "CustomerInformation"."CustomerConn"
	-- FOR EACH STATEMENT
	-- EXECUTE PROCEDURE refresh_mv_{{ VIEW_WATER_CONSUMERS }}();

	-- -- capture the event of source table refresh can call trigger function
	-- CREATE TRIGGER refresh_{{ VIEW_WATER_CONSUMERS }} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON
	-- "CustomerInformation"."WaterKiosk"
	-- FOR EACH STATEMENT
	-- EXECUTE PROCEDURE refresh_mv_{{ VIEW_WATER_CONSUMERS }}();

	-- -- capture the event of source table refresh can call trigger function
	-- CREATE TRIGGER refresh_{{ VIEW_WATER_CONSUMERS }} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON
	-- "CustomerInformation"."WaterPoint"
	-- FOR EACH STATEMENT
	-- EXECUTE PROCEDURE refresh_mv_{{ VIEW_WATER_CONSUMERS }}();


