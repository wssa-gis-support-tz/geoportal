-------------------------------------------------------
-- admin boundaries
-------------------------------------------------------


-- case when is_materialized("{{ VIEW_ADMIN_BOUNDS }}")
DROP materialized VIEW IF EXISTS "{{ VIEW_ADMIN_BOUNDS }}" CASCADE;
DROP VIEW IF EXISTS "{{ VIEW_ADMIN_BOUNDS }}" CASCADE;


CREATE materialized VIEW "{{ VIEW_ADMIN_BOUNDS }}" AS SELECT DISTINCT
	row_number() over (PARTITION BY true) as "{{ ID_FIELDNAME }}"
	, "{{ ADMIN_LEVEL }}"
	, "{{ ADMIN_LEVEL_FIELDNAME }}"
	, "{{ LABEL_FIELDNAME }}"
	, "{{ YEAR_START }}"
	, "{{ YEAR_END }}"
	, st_multi(st_simplifypreservetopology("{{ GEOMETRY_FIELD }}", grid() * 1000))::geometry(
		MULTIPOLYGON
		, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
from (
		( SELECT DISTINCT
			3::integer as "{{ ADMIN_LEVEL }}"
			, 'District' as "{{ ADMIN_LEVEL_FIELDNAME }}"
			, "Name" as "{{ LABEL_FIELDNAME }}"
			, "SourceYear" as "{{ YEAR_START }}"
			, "SourceYear" + 20 as "{{ YEAR_END }}"
			, st_snaptogrid(st_transform("geom", {{ SOURCE_TABLES_CRS }}), grid()*100 ) as "{{ GEOMETRY_FIELD }}"
			FROM "AdminBoundaries"."District"
		) UNION ( SELECT DISTINCT
			2::integer as "{{ ADMIN_LEVEL }}"
			, 'Region' as "{{ ADMIN_LEVEL_FIELDNAME }}"
			, "Name" as "{{ LABEL_FIELDNAME }}"
			, "SourceYear" as "{{ YEAR_START }}"
			, "SourceYear" + 20 as "{{ YEAR_END }}"
			, st_snaptogrid(st_transform("geom", {{ SOURCE_TABLES_CRS }}), grid()*100 ) as "{{ GEOMETRY_FIELD }}"
			FROM "AdminBoundaries"."Region"
		) UNION ( SELECT DISTINCT
			5::integer as "{{ ADMIN_LEVEL }}"
			, 'Ward' as "{{ ADMIN_LEVEL_FIELDNAME }}"
			, "Name" as "{{ LABEL_FIELDNAME }}"
			, "SourceYear" as "{{ YEAR_START }}"
			, "SourceYear" + 20 as "{{ YEAR_END }}"
			, st_snaptogrid(st_transform("geom", {{ SOURCE_TABLES_CRS }}), grid()*100 ) as "{{ GEOMETRY_FIELD }}"
			FROM "AdminBoundaries"."WardShehia"
		)
	) as "admin_bounds"
;

--ALTER VIEW "{{ VIEW_WATER_PIPES }}" ADD PRIMARY KEY "{{ ID_FIELDNAME }}" DISABLE NOVALIDATE;


	create unique index on "{{ VIEW_ADMIN_BOUNDS }}" ("{{ ID_FIELDNAME }}");

