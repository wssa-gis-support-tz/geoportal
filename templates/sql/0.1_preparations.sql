﻿-- define a maximum accuracy to be used when working with geometries. Be careful with Gepgraphic and UTM projections

-- in case of UTM source CRS:
-- create or replace function grid() returns real language sql IMMUTABLE as $body$ select 0.01::real $body$;

-- in case of a WGS84 source CRS (default), a ugly workaround for places close to the equator:
-- FIXME: use {{ WGS84_TO_UTM_SCALE_FACTOR }}!
-- FIXME: READ from CRS if UTM or not
create or replace function grid() returns real language sql IMMUTABLE as $body$ select (0.20/{{ WGS84_TO_UTM_SCALE_FACTOR }})::real $body$;


-- length of longest pipe segment for hydraulic modelling in meter. Be careful with Gepgraphic and UTM projections
-- FIXME: use {{ WGS84_TO_UTM_SCALE_FACTOR }}!
-- FIXME: READ from CRS if UTM or not
create or replace function pipe_segment_length() returns float language sql IMMUTABLE as $body$ select 250.0::float $body$;
create or replace function pipe_segment_length() returns float language sql IMMUTABLE as $body$ select (250.0/{{ WGS84_TO_UTM_SCALE_FACTOR }})::float $body$;



-- materialized views
--CREATE OR REPLACE FUNCTION tg_refresh_my_mv()
--RETURNS trigger LANGUAGE plpgsql AS $$
--BEGIN
--    NOTIFY refresh_mv, 'my_mv';
--    RETURN NULL;
--END;
--$$;


-- check if view is materialized
--create or replace function is_materialized(the_view_name text) returns boolean language sql IMMUTABLE as $body$
--	select relkind = 'm'
--	--select relname, relkind
--	from pg_class
--	where relname = the_view_name
--	-- and relkind = 'm';
--$body$;


-- recursively splits lines at points
DROP FUNCTION IF EXISTS cutlineatpoints(geometry, geometry, double precision) CASCADE;;
CREATE OR REPLACE FUNCTION cutlineatpoints(param_mlgeom geometry, param_mpgeom geometry, param_tol double precision)
RETURNS geometry AS
$$
DECLARE
	var_resultgeom geometry;
	-- dump out multi- into single points and lines so we can use line ref functions
	var_pset geometry[] := ARRAY
	(
		SELECT geom
		FROM ST_Dump(param_mpgeom)
		WHERE ST_IsValid(param_mpgeom)
	);
	var_lset geometry[] := ARRAY
	(
		SELECT geom
		FROM ST_Dump(param_mlgeom)
		WHERE ST_IsValid(param_mlgeom)
	);
	var_sline geometry;
	var_eline geometry;
	var_perc_line double precision;
	var_refgeom geometry;
BEGIN
	FOR i in 1 .. array_upper(var_pset,1) LOOP
		-- Loop through the linestrings
		FOR j in 1 .. array_upper(var_lset,1) LOOP
			-- Check the distance and update if within tolerance
			IF ST_DWithin(var_lset[j], var_pset[i], param_tol)
			AND NOT ST_Intersects(ST_Boundary(var_lset[j]), var_pset[i]) THEN
				IF ST_NumGeometries(ST_Multi(var_lset[j])) = 1 THEN
					--get percent along line point is
					var_perc_line := ST_LineLocatePoint(var_lset[j], var_pset[i]);
					IF var_perc_line > 0 and var_perc_line < 1
					-- or allow some inaccuracies:
					-- IF var_perc_line BETWEEN 0.0001 and 0.9999
					THEN
						--get first cut only cut if not too close to edge
						var_sline := ST_LineSubstring(var_lset[j], 0, var_perc_line);
						-- get secont cut
						var_eline := ST_LineSubstring(var_lset[j], var_perc_line, 1);
						--fix rounding so start line abutts second cut
						var_eline := ST_SetPoint(var_eline, 0, ST_EndPoint(var_sline));
						-- collect the two parts together
						var_lset[j] := ST_Collect(var_sline, var_eline);
					END IF;
				ELSE
					var_lset[j] := cutlineatpoints(var_lset[j], var_pset[i], param_tol);
				END IF;
			END IF;
		END LOOP;
	END LOOP;
	RETURN ST_Union(var_lset);
END;$$LANGUAGE 'plpgsql' STRICT VOLATILE;





-- splits a line along all it's vertexes.
-- returns a multigeometry that needs to be dumped into seperate lines!
drop function if exists st_selfsplit(geometry) CASCADE;
CREATE OR REPLACE FUNCTION st_selfsplit(param_mlgeom geometry) RETURNS geometry LANGUAGE SQL STABLE AS
$body$
	WITH pointdump AS (
		select (st_dumppoints(param_mlgeom)).geom as geom
	)
	, coll as (
		SELECT st_collect(geom) as geom
		FROM pointdump
	)
	SELECT
		cutlineatpoints (
			param_mlgeom
			, coll.geom
			, grid()
		)
		FROM coll
$body$;



-- if no install date is given, determine some value from adjacent attributes:
drop function if exists get_resonable_start_year(input_year, impl_status) CASCADE;
CREATE OR REPLACE FUNCTION get_resonable_start_year(input_year integer, impl_status text) RETURNS integer LANGUAGE SQL IMMUTABLE AS
$body$
SELECT coalesce(
	input_year,
	-- if input_year not set, but it's a known future feature, add one year to the current and submit as Install Date:
	CASE WHEN impl_status ILIKE '%proposed%'
		or impl_status ILIKE '%constr%'
		or impl_status ILIKE '%planned%'
		-- is it's just said "it's proposed", assume it'll be build the next year
		THEN date_part('year', now()) + 1
	END
	)::integer
$body$;



-- if no end year is given, determine some value from adjacent attributes:
drop function if exists get_resonable_end_year(integer, text, integer) CASCADE;
CREATE OR REPLACE FUNCTION get_resonable_end_year(input_year integer, impl_status text, lifespan integer default 60) RETURNS integer LANGUAGE SQL IMMUTABLE AS
$body$
SELECT coalesce(
	-- if input_year not set, but it's a known old feature, deduct one year from current date, but not earlier then install date:
	CASE WHEN impl_status ILIKE '%abandoned%'
		or impl_status ILIKE '%not%functioning%'
		-- THEN round(greatest(date_part('year', now()) - 1, input_year))
		THEN COALESCE(
				-- if its said to be "abandoned" or similar, but not year is given, assume it existed at least for the year of construction
				input_year
				-- 0 will be submitted, it there is no construction year and it's just said to be abandoned, not functioning, etc.
				, 0
			)
	END,
	-- otherwise assume, the features will exist for the given or guessed (50 years) lifespan
	input_year + coalesce(lifespan, 50)
	)::integer
$body$;



-- Provided a diameter in inch and/or mm, return the mm if the given arguments
drop function if exists get_diameter_in_mm_or_null(integer, float) CASCADE;
CREATE OR REPLACE FUNCTION get_diameter_in_mm_or_null(metric integer, imperial float) RETURNS integer LANGUAGE SQL IMMUTABLE AS
$body$
SELECT
	case when metric is not null or imperial is not null
	-- prefer metric over imperial
	then round(coalesce(metric, (imperial * 25.4)))::integer
	else null::integer
	end
$body$;


drop table if exists assign_diameters cascade;
CREATE TABLE assign_diameters
(
	zeile integer
	, ref_letter text
	, dn integer DEFAULT 0
	, od integer DEFAULT 0
);

INSERT INTO assign_diameters VALUES (1, 'a', 10 );
INSERT INTO assign_diameters VALUES (2, 'b', 15, 15);
INSERT INTO assign_diameters VALUES (3, 'c', 20, 20);
INSERT INTO assign_diameters VALUES (4, 'd', 25, 32);
INSERT INTO assign_diameters VALUES (5, 'e', 32, 40);
INSERT INTO assign_diameters VALUES (6, 'f', 40, 50);
INSERT INTO assign_diameters VALUES (7, 'g', 50, 63);
INSERT INTO assign_diameters VALUES (8, 'h', 65, 75);
INSERT INTO assign_diameters VALUES (9, 'i', 80, 90);
INSERT INTO assign_diameters VALUES (10, 'j', 100, 110);
INSERT INTO assign_diameters VALUES (11, 'k', 150, 160);
INSERT INTO assign_diameters VALUES (12, 'n', 200, 225);
INSERT INTO assign_diameters VALUES (13, 'p', 250, 280);
INSERT INTO assign_diameters VALUES (14, 'q', 300, 315);
INSERT INTO assign_diameters VALUES (15, 'r', 350, 355);
INSERT INTO assign_diameters VALUES (16, 's', 400, 400);
INSERT INTO assign_diameters VALUES (17, 't', 450 );
INSERT INTO assign_diameters VALUES (18, 'u', 500 );
INSERT INTO assign_diameters VALUES (19, 'v', 550 );
INSERT INTO assign_diameters VALUES (20, 'w', 600 );
INSERT INTO assign_diameters VALUES (21, 'x', 700 );
INSERT INTO assign_diameters VALUES (22, 'y', 800 );
INSERT INTO assign_diameters VALUES (23, 'z', 900 );
INSERT INTO assign_diameters VALUES (24, 'zz', 1000 );
INSERT INTO assign_diameters VALUES (25, 'zzz', 1200 );
INSERT INTO assign_diameters VALUES (26, 'zzzz', 1400 );
INSERT INTO assign_diameters VALUES (27, 'zzzzz', 1500 );

CREATE EXTENSION IF NOT EXISTS btree_gist;
CREATE INDEX ON assign_diameters USING gist(dn);
CREATE INDEX ON assign_diameters USING gist(od);


-- returns the closest DN to given diameter
DROP FUNCTION IF EXISTS select_closest_dn(integer) CASCADE;
CREATE OR REPLACE FUNCTION select_closest_dn(diameter integer)
RETURNS integer LANGUAGE SQL STABLE AS
$body$
	SELECT
	CASE when diameter is null then (select NULL::int)
	ELSE ( select dn
		FROM assign_diameters
		Order BY diameter <-> dn
		LIMIT 1
	)
	END
$body$;



-- returns the closest OD to given DN diameter
DROP FUNCTION IF EXISTS select_closest_od(integer) CASCADE;
CREATE OR REPLACE FUNCTION select_closest_od(dn_diameter integer)
RETURNS integer LANGUAGE SQL STABLE AS
$body$
	SELECT CASE when dn_diameter is null then (select NULL::int)
	ELSE ( select od
		FROM assign_diameters
		Order BY dn_diameter <-> dn
		LIMIT 1
	) END
$body$;




-- returns true if the pipe is a plastic pipe and should be labelled with "OD" (also needed to calculate the inner diameter)
DROP FUNCTION IF EXISTS is_pipe_of_od_diameter(text) CASCADE;
CREATE OR REPLACE FUNCTION is_pipe_of_od_diameter(material text) RETURNS boolean LANGUAGE SQL IMMUTABLE AS
$body$
	SELECT
		case when material is null
		then false
		else
			(
				material ilike '%pvc%'
				or material ilike '%pe%'
				or material ilike '%pol%'
			)
		end::boolean
$body$;


-- returns true if the pipe is a plastic pipe and should be labelled with "OD" (also needed to calculate the inner diameter)
DROP FUNCTION IF EXISTS get_pipe_diameter_label(text, integer) CASCADE;
CREATE OR REPLACE FUNCTION get_pipe_diameter_label(material text, inner_diameter integer) RETURNS varchar LANGUAGE SQL IMMUTABLE AS
$body$
	SELECT
		case when inner_diameter is not null and inner_diameter > 0
		then
		 	trim( both from
				CASE WHEN is_pipe_of_od_diameter("{{ MATERIAL_FIELDNAME }}")
					THEN 'OD ' || coalesce(select_closest_od(inner_diameter)::varchar, '')
					ELSE 'DN ' || coalesce(select_closest_dn(inner_diameter)::varchar, '')
				END
			)
		else null
		end::varchar
$body$;


-- function to notify the geoportal python app on a refreshed materialized view
--drop function if exists notify_refreshed_mv() CASCADE;
--CREATE OR REPLACE FUNCTION notify_refreshed_mv() RETURNS trigger LANGUAGE 'plpgsql' AS $BODY$
--BEGIN
--	execute format('notify test, ''%s''', cast(to_char((current_timestamp)::TIMESTAMP,'yyyymmddhhmiss') as BigInt) );
--	return NULL;
--END
--$BODY$;


--CREATE OR REPLACE FUNCTION tg_refresh_my_mv()
--RETURNS trigger LANGUAGE plpgsql AS $$
--BEGIN
--    REFRESH MATERIALIZED VIEW CONCURRENTLY my_mv;
--    RETURN NULL;
--END;
--$$;



-- FIXME: better as from an ARRAY[15°, 22.5°, 30°, 45°, 60°, 90°, 180°] then a table
drop table if exists bend_degrees cascade;
CREATE TABLE bend_degrees ( bend_degree real );

-- possible bend degrees; add at your discretion
INSERT INTO bend_degrees VALUES (0.0);
INSERT INTO bend_degrees VALUES (15.0);
INSERT INTO bend_degrees VALUES (22.5);
INSERT INTO bend_degrees VALUES (30.0);
INSERT INTO bend_degrees VALUES (45.0);
INSERT INTO bend_degrees VALUES (60.0);
INSERT INTO bend_degrees VALUES (90.0);
INSERT INTO bend_degrees VALUES (180.0);


-- returns the closest bend
DROP FUNCTION IF EXISTS select_closest_bend(real) CASCADE;
CREATE OR REPLACE FUNCTION select_closest_bend(the_degree real)
RETURNS real LANGUAGE SQL STABLE AS
$body$
	SELECT CASE when the_degree is null then (select NULL::real)
	ELSE ( select bend_degree
		FROM bend_degrees
		Order BY the_degree <-> bend_degree
		LIMIT 1
	) END
$body$;











