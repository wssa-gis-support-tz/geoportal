







-------------------------------------------------------
-- water pipes
-------------------------------------------------------
DROP MATERIALIZED VIEW IF EXISTS "{{ VIEW_WATER_PIPES }}" CASCADE;
CREATE materialized VIEW "{{ VIEW_WATER_PIPES }}" AS
SELECT DISTINCT row_number() over (PARTITION BY true) as "{{ ID_FIELDNAME }}"

, "{{ LABEL_FIELDNAME }}"
, "{{ SCHEME_FIELDNAME }}"
, "{{ OPERATOR_FIELDNAME }}"
, "{{ COMPONENT_FIELDNAME }}"
, "{{ PURPOSE_FIELDNAME }}"
, "{{ DIAMETER_FIELDNAME }}"
, "{{ INNER_DIAMETER_FIELDNAME }}"
, "{{ MATERIAL_FIELDNAME }}"
, "{{ PRESSURECLASS_FIELDNAME }}"
, "{{ CONDITION_FIELDNAME }}"
, "{{ YEAR_START }}"
, "{{ YEAR_END }}"
, "{{ REMARK_FIELDNAME }}"
, "{{ ROUGHNESS_FIELDNAME }}"
, "{{ MINORLOSS_FIELDNAME }}"
, st_setsrid("{{ GEOMETRY_FIELD }}", {{ SOURCE_TABLES_CRS }})::geometry(LINESTRING, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"

FROM
(
	-- prepare wrap the source tables to a set of known fields and do the aggregation
	with united as (

		SELECT DISTINCT
			"{{ LABEL_FIELDNAME }}"
			, "{{ SCHEME_FIELDNAME }}"
			, "{{ OPERATOR_FIELDNAME }}"
			, "{{ COMPONENT_FIELDNAME }}"
			, "{{ PURPOSE_FIELDNAME }}"
			, "{{ INNER_DIAMETER_FIELDNAME }}"
			, "{{ MATERIAL_FIELDNAME }}"
			, "{{ PRESSURECLASS_FIELDNAME }}"
			, "{{ CONDITION_FIELDNAME }}"
			, "{{ YEAR_START }}"
			, "{{ YEAR_END }}"
			, "{{ REMARK_FIELDNAME }}"
			, coalesce("{{ ROUGHNESS_FIELDNAME }}", 1) as "{{ ROUGHNESS_FIELDNAME }}"
			, coalesce("{{ MINORLOSS_FIELDNAME }}", 0) as "{{ MINORLOSS_FIELDNAME }}"
			, get_pipe_diameter_label("{{ MATERIAL_FIELDNAME }}", "{{ INNER_DIAMETER_FIELDNAME }}")	as "{{ DIAMETER_FIELDNAME }}"
			, st_multi(
				ST_LineMerge(
					st_collect(
						st_collectionextract(
								st_removerepeatedpoints(
									st_snaptogrid(
										"{{ GEOMETRY_FIELD }}"
										, grid()
									)
								)
							, 2)
						)
					)
				)
			::geometry(MULTILINESTRING, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
		FROM
		(
			(
				SELECT DISTINCT
				"Name" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, coalesce("PipePurpose", 'Distribution') as "{{ PURPOSE_FIELDNAME }}"
				, "Location" as "{{ COMPONENT_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, "Roughness" as "{{ ROUGHNESS_FIELDNAME }}"
				, "LossCoeffHm" as "{{ MINORLOSS_FIELDNAME }}"
				, "Installer" as "{{ OPERATOR_FIELDNAME }}"
				, "NominalPressure" as "{{ PRESSURECLASS_FIELDNAME }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				, "Material" as "{{ MATERIAL_FIELDNAME }}"
				, get_diameter_in_mm_or_null("NomDiamMm", "NomDiamInch" ) as "{{ INNER_DIAMETER_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, st_transform("geom", {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
				FROM "NetworkAssets"."WaterPipe"
				WHERE "{{ GEOMETRY_FIELD }}" is not null and st_isvalid("{{ GEOMETRY_FIELD }}")
			) UNION	(
				SELECT DISTINCT
				"Name" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, 'ServiceLine' as "{{ PURPOSE_FIELDNAME }}"
				, "Location" as "{{ COMPONENT_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, "Roughness" as "{{ ROUGHNESS_FIELDNAME }}"
				, 0 as "{{ MINORLOSS_FIELDNAME }}"
				, "Installer" as "{{ OPERATOR_FIELDNAME }}"
				, "NominalPressure" as "{{ PRESSURECLASS_FIELDNAME }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				, "Material" as "{{ MATERIAL_FIELDNAME }}"
				, get_diameter_in_mm_or_null("NomDiamMm", "NomDiamInch" ) as "{{ INNER_DIAMETER_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, st_transform("geom", {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
				FROM "NetworkAssets"."ServiceLine"
				WHERE "{{ GEOMETRY_FIELD }}" is not null and st_isvalid("{{ GEOMETRY_FIELD }}")
			)
		) as sources_united
		WHERE st_length(geom) > 0
		GROUP BY
			"{{ LABEL_FIELDNAME }}"
			, "{{ INNER_DIAMETER_FIELDNAME }}"
			, "{{ INNER_DIAMETER_FIELDNAME }}"
			, "{{ SCHEME_FIELDNAME }}"
			, "{{ PURPOSE_FIELDNAME }}"
			, "{{ OPERATOR_FIELDNAME }}"
			, "{{ DIAMETER_FIELDNAME }}"
			, "{{ COMPONENT_FIELDNAME }}"
			, "{{ CONDITION_FIELDNAME }}"
			, "{{ ROUGHNESS_FIELDNAME }}"
			, "{{ MINORLOSS_FIELDNAME }}"
			, "{{ PRESSURECLASS_FIELDNAME }}"
			, "{{ REMARK_FIELDNAME }}"
			, "{{ MATERIAL_FIELDNAME }}"
			, "{{ YEAR_START }}"
			, "{{ YEAR_END }}"
	)

	-- wrap the known field's values
	SELECT DISTINCT

			trim (both from
				coalesce("{{ LABEL_FIELDNAME }}" || ' ', ''::text) ||
				coalesce("{{ DIAMETER_FIELDNAME }}" || ' ', ''::text) ||
				coalesce("{{ MATERIAL_FIELDNAME }}", ''::text
			)) as "{{ LABEL_FIELDNAME }}"
		, "{{ SCHEME_FIELDNAME }}"
		, "{{ OPERATOR_FIELDNAME }}"
		, "{{ COMPONENT_FIELDNAME }}"
		, "{{ PURPOSE_FIELDNAME }}"
		, "{{ INNER_DIAMETER_FIELDNAME }}"
		, "{{ MATERIAL_FIELDNAME }}"
		, "{{ PRESSURECLASS_FIELDNAME }}"
		, "{{ CONDITION_FIELDNAME }}"
		, "{{ YEAR_START }}"
		, "{{ YEAR_END }}"
		, "{{ REMARK_FIELDNAME }}"
		, coalesce("{{ ROUGHNESS_FIELDNAME }}", 1) as "{{ ROUGHNESS_FIELDNAME }}"
		, coalesce("{{ MINORLOSS_FIELDNAME }}", 0) as "{{ MINORLOSS_FIELDNAME }}"
		, get_pipe_diameter_label("{{ MATERIAL_FIELDNAME }}", "{{ INNER_DIAMETER_FIELDNAME }}")	as "{{ DIAMETER_FIELDNAME }}"
		, (st_dump("{{ GEOMETRY_FIELD }}")).geom as "{{ GEOMETRY_FIELD }}"
	FROM united
	WHERE st_length(geom) > 0

) as uniformed
;


create unique index on "{{ VIEW_WATER_PIPES }}" ("{{ ID_FIELDNAME }}");

-- execute the refresh of materialized view
-- FIXME: let the geoportal initiate the refresh, because it's the only instance that is interested in the view; this avoids overhead when the geoportal is not running
drop function if exists refresh_mv_{{ VIEW_WATER_PIPES }}() CASCADE;
