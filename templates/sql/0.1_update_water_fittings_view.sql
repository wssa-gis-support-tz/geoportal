
---------------------------------------------------
-- Water Fittings
-- TODO: Extract tees and bends
---------------------------------------------------


DROP materialized VIEW IF EXISTS "{{ VIEW_WATER_FITTINGS }}" cascade;
DROP VIEW IF EXISTS "{{ VIEW_WATER_FITTINGS }}" cascade;

create materialized view "{{ VIEW_WATER_FITTINGS }}" as
SELECT DISTINCT
	row_number() over (PARTITION BY true) as "{{ ID_FIELDNAME }}"
	, "{{ FITTING_TYPE_FIELDNAME }}"
	, "{{ LABEL_FIELDNAME }}"
	, "{{ SCHEME_FIELDNAME }}"
	, "{{ FITTING_SPECS_FIELDNAME }}"
	, 0 as "{{ AZIMUTH_FIELDNAME }}"
	, "{{ CONDITION_FIELDNAME }}"
	, "{{ YEAR_START }}"
	, "{{ YEAR_END }}"
	, "{{ REMARK_FIELDNAME }}"
	, null::real as "{{ ELEVATION_FIELDNAME }}"
	--, round((tnk_dpth_m * 0.85)::numeric, 1) as initiallev
	--, 0 as minimumlev
	--, tnk_dpth_m as maximumlev
	--, 0::real as minimumvol
	--, null::text as volumecurv
	--, round((sqrt(volume_cbm/tnk_dpth_m*4/3.1415))::numeric,1) as diameter
	--, null::text as pattern
	---- , null::real as head -- qwater plugin wünscht es
	--, 0.0::real as result_dem
	--, 0.0::real as result_pre
	--, 0.0::real as result_hea
	--, 0.0::real as result_qua
	, "{{ GEOMETRY_FIELD }}"::geometry(POINT, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
FROM (
		(
			SELECT DISTINCT
				"Name" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, coalesce("HydrantType", 'Hydrant') as "{{ FITTING_TYPE_FIELDNAME }}"
				, get_diameter_in_mm_or_null("NozzleDiamMm", "NozzleDiamInch") || ' mm' as "{{ FITTING_SPECS_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				, st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			FROM "NetworkAssets"."Hydrant"
		) UNION (
			SELECT DISTINCT
				"Name" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, coalesce("ValveFunction", 'Valve') as "{{ FITTING_TYPE_FIELDNAME }}"
				, trim(both from coalesce("ValveMechanism", '') || ' ' || coalesce("ValveType", '')) as "{{ FITTING_SPECS_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				, st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			FROM "NetworkAssets"."Valve"
		) UNION (
			SELECT DISTINCT
				"Name" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, coalesce("FittingType", 'Fitting') as "{{ FITTING_TYPE_FIELDNAME }}"
				, trim(both from coalesce("Material", '') || ' ' || coalesce(get_diameter_in_mm_or_null("Diameter1Mm", "Diameter1Inch")::text) || '/' || get_diameter_in_mm_or_null("Diameter2Mm", "Diameter2Inch")::text || ' mm')  as "{{ FITTING_SPECS_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				, st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			FROM "NetworkAssets"."Fitting"
		) UNION (
			SELECT DISTINCT
				"Name" as "{{ LABEL_FIELDNAME }}"
				, trim(both from coalesce("ZoneLocation", '')) as "{{ SCHEME_FIELDNAME }}"
				, coalesce('Bulk Meter, ' || "BulkMeterType", 'Bulk Meter') as "{{ FITTING_TYPE_FIELDNAME }}"
				, trim(both from coalesce(get_diameter_in_mm_or_null("DiameterMm", "DiameterInch")::text) || ' mm') as "{{ FITTING_SPECS_FIELDNAME }}"
				, "Condition" as "{{ CONDITION_FIELDNAME }}"
				, get_resonable_start_year("InstallationDate", "Status") as "{{ YEAR_START }}"
				, get_resonable_end_year(get_resonable_start_year("InstallationDate", "Status"), "Status") as "{{ YEAR_END }}"
				, "remark" as "{{ REMARK_FIELDNAME }}"
				, st_transform(geom, {{ SOURCE_TABLES_CRS }}) as "{{ GEOMETRY_FIELD }}"
			FROM "NetworkAssets"."BulkMeter"
		)
	) as all_structures
;



	create unique index on "{{ VIEW_WATER_FITTINGS }}" ("{{ ID_FIELDNAME }}");


	-- execute the refresh of materialized view
	-- FIXME: let the geoportal initiate the refresh, because it's the only instance that is interested in the view; this avoids overhead when the geoportal is not running
	drop function if exists refresh_mv_{{ VIEW_WATER_FITTINGS }}() CASCADE;
	-- CREATE OR REPLACE FUNCTION refresh_mv_{{ VIEW_WATER_FITTINGS }}() RETURNS trigger LANGUAGE 'plpgsql' AS $BODY$
	-- BEGIN
		-- -- execute format('notify modification, ''%s''', cast(to_char((current_timestamp)::TIMESTAMP,'yyyymmddhhmiss') as BigInt) );
		-- REFRESH MATERIALIZED VIEW CONCURRENTLY "{{ VIEW_WATER_FITTINGS }}";
		-- return NULL;
	-- END
	-- $BODY$;


	-- -- capture the event of source table refresh can call trigger function
	-- CREATE TRIGGER refresh_{{ VIEW_WATER_FITTINGS }} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON
	-- "NetworkAssets"."Hydrant"
	-- FOR EACH STATEMENT EXECUTE PROCEDURE refresh_mv_{{ VIEW_WATER_FITTINGS }}();

	-- -- capture the event of source table refresh can call trigger function
	-- CREATE TRIGGER refresh_{{ VIEW_WATER_FITTINGS }} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON
	-- "NetworkAssets"."Valve"
	-- FOR EACH STATEMENT EXECUTE PROCEDURE refresh_mv_{{ VIEW_WATER_FITTINGS }}();

	-- -- capture the event of source table refresh can call trigger function
	-- CREATE TRIGGER refresh_{{ VIEW_WATER_FITTINGS }} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON
	-- "NetworkAssets"."Fitting"
	-- FOR EACH STATEMENT EXECUTE PROCEDURE refresh_mv_{{ VIEW_WATER_FITTINGS }}();

	-- -- capture the event of source table refresh can call trigger function
	-- CREATE TRIGGER refresh_{{ VIEW_WATER_FITTINGS }} AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ON
	-- "NetworkAssets"."BulkMeter"
	-- FOR EACH STATEMENT EXECUTE PROCEDURE refresh_mv_{{ VIEW_WATER_FITTINGS }}();




DROP table if exists nodes_raw;
