# Purpose

Webapplication for Water and Sanitation Authorities for the datamodel as agreed during the ATAWAS conference in February 2020 in Dodoma, Tanzania.

Current features:

- Live mapping of Water and Sanitation infrastructure in various, predefined styles, reflecting the GIS "as it should be used"
- Works on materialized views of the source data model, therefore only this wrapper has to be modified to adjust it to other source data models


![Current development screenshot of the Geoportal browser Window](https://gitlab.com/wssa-gis-support-tz/geoportal/-/raw/master/screenshot.jpg?inline=false "Development screenshot")


# Quickstart: Setup of a development server

You'll require access to a ready configured [PostGIS](https://postgis.net/) server. Get a blank database/datamodel from [https://gitlab.com/wssa-gis-support-tz/datamodel](https://gitlab.com/wssa-gis-support-tz/datamodel).

## Setup under Windows

1. Make sure you have python enviroment in your machine if not download and install python enviroment from; [Python](http://python.org) or [Anaconda](http://Anaconda.org) choose python3 version
 ```
Run CMD
cd Desktop
```   

2. Create a virtual env (Python 3) and enter it
```
pip install virtualenv
mkdir yourProjectName
cd yourProjectName
virtualenv wssa-gis-support-tz
cd wssa-gis-support-tz
```

3.  Download & install Git to enable using it in command line from [install git for windows](https://gitforwindows.org/)

> Go to  desktop -> yourProjectName then right click on 'wssa-gis-support-tz' and select with 'GitBash Here'

> Type on the opened console "git clone 'then paste the link' (http://gitlab.com/wssa-gis-support-tz/geoportal)"


4. Clone repository to geoportal
```
git clone https://gitlab.com/wssa-gis-support-tz/geoportal.git
```

5.  Activate the virtualenv
```
cd scripts
activate
```

6. Setup the geoportal development server; download GDAL (xxxxx.whl) from packages build by Christoph Gohlke available [here](https://www.lfd.uci.edu/~gohlke/pythonlibs/#gdal)[](url). Choose the package that match the version of Python
```
cd..
cd geoportal
pip install -r requirements.txt
pip install psycopg2
cd C:\Users\(AdminName)\Downloads
python.exe -m pip install xxxxx.whl
cd C:\Users\(AdminName)\Desktop\(yourProjectName)\wssa-gis-support-tz\geoportal

```

7. Setup django
```
python manage.py makemigrations # No changes detected
python manage.py migrate
python manage.py createsuperuser # enter some data here
python manage.py collectstatic # say yes
```

8. Edit the geoportal configuration file `geoportal/.env` to suit your needs. Set at least the credentials of your geodatabase in this line:

> GEODATABASE_URL='postgis://<db_username>:<postgresql>@<db_hostaddress>:5432/<db_name>?' 

If you want Bing maps background images (live satellite images and road map), get a API key [here](http://www.bingmapsportal.com/) and set the corresponding variable:


> BING_MAPS_KEY='<your_API_key>' 


9.  Start the development server
```
python manage.py runserver 0.0.0.0:8000
```

10. Access the Geoportal from a Webbrowser of your choice

> [http://<your_server_ip>:8000/](http://localhost:8000/)

![Current development screenshot of the Geoportal browser Window](https://gitlab.com/wssa-gis-support-tz/geoportal/-/raw/master/illustration.jpg?inline=false "Development screenshot")

## Setup under Linux

1.  Assuming python is already available, create a virtual env (Python 3) and enter it
```
$ virtualenv wssa-gis-support-tz
$ cd wssa-gis-support-tz
```
2.  Clone the source code repository
```
$ git clone https://gitlab.com/wssa-gis-support-tz/geoportal.git
```

3.  Enter the Python virtualenv and setup the geoportal development server
```
$ source bin/activate
$ cd geoportal
$ pip install -r requirements.txt # hope for no error; GDAL might be problematic
```

4. Setup django
```
$ python manage.py makemigrations # No changes detected
$ python manage.py migrate
$ python manage.py createsuperuser # enter some data here
$ python manage.py collectstatic # say yes
```

5. Edit the geoportal configuration file `geoportal/.env` to suit your needs. Set at least the credentials of your geodatabase in this line:

> GEODATABASE_URL='postgis://<db_username>:<postgresql>@<db_hostaddress>:5432/<db_name>?' 

If you want Bing maps background images (live satellite images and road map), get a API key [here](http://www.bingmapsportal.com/) and set the corresponding variable:

> BING_MAPS_KEY='<your_API_key>' 

6.  Start the development server
```
$ python manage.py runserver 0.0.0.0:8000
```

7. Access the Geoportal from a Webbrowser of your choice

> [http://<your_server_ip>:8000/](http://localhost:8000/)


# Contributors: technologies used

## Python 3.X

* [Django framework](http://www.djangoproject.com)

## CSS

* [Bootstrap 4](https://getbootstrap.com)

## Javascript

* jQuery
* Openlayers (Vector contents are submitted as KML)
* jsPDF