��    0      �  C         (  #   )     M     g     u     �     �  #   �     �     �     �  3     '   :     b     �     �     �     �  
   �     �  
   �     �     �               -     6     K  
   Y     d     v     �     �  3   �     �     �     �               5     G     ^      c     �     �     �     �     �  B  �     .	  	   B	  	   L	     V	     q	     �	     �	  
   �	     �	     �	  1   �	     "
     B
     ^
     l
     {
     �
     �
     �
     �
     �
     �
     �
  "        1     =     S     k     �     �  
   �     �     �     �                      8     Y     q     �  #   �     �     �     �     �                               
   /         (                 +          	      *                          .   $                                         #   '   )       !         ,   "              &                  0   -                   %     is logged in. Click here to Logout Administrative Boundaries Administrator Borehole with electric pump Click here to login Consumer connection Coordinate reference system for map Distribution Documentation Elevated tank Emailaddress is not assigned to any registered user Features of undefined construction year Features younger then {} years Future features Ground tank Hydraulic modelling Language Last login Login Login name No Login entered Older then {} years features Other purpose Other type of water source Password Primary (Trunk main) Printable map Sanitation Sanitation system Secondary (Lateral pipe) Staff Staff login Style map features considering this design horizon. Surface water intake Switch language Transmission Undefined purpose Undefined water source type Underground basin Unknown tank structure User User exists, but it is inactive. User not registered Water Supply Water supply system Water treatment plant {} to {} years old features Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Bonyeza hapa kutoka Msimamizi Msimamizi Borehole na pampu ya umeme Bonyeza hapa kuingia Uunganisho wa watumiaji Chagua mfumo wa kuatibude Usambazaji Mfumo wa usafishaji maji Tanki lililoinuliwa BaruaPepe haijapewa mtumiaji yeyote aliyesajiliwa Mwaka wa ujenzi haujafafanuliwa Chini ya miaka {} iliyopita Siku za usoni Tanki la chini Modeli ya mfumo wa majimaji Badili lugha Mara ya mwisho kuingia Ingia Jina la Mtumiaji Hutajaza taarifa Zaidi ya miaka {} iliyopita Kusudi lingine Aina nyinginezo za vyanzo vya maji Neo la siri Shina la maji machafu Onyesha ramani chapishi Mfumo wa usafishaji maji Mfumo wa usafishaji maji Mfereji wa maji machafu Mfanyakazi Ingilio la wafanyikazi Chagua upeo wa muundo Uchukuzi wa Bwawa au Mto Badili lugha Uwasilishaji Kusudi halijafafanuliwa Chanzo cha maji hakijafafanuliwa Bonde la chini ya ardhi Muundo usiojulikana Mtumiaji Mtumiaji yupo, lakini haifanyi kazi Mtumiaji hajasajiliwa Usambazaji wa maji Usambazaji wa maji Eneo la kutibu maji Miaka ({} hadi {}) iliyopita 